﻿using HotlineMVC.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace HotlineMVC.Controllers
{
    public class HelpdeskController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.OpenIssues = CommonFunctions.GetValueFromDb("Select Count(*) from logbook_helpdesk where status=1");
            ViewBag.AllIssues = CommonFunctions.GetValueFromDb("Select Count(*) from logbook_helpdesk");
            ViewBag.AddressedOnSpot = CommonFunctions.GetValueFromDb("Select Count(*) from logbook_helpdesk where AddressedOnSpot=1");
            ViewBag.TargetingIssues = CommonFunctions.GetValueFromDb("Select Count(*) from logbook_helpdesk_view where Category=3");
            ViewBag.CalendarEvents = CommonFunctions.GetJson(@"select concat(Date,'T08:00:00') as [start],concat(Date,'T08:00:00') as [end],count(*) as title,concat('/LogBook/Helpdesk/Records?grid-filter=Date__1__',Date) as url from logbook_helpdesk
                                                                    Group by Date");
            ViewBag.PeriodicReport = getPeriodicReport();
            ViewBag.MonthlyReport = getMonthlyReport();
            ViewBag.RecordsByGovernorate = getGovernorateReport();
            return View();
        }
        public ActionResult Records()
        {
            ViewBag.Status = CommonFunctions.GetListFromDb("select id as Value, StatusDesc as Text from logbook_Status");
            ViewBag.Governorate = CommonFunctions.GetJson("select id, GovernorateDesc as name, Partner as pkey from logbook_Governorates");
            ViewBag.Partner = CommonFunctions.GetListFromDb("select id as Value, PartnerDesc as Text from logbook_Partners");
            ViewBag.CBO = CommonFunctions.GetJson("select id, CBODesc, Partner, Governorate from logbook_Helpdesk_CBOs");
            ViewBag.Category = CommonFunctions.GetListFromDb("select id as Value, CategoryDesc as Text from logbook_Helpdesk_Categories");
            ViewBag.ResponsibleUnit = CommonFunctions.GetListFromDb("select id as Value, Unit as Text from Units");
            ViewBag.SubCategory = CommonFunctions.GetJson(@"select a.id, subcategorydesc as name,category as pkey,a.unit,b.Unit as UnitDesc from logbook_helpdesk_subcategories a
                                                                left join units b on a.Unit = b.ID");
            string SQL = @"SELECT [ID]
                  ,[RecordNumber]
                  ,[Date]
                  ,[Time]
                  ,[Status]
                  ,[CaseNumber]
                  ,[PhoneNumber]
                  ,[Gender]
                  ,[Governorate]
                  ,[Partner]
                  ,[CBO]
                  ,[Staff]
                  ,[Category]
                  ,[SubCategory]
                  ,[Details]
                  ,[AddressedOnSpot]
                  ,[ReplacementRequired]
                  ,[ResponsibleUnit]
                  ,[GovernorateDesc]
                  ,[PartnerDesc]
                  ,[CBODesc]
                  ,[AddressedOnSpotDesc]
                  ,[ReplacementRequiredDesc]
                  ,[CategoryDesc]
                  ,[SubCategoryDesc]
                  ,[ResponsibleUnitDesc]
                  ,[StatusDesc]
                  ,[CategoryDesc]
                  ,[SubcategoryDesc]
                  ,GenderDesc
              FROM [logbook].[dbo].[logbook_helpdesk_view]
              ORDER BY RecordNumber DESC";
            List<HelpdeskModel> list = GetHelpDeskFromDb(SQL);
            return View(list.ToList());
        }
        public static List<HelpdeskModel> GetHelpDeskFromDb(string SQL)
        {
            DataTable dt = CommonFunctions.GetDataFromDb(SQL);
            List<HelpdeskModel> items = new List<HelpdeskModel>();
            foreach (DataRow row in dt.Rows)
            {
                items.Add(new HelpdeskModel
                {
                    RecordNumber = row["RecordNumber"].ToString(),
                    Date = (DateTime)row["Date"],
                    Time = row["Time"].ToString(),
                    Status = row["Status"].ToString(),
                    CaseNumber = row["CaseNumber"].ToString(),
                    PhoneNumber = CommonFunctions.IntVal(row["PhoneNumber"].ToString()),
                    Gender = row["Gender"].ToString(),
                    Governorate = row["Governorate"].ToString(),
                    Partner = row["Partner"].ToString(),
                    CBO = row["CBO"].ToString(),
                    Staff = row["Staff"].ToString(),
                    Category = row["Category"].ToString(),
                    Subcategory = row["Subcategory"].ToString(),
                    Details = row["Details"].ToString(),
                    AddressedOnSpot = row["AddressedOnSpot"].ToString(),
                    ReplacementRequired = row["ReplacementRequired"].ToString(),
                    ResponsibleUnit = row["ResponsibleUnit"].ToString(),
                    GovernorateDesc = row["GovernorateDesc"].ToString(),
                    PartnerDesc = row["PartnerDesc"].ToString(),
                    CBODesc = row["CBODesc"].ToString(),
                    AddressedOnSpotDesc = row["AddressedOnSpotDesc"].ToString(),
                    ReplacementRequiredDesc = row["ReplacementRequiredDesc"].ToString(),
                    ResponsibleUnitDesc = row["ResponsibleUnitDesc"].ToString(),
                    StatusDesc = row["StatusDesc"].ToString(),
                    CategoryDesc = row["CategoryDesc"].ToString(),
                    SubcategoryDesc = row["SubcategoryDesc"].ToString(),
                    GenderDesc = row["GenderDesc"].ToString(),
                });
            }
            return items;
        }
        public ActionResult Replacement()
        {
            return View();
        }
        public ActionResult BeneficiaryProfile(string id)
        {
            string SQL = string.Format(@"select a.*,b.Trans_Date as DateOfEntry,c.PreviousRecords,
                'N/A' as ReasonForRemoval,'N/A' as ReplacementStatus,0 as CurrentBalance
                from (select top 1 Beneficiary_ID,Trans_Date as LastReloadDate, 
                Governorate,Household_Size as CaseSize,trans_amount as ReloadAmount,Location,
                case when Reload_Percent = '100' then 'Extremely Vulnerable' else 'Vulnerable' end 
                as VulnerabilityStatus ,
                case when Location like '%Camp%' then 'Camp' else 'Community' end as LocationStatus 
                from jordandb.dbo.prepaid_reload_wfp where Beneficiary_ID='{0}' order by Trans_Date Desc) a
                left join (select top 1 Beneficiary_ID,Trans_Date
                from jordandb.dbo.prepaid_reload_wfp where Beneficiary_ID='{0}' order by Trans_Date) b 
                on a.Beneficiary_ID=b.Beneficiary_ID
                left join (select CaseNumber,count(*) as PreviousRecords 
                from logbook.dbo.logbook_helpdesk where CaseNumber='{0}'  Group By CaseNumber) c
                on a.Beneficiary_ID=c.CaseNumber",id);
            DataTable data = CommonFunctions.GetDataFromDb(SQL);
            if (data.Rows.Count > 0)
                ViewBag.Data = data.Rows[0];
            else
                ViewBag.Data = null;
            return View();
        }
        public ActionResult ViewRecord(string id)
        {
            if (id == "")
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            List<HelpdeskModel> list = GetHelpDeskFromDb(string.Format(@"SELECT [ID]
                  ,[RecordNumber]
                  ,[Date]
                  ,[Time]
                  ,[Status]
                  ,[CaseNumber]
                  ,[PhoneNumber]
                  ,[Gender]
                  ,[GenderDesc]
                  ,[Governorate]
                  ,[Partner]
                  ,[CBO]
                  ,[Staff]
                  ,[Category]
                  ,[SubCategory]
                  ,[Details]
                  ,[AddressedOnSpot]
                  ,[ReplacementRequired]
                  ,[ResponsibleUnit]
                  ,[GovernorateDesc]
                  ,[PartnerDesc]
                  ,[CBODesc]
                  ,[AddressedOnSpotDesc]
                  ,[ReplacementRequiredDesc]
                  ,[CategoryDesc]
                  ,[SubCategoryDesc]
                  ,[ResponsibleUnitDesc]
                  ,[StatusDesc]
                  ,[CategoryDesc]
                  ,[SubcategoryDesc]
              FROM [logbook].[dbo].[logbook_helpdesk_view]
              where RecordNumber='{0}'", id));
            HelpdeskModel model = list.First(s => s.RecordNumber == id);
            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }
        public ActionResult BeneficiarySearch(string id)
        {
            return View();
        }
        private IDictionary<string, string> getPeriodicReport()
        {
            int Month = DateTime.Now.Month;
            int Year = DateTime.Now.Year;
            string today = DateTime.Now.ToString("yyyy-MM-dd");
            string todayRecords = CommonFunctions.GetValueFromDb(string.Format("Select count(*) from logbook_helpdesk where date='{0}'", today));
            string yesterday = DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd");
            string yesterdayRecords = CommonFunctions.GetValueFromDb(string.Format("Select count(*) from logbook_helpdesk where date='{0}'", yesterday));
            string weekstart = DateTime.Now.StartOfWeek(DayOfWeek.Sunday).ToString("yyyy-MM-dd");
            string weekRecords = CommonFunctions.GetValueFromDb(string.Format("Select count(*) from logbook_helpdesk where date between '{0}' and '{1}'", weekstart, today));
            string monthRecords = CommonFunctions.GetValueFromDb(string.Format("Select count(*) from logbook_helpdesk where month(date)={0} and year(date)={1}", Month, Year));
            string yearRecords = CommonFunctions.GetValueFromDb(string.Format("Select count(*) from logbook_helpdesk where year(date)={0}", Year));
            string allRecords = getAllRecords();
            IDictionary<string, string> data = new Dictionary<string, string>();
            data["today"] = string.Format("{0:c}", todayRecords);
            data["yesterday"] = string.Format("{0:c}", yesterdayRecords);
            data["week"] = string.Format("{0:c}", weekRecords);
            data["month"] = string.Format("{0:c}", monthRecords);
            data["year"] = string.Format("{0:c}", yearRecords);
            data["all"] = string.Format("{0:c}", allRecords);
            return data;
        }
        private string getMonthlyReport()
        {
            DataTable dt = new DataTable();
            string SQL = @"select format(date,'MMM-yy') as Month, count(*) as Number from logbook_helpdesk 
                group by format(date,'MMM-yy'),Left(date,7)
                order by Left(date,7)";
            return CommonFunctions.GetJson(SQL);
        }
        private string getGovernorateReport()
        {
            string SQL = @"SELECT GovernorateDesc Governorate, COUNT(GovernorateDesc) Number FROM dbo.logbook_helpdesk_view GROUP BY GovernorateDesc";
            return CommonFunctions.GetJson(SQL);
        }
        private string getAllRecords()
        {
            return CommonFunctions.GetValueFromDb("Select count(*) from logbook_helpdesk");
        }
    }
}