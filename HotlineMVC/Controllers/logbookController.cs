﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using HotlineMVC.Models;
using System.Web;
using System.IO;
using System.Data.OleDb;



namespace HotlineMVC.Controllers
{
    public class logbookController : Controller
    {
        private LogBookEntities db = new LogBookEntities();
        List<SelectListItem> Source = CommonFunctions.GetListFromDb("select id as Value, sourceDesc as Text from logbook_sources");        
        List<SelectListItem> Status = CommonFunctions.GetListFromDb("select id as Value, statusDesc as Text from logbook_Status where id <> 3");
        List<SelectListItem> ResponsibleUnit = CommonFunctions.GetListFromDb("select StaffUnitID as Value, (Unit) as Text from logbook_responsiblePerson_vw where isPrimary =1 and staffunitid <> 1015");
        string responsiblePerson = CommonFunctions.GetJson("select StaffID id, Name as name,StaffUnitID as pkey from logbook_responsiblePerson_vw where isPrimary =1");        
        List<SelectListItem> Programme = CommonFunctions.GetListFromDb("select id as Value, ProgrammeDesc as Text from logbook_programmes where id not in (5,6, 7)");
        List<SelectListItem> Severity = CommonFunctions.GetListFromDb("select id as Value, SeverityDesc as Text from logbook_severity");
        List<SelectListItem> LocationTypes = CommonFunctions.GetListFromDb("select id as Value, LocationTypeDesc as Text from logbook_location_types where  id <> 3");
        List<SelectListItem> Gender = CommonFunctions.GetListFromDb("select id as Value, GenderDesc as Text from logbook_gender");
        string Category = CommonFunctions.GetJson("select id, categorydesc as name,programme as pkey from logbook_categories");
        string SubCategory = CommonFunctions.GetJson("select id, subcategorydesc as name,category as pkey from logbook_subcategories");
        string Governorate = CommonFunctions.GetJson("select id, governoratedesc as name,type as pkey from logbook_governorates");
        string SubSource = CommonFunctions.GetJson("select id, subsourcedesc as name,source as pkey from logbook_subsources");
        string SubProgramme = CommonFunctions.GetJson("select id, subprogrammedesc as name,programme as pkey from logbook_subprogrammes");
        string district = CommonFunctions.GetJson("select id, districtdesc as name,governorate as pkey from logbook_districts");

        List<SelectListItem> ddlMonitoringActivity = CommonFunctions.GetListFromDb("select id Value, MonitoringActivity Text from logbook_monitoring_activities_types where parentID is null and ID <> 13");
        string ddlMonitoringActivitySub = CommonFunctions.GetJson("select id, MonitoringActivity name, ParentID as pkey from logbook_monitoring_activities_types where parentID is not null");

        

        public ActionResult ExportExcel(string tablename = "", string calls = "", string q = "", string clientflt = "")
        {
            HotlineMVC.Controllers.UserInfo ui = (HotlineMVC.Controllers.UserInfo)Session["UserInfo"];
            CommonFunctions.LogActivity("Exported data to Excel");
            string select = @"select RecordNumber,RecordTypeDesc as [Record Type],StatusDesc as Status,
                            SubProgrammeDesc as [Sub Programme],
                            Staff as [Created By],Date,Time,SourceDesc as Source,SubSourceDesc as [Sub Source],
                            SeverityDesc as Severity,CategoryDesc as Category,SubCategoryDesc as [Sub Category],
                            PartnerName as [Partner Name],LocationTypeDesc as [Location Type],
                            GovernorateDesc as Governorate,District,GPSCoordinateLat as Latitude,
                            GPSCoordinateLong as Longitude,CaseNumber,PhoneNumber,Gender,SubCategory,
                            Details,Severity,ShopID,ResponsibleUnitDesc as [Responsible Unit],
                            ResponsiblePersonDesc as [Responsible Person],Action as [Action Taken],
                            DateOfLastAction as [Date of Last Action],ActionTakenBy as [Action Taken By],
                            ClosedBy as [Closed by],DateClosed as [Date Closed],
                            ShopName as [Shop Name],GenderDesc as Gender,
                            ShopBranch as [Shop Branch],[address] as [Shop Address]
                            from [dbo].[logbook_records_view] where RecordNumber is not null ";
            string criteria = "";
            if (tablename != null)
            {
                if (calls != null && calls.Length > 0)
                {
                    criteria = " and RecordNumber in (" + calls + ")";
                }
                else if (tablename == "MyCalls")
                {
                    string name = Session["Username"] == null ? "" : Session["Username"].ToString();
                    string StaffId=Session["StaffId"] == null ? "" : Session["StaffId"].ToString();
                    criteria = " and (Staff='" + name + "' or ResponsiblePerson=" + StaffId + ")"; 
                }
                else if (tablename == "Search")
                {
                    criteria = string.Format(@" and (RecordNumber{0} or RecordTypeDesc{0} or StatusDesc{0} or 
                            SubProgrammeDesc{0} or 
                            Staff{0} or Date{0} or Time{0} or SourceDesc{0} or SubSourceDesc{0} or 
                            SeverityDesc{0} or CategoryDesc{0} or SubCategoryDesc{0} or 
                            PartnerName{0} or LocationTypeDesc{0} or 
                            GovernorateDesc{0} or District{0} or GPSCoordinateLat{0} or 
                            GPSCoordinateLong{0} or CaseNumber{0} or PhoneNumber{0} or SubCategory{0} or 
                            Details{0} or Severity{0} or ShopID{0} or ResponsibleUnitDesc{0} or 
                            ResponsiblePersonDesc{0} or Action{0} or 
                            DateOfLastAction{0} or GenderDesc{0} or 
                            ClosedBy{0} or DateClosed{0} or 
                            ShopName{0} or 
                            ShopBranch{0} or address{0})", " like '%" + q + "%'");
                }
                else if (tablename == "DetailedReport")
                {
                    criteria = Request.QueryString["criteria"];
                }
            }
            string clientcriteria = GetCriteriaFromClientFilters(clientflt);
            string sql = select + criteria+clientcriteria;
            string result = CommonFunctions.GetExcel(sql, tablename);
            string response = "";
            if (result.Substring(result.Length - 3) == "xls")
            {
                response = CommonFunctions.JsonResponse(200, result);
            }
            else
            {
                response = CommonFunctions.JsonResponse(400, "The excel file couldn't be created because of error: " + result);
            }
            return Content(response, "application/json");
        }
        public ActionResult AssignRecords(string tablename, string staff, string unit, string person, string q = "", string clientflt = "")
        {
            CommonFunctions.LogActivity("Batch assigned records");
            string select = @"select RecordNumber,RecordTypeDesc as [Record Type],StatusDesc as Status,
                            SubProgrammeDesc as [Sub Programme],
                            Staff as [Created By],Date,Time,SourceDesc as Source,SubSourceDesc as [Sub Source],
                            SeverityDesc as Severity,CategoryDesc as Category,SubCategoryDesc as [Sub Category],
                            PartnerName as [Partner Name],LocationTypeDesc as [Location Type],
                            GovernorateDesc as Governorate,District,GPSCoordinateLat as Latitude,
                            GPSCoordinateLong as Longitude,CaseNumber,PhoneNumber,Gender,SubCategory,
                            Details,Severity,ShopID,ResponsibleUnitDesc as [Responsible Unit],
                            ResponsiblePersonDesc as [Responsible Person],Action as [Action Taken],
                            DateOfLastAction as [Date of Last Action],ActionTakenBy as [Action Taken By],
                            ClosedBy as [Closed by],DateClosed as [Date Closed],
                            ShopName as [Shop Name],GenderDesc as Gender,
                            ShopBranch as [Shop Branch],[address] as [Shop Address]
                            from [dbo].[logbook_records_view] where RecordNumber is not null ";
            string criteria = "";
            if (tablename != null)
            {
                if (tablename=="Selected")
                {
                    criteria = " and RecordNumber in (" + clientflt + ")";
                }
                else if (tablename == "MyCalls")
                {
                    string name = Session["Username"] == null ? "" : Session["Username"].ToString();
                    criteria = " and Staff='" + name + "'";
                }
                else if (tablename == "Search")
                {
                    criteria = string.Format(@" and (RecordNumber {0} or RecordTypeDesc {0} or StatusDesc {0} or 
                            SubProgrammeDesc {0} or 
                            Staff {0} or Date {0} or Time {0} or SourceDesc {0} or SubSourceDesc {0} or 
                            SeverityDesc {0} or CategoryDesc {0} or SubCategoryDesc {0} or 
                            PartnerName {0} or LocationTypeDesc {0} or 
                            GovernorateDesc {0} or District {0} or GPSCoordinateLat {0} or 
                            GPSCoordinateLong {0} or CaseNumber {0} or PhoneNumber {0} or SubCategory {0} or 
                            Details {0} or Severity {0} or ShopID {0} or ResponsibleUnitDesc {0} or 
                            ResponsiblePersonDesc {0} or Action {0} or 
                            DateOfLastAction {0} or GenderDesc {0} or 
                            ClosedBy {0} or DateClosed {0} or 
                            ShopName {0} or 
                            ShopBranch {0} or address {0})", " like '%" + q + "%'");
                }
            }
            string clientcriteria = "";
            if(tablename!="Selected")
                clientcriteria=GetCriteriaFromClientFilters(clientflt);
            string sql = select + criteria + clientcriteria;
            CommonFunctions.UpdateRecordAssignment(sql, unit, person);
            string result = CommonFunctions.GetExcel(sql, tablename);
            string response = "";
            if (result.Substring(result.Length - 3) == "xls")
            {
                string From = staff + "@wfp.org";
                string To = CommonFunctions.GetStaffEmail(person);
                string RecordUrl = "https://cbttriangulation.jor.wfp.org/LogBook/logbook?grid-column=StatusDesc&grid-dir=0&grid-filter=StatusDesc__1__Open";
                string Message = "Dear " + CommonFunctions.GetFirstNameFromEmail(To) + ",<br /><br />" +
                    "This is to inform you that the attached records have been assigned to you. Kindly take action and update me accordingly."
                    +"<br /><br /> Click "+RecordUrl+" to update the records"
                    + "<br /><br /> Thanks, <br />" + CommonFunctions.GetFirstNameFromEmail(From);
                CommonFunctions.SendEmail(From, To, "Log Book Batch Assignment", Message, Server.MapPath("~/Content/Downloads/"+result));
                response = CommonFunctions.JsonResponse(200, result);
            }
            else
            {
                response = CommonFunctions.JsonResponse(400, "The excel file couldn't be created because of error: " + result);
            }
            return Content(response, "application/json");
        }
        public ActionResult CloseRecords(string tablename, string staff, string actiontaken, string clientflt = "")
        {
            CommonFunctions.LogActivity("Closed some records");
            string select = @"select RecordNumber,RecordTypeDesc as [Record Type],StatusDesc as Status,
                            SubProgrammeDesc as [Sub Programme],
                            Staff as [Created By],Date,Time,SourceDesc as Source,SubSourceDesc as [Sub Source],
                            SeverityDesc as Severity,CategoryDesc as Category,SubCategoryDesc as [Sub Category],
                            PartnerName as [Partner Name],LocationTypeDesc as [Location Type],
                            GovernorateDesc as Governorate,District,GPSCoordinateLat as Latitude,
                            GPSCoordinateLong as Longitude,CaseNumber,PhoneNumber,Gender,SubCategory,
                            Details,Severity,ShopID,ResponsibleUnitDesc as [Responsible Unit],
                            ResponsiblePersonDesc as [Responsible Person],Action as [Action Taken],
                            DateOfLastAction as [Date of Last Action],ActionTakenBy as [Action Taken By],
                            ClosedBy as [Closed by],DateClosed as [Date Closed],
                            ShopName as [Shop Name],GenderDesc as Gender,
                            ShopBranch as [Shop Branch],[address] as [Shop Address]
                            from [dbo].[logbook_records_view] where RecordNumber is not null ";
            string criteria = "";
            if (tablename != null)
            {
                if (tablename == "Selected")
                {
                    criteria = " and RecordNumber in (" + clientflt + ")";
                }
            }
            string sql = select + criteria;
            CommonFunctions.CloseRecords(sql,actiontaken);
            string result = CommonFunctions.GetExcel(sql, tablename);
            string response = "";
            if (result.Substring(result.Length - 3) == "xls")
            {
                string To = "oscar.lindow@wfp.org";
                string From = staff+"@wfp.org";
                string Message = "Dear Log Book Admin, <br /><br />" +
                    "This is to inform you that the attached records have been closed by "+ CommonFunctions.GetFirstNameFromEmail(From) +". Kindly inform the benficiaries if necessary."
                       + "<br /><br /> Thanks, <br />" + CommonFunctions.GetFirstNameFromEmail(From);
                CommonFunctions.SendEmail(From, To, "Log Book Record Batch Closure", Message, Server.MapPath("~/Content/Downloads/" + result));
                response = CommonFunctions.JsonResponse(200, result);
            }
            else
            {
                response = CommonFunctions.JsonResponse(400, "The excel file couldn't be created because of error: " + result);
            }
            return Content(response, "application/json");
        }

        public ActionResult UploadCloseRecords(string tablename, string staff)
        {
            CommonFunctions.LogActivity("Closed records by uploading a file");
            string select = @"select RecordNumber,RecordTypeDesc as [Record Type],StatusDesc as Status,
                            SubProgrammeDesc as [Sub Programme],
                            Staff as [Created By],Date,Time,SourceDesc as Source,SubSourceDesc as [Sub Source],
                            SeverityDesc as Severity,CategoryDesc as Category,SubCategoryDesc as [Sub Category],
                            PartnerName as [Partner Name],LocationTypeDesc as [Location Type],
                            GovernorateDesc as Governorate,District,GPSCoordinateLat as Latitude,
                            GPSCoordinateLong as Longitude,CaseNumber,PhoneNumber,Gender,SubCategory,
                            Details,Severity,ShopID,ResponsibleUnitDesc as [Responsible Unit],
                            ResponsiblePersonDesc as [Responsible Person],Action as [Action Taken],
                            DateOfLastAction as [Date of Last Action],ActionTakenBy as [Action Taken By],
                            ClosedBy as [Closed by],DateClosed as [Date Closed],
                            ShopName as [Shop Name],GenderDesc as Gender,
                            ShopBranch as [Shop Branch],[address] as [Shop Address]
                            from [dbo].[logbook_records_view] where RecordNumber is not null ";
            DataTable RecordsDt = GetRecordsDtFromCSV();
            string RecordIds = "";
            foreach (DataRow row in RecordsDt.Rows)
            {
                RecordIds += row[0] + "','";
            }
            RecordIds = RecordIds.Trim(new Char[] { ' ', ',', '.','\'' });
            string criteria = " and RecordNumber in ("+ RecordIds+")";
            string sql = select + criteria;
            CommonFunctions.CloseUploadedRecords(RecordsDt);
            string result = CommonFunctions.GetExcel(sql, tablename);
            string response = "";
            if (result.Substring(result.Length - 3) == "xls")
            {
                //Notify log book admin
                string To = "boster.sibande@wfp.org"; // Call center manager
                string From = staff + "@wfp.org";
                string Message = "Dear Log Book Admin, <br /><br />" +
                    "This is to inform you that the attached records have been closed by " + CommonFunctions.GetFirstNameFromEmail(From) + ". Kindly inform the benficiaries if necessary."
                       + "<br /><br /> Thanks, <br />" + CommonFunctions.GetFirstNameFromEmail(From);
                CommonFunctions.SendEmail(From, To, "Log Book Record Batch Closure", Message, Server.MapPath("~/Content/Downloads/" + result));
                response = CommonFunctions.MsgBox("The records have been successfully closed","MyCalls");
            }
            else
            {
                response = result+CommonFunctions.MsgBox("There was an error in closing the records", "MyCalls");
            }
            return Content(response);
        }
        private string GetCriteriaFromClientFilters(string filters)
        {
            
            string[] ArrFilters = filters.Trim('|').Split('|');
            string StFilter = "";
            for (int i = 0; i < ArrFilters.Length; i++)
            {
                if (ArrFilters[i].Contains("__1__"))
                {
                    //Equals
                    ArrFilters[i] = ArrFilters[i].Replace("__1__", " ='");
                    ArrFilters[i] += "'";
                }
                else if (ArrFilters[i].Contains("__2__"))
                {
                    //Contains
                    ArrFilters[i] = ArrFilters[i].Replace("__2__", " LIKE '%");
                    ArrFilters[i] += "%'";
                }
                else if (ArrFilters[i].Contains("__3__"))
                {
                    //Starts with
                    ArrFilters[i] = ArrFilters[i].Replace("__3__", " LIKE '%");
                    ArrFilters[i] += "'";
                }
                else if (ArrFilters[i].Contains("__4__"))
                {
                    //Ends with
                    ArrFilters[i] = ArrFilters[i].Replace("__4__", " LIKE '");
                    ArrFilters[i] += "%'";
                }
                else if (ArrFilters[i].Contains("__5__"))
                {
                    //Greater than
                    ArrFilters[i] = ArrFilters[i].Replace("__5__", ">'");
                    ArrFilters[i] += "'";
                }
                else if (ArrFilters[i].Contains("__6__"))
                {
                    //Less than
                    ArrFilters[i] = ArrFilters[i].Replace("__6__", "<'");
                    ArrFilters[i] += "'";
                }
                if(ArrFilters[i].Length>0)
                    StFilter += " AND " + ArrFilters[i];
            }
            return StFilter;
        }

        public ActionResult Index()
        {
            CommonFunctions.LogActivity("Opened All Records Page");
            ViewBag.Source = Source;
            //ViewBag.ShopId = ShopId;
            ViewBag.Status = Status;
            ViewBag.ResponsibleUnit = ResponsibleUnit;
            //ViewBag.ResponsiblePerson = ResponsiblePerson;
            ViewBag.Programme = Programme;
            ViewBag.SubProgramme = SubProgramme;
            ViewBag.district = district;
            ViewBag.responsiblePerson = responsiblePerson;

            ViewBag.Severity = Severity;
            ViewBag.LocationTypes = LocationTypes;
            ViewBag.Gender = Gender;
            ViewBag.Category = Category;
            ViewBag.Subcategory = SubCategory;
            ViewBag.Governorate = Governorate;
            ViewBag.SubSource = SubSource;
            ViewBag.ddlMonitoringActivity = ddlMonitoringActivity;
            ViewBag.ddlMonitoringActivitySub = ddlMonitoringActivitySub;
            //ViewBag.UnitsInCopy = UnitsInCopy;

            ViewBag.MyRecords = GetMyRecords();

            string CCRights = "";
            string username = Session["Username"] != null ? Session["Username"].ToString() : string.Empty;
            string FullName = Session["fullname"] != null ? Session["fullname"].ToString() : string.Empty;
            UserInfo ui = (UserInfo)Session["UserInfo"];
            


            if (Session["LogBookRights"] != null)
            {
                CCRights = Session["LogBookRights"].ToString();
            }

            if (CCRights.Contains("VA"))
            {
                return View(db.logbook_records_view.OrderByDescending(x => x.RecordNumber));
            }
            else if (Session["UserInfo"] != null && ui.ProtectionAdvisor)
            {
                var list = (from c in db.logbook_records_view
                            where c.Staff == username || c.ResponsiblePersonDesc == FullName || ui.Programms.Contains<string>(c.Programme.ToString())
                            || ui.ResponsibleForUnits.Contains(c.ResponsibleUnit.ToString())
                            || c.CopyProtectionAdvisor == 1
                            select c).OrderByDescending(x => x.RecordNumber);

                return View(list.ToList());
            }
            else if (Session["UserInfo"] != null && ui.GenderFocalPoint)
            {
                var list = (from c in db.logbook_records_view
                            where c.Staff == username || c.ResponsiblePersonDesc == FullName || ui.Programms.Contains<string>(c.Programme.ToString()) 
                            || ui.ResponsibleForUnits.Contains(c.ResponsibleUnit.ToString())
                            || c.CopyGenderFocalPoint==1 
                            select c).OrderByDescending(x => x.RecordNumber);

                return View(list.ToList());
            }
            else
            {
                var list = (from c in db.logbook_records_view
                            where c.Staff == username || c.ResponsiblePersonDesc == FullName || ui.Programms.Contains<string>(c.Programme.ToString()) || ui.ResponsibleForUnits.Contains(c.ResponsibleUnit.ToString())
                            select c).OrderByDescending(x => x.RecordNumber);

                return View(list.ToList());
            }
        }

        public ActionResult MyCalls()
        {
            CommonFunctions.LogActivity("Opened My Records Page");
            ViewBag.Source = Source;
            //ViewBag.ShopId = ShopId;
            ViewBag.Status = Status;
            ViewBag.ResponsibleUnit = ResponsibleUnit;
            //ViewBag.ResponsiblePerson = ResponsiblePerson;
            ViewBag.Programme = Programme;
            ViewBag.SubProgramme = SubProgramme;
            ViewBag.district = district;
            ViewBag.responsiblePerson = responsiblePerson;
            ViewBag.Severity = Severity;
            ViewBag.LocationTypes = LocationTypes;
            ViewBag.Gender = Gender;
            ViewBag.Category = Category;
            ViewBag.Subcategory = SubCategory;
            ViewBag.Governorate = Governorate;
            ViewBag.SubSource = SubSource;
            ViewBag.ddlMonitoringActivity = ddlMonitoringActivity;
            ViewBag.ddlMonitoringActivitySub = ddlMonitoringActivitySub;
           // ViewBag.UnitsInCopy = UnitsInCopy;
            string username = Session["Username"] != null ? Session["Username"].ToString() : string.Empty;
            string FullName = Session["fullname"] != null ? Session["fullname"].ToString() : string.Empty;
            ViewBag.MyRecords = GetMyRecords();
            var list = (from c in db.logbook_records_view
                        where c.Staff == username || c.ResponsiblePersonDesc == FullName
                        select c).OrderByDescending(x => x.RecordNumber);

            return View(list.ToList());
            
        }

        // GET: logbook/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            logbook_records_view logbook_records_view = db.logbook_records_view.Find(id);
            if (logbook_records_view == null)
            {
                return HttpNotFound();
            }
            return View(logbook_records_view);
        }

        // GET: logbook/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: logbook/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "RecordNumber,Status,Staff,Date,Time,Source,Governorate,CaseNumber,PhoneNumber,CallerNumber,Gender,MainPurpose,ShopProblem,ShopID,ProtectionIssue,ComparativeStudy,Referral,ResponsibleUnit,ResponsiblePerson,Action,ActionTakenBy,StatusDesc,ReferralDesc,GovernorateDesc,ShopIssue,MainPurposeDesc,GenderDesc,CashComparativeStudy,SourceDesc,ProtectionIssueDesc,ResponsibleUnitDesc,ShopName,ShopBranch")] logbook_records_view logbook_records_view)
        {
            if (ModelState.IsValid)
            {
                db.logbook_records_view.Add(logbook_records_view);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(logbook_records_view);
        }

        // GET: logbook/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            logbook_records_view logbook_records_view = db.logbook_records_view.Find(id);
            if (logbook_records_view == null)
            {
                return HttpNotFound();
            }
            return View(logbook_records_view);
        }
        public ActionResult ViewTicket(string id)
        {
            if (id == "")
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            logbook_records_view logbook_records_view = db.logbook_records_view.First(s => s.RecordNumber == id);
            if (logbook_records_view == null)
            {
                return HttpNotFound();
            }
            return View(logbook_records_view);
        }

        public ActionResult ViewHistory(string id)
        {
            if (id == "")
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            List<HistoryModel> list = GetHistoryFromDb("select id,RecordNumber,changes,doneby,datetime from logbook_record_changes where RecordNumber='"+id + "'");
            return View(list.ToList());
        }
        public static List<HistoryModel> GetHistoryFromDb(string SQL)
        {
            DataTable dt = CommonFunctions.GetDataFromDb(SQL);
            List<HistoryModel> items = new List<HistoryModel>();
            foreach (DataRow row in dt.Rows)
            {
                items.Add(new HistoryModel
                {
                    Changes = row["changes"].ToString(),
                    DoneBy = row["doneby"].ToString(),
                    DateTime = row["datetime"].ToString()
                });
            }
            return items;
        }
        // POST: logbook/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "RecordNumber,Status,Staff,Date,Time,Source,Governorate,CaseNumber,PhoneNumber,CallerNumber,Gender,MainPurpose,ShopProblem,ShopID,ProtectionIssue,ComparativeStudy,Referral,ResponsibleUnit,ResponsiblePerson,Action,ActionTakenBy,StatusDesc,ReferralDesc,GovernorateDesc,ShopIssue,MainPurposeDesc,GenderDesc,CashComparativeStudy,SourceDesc,ProtectionIssueDesc,ResponsibleUnitDesc,ShopName,ShopBranch")] logbook_records_view logbook_records_view)
        {
            if (ModelState.IsValid)
            {
                db.Entry(logbook_records_view).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(logbook_records_view);
        }

        // GET: logbook/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            logbook_records_view logbook_records_view = db.logbook_records_view.Find(id);
            if (logbook_records_view == null)
            {
                return HttpNotFound();
            }
            return View(logbook_records_view);
        }

        // POST: logbook/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            logbook_records_view logbook_records_view = db.logbook_records_view.Find(id);
            db.logbook_records_view.Remove(logbook_records_view);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        public ActionResult Search(string q)
        {
            CommonFunctions.LogActivity("Searched for ''"+q+"'' in the records");
            ViewBag.Source = Source;            
            ViewBag.Status = Status;
            ViewBag.ResponsibleUnit = ResponsibleUnit;            
            ViewBag.Programme = Programme;
            ViewBag.SubProgramme = SubProgramme;
            ViewBag.district = district;
            ViewBag.responsiblePerson = responsiblePerson;
            ViewBag.Severity = Severity;
            ViewBag.LocationTypes = LocationTypes;
            ViewBag.Gender = Gender;
            ViewBag.Category = Category;
            ViewBag.Subcategory = SubCategory;
            ViewBag.Governorate = Governorate;
            ViewBag.SubSource = SubSource;
            ViewBag.ddlMonitoringActivity = ddlMonitoringActivity;
            ViewBag.ddlMonitoringActivitySub = ddlMonitoringActivitySub;
            //ViewBag.UnitsInCopy = UnitsInCopy;
            string username = Session["Username"] != null ? Session["Username"].ToString() : string.Empty;
            string FullName = Session["fullname"] != null ? Session["fullname"].ToString() : string.Empty;
            UserInfo ui = (UserInfo)Session["UserInfo"];
            ViewBag.MyRecords = GetMyRecords();
            ViewBag.Programme = Programme;

            string CCRights = "";
           
            if (Session["LogBookRights"] != null)
            {
                CCRights = Session["LogBookRights"].ToString();
            }

            if (Session["LogBookRights"] != null && CCRights.Contains("VA"))
            {
                var list = from c in db.logbook_records_view
                           orderby c.RecordNumber descending
                           where (c.RecordNumber.Contains(q) || c.ProgrammeDesc.Contains(q)
                                || c.SubProgrammeDesc.Contains(q) || c.SourceDesc.Contains(q)
                                || c.OtherExternalSource.Contains(q) || c.SubSourceDesc.Contains(q)
                                || c.SourceFurtherInfo.Contains(q) || c.HKMLocationType.Contains(q)
                                || c.GovernorateDesc.Contains(q)  || c.DistrictDesc.Contains(q)
                                || c.Gov2.Contains(q) || c.Dis2.Contains(q)
                                || c.Gov3.Contains(q) || c.Dis3.Contains(q)
                                || c.Gov4.Contains(q) || c.Dis4.Contains(q)
                                || c.Gov5.Contains(q) || c.Dis5.Contains(q)
                                || c.Gov6.Contains(q) || c.Dis6.Contains(q)
                                || c.Gov7.Contains(q) || c.Dis7.Contains(q)
                                || c.Gov8.Contains(q) || c.Dis8.Contains(q)
                                || c.Gov9.Contains(q) || c.Dis9.Contains(q)
                                || c.Gov10.Contains(q) || c.Dis10.Contains(q)
                                || c.StatusDesc.Contains(q) || c.CategoryDesc.Contains(q)
                                || c.SubCategoryDesc.Contains(q) || c.Details.Contains(q)
                                || c.ResponsibleUnitDesc.Contains(q) || c.ResponsiblePersonDesc.ToString().Contains(q)
                                || c.SeverityDesc.ToString().Contains(q) || c.Action.Contains(q)
                                || c.MonitoringActivity.Contains(q) || c.Municipality.Contains(q) 
                                || c.Title.Contains(q) 
                                || c.ActivityType1.Contains(q)
                                || c.ActivityType2.Contains(q)
                                || c.ActivityType3.Contains(q)
                                || c.ActivityType4.Contains(q)
                                || c.ActivityType5.Contains(q)
                                || c.ActivityType6.Contains(q)
                                || c.ActivityType7.Contains(q)
                                || c.ActivityType8.Contains(q)
                                || c.ActivityType9.Contains(q)
                                || c.ActivityType10.Contains(q)
                                || c.MainChallenges.Contains(q)
                                || c.MainFindings.Contains(q)
                                || c.IssuestoFollowUp.Contains(q)
                                || c.ActionTakenbyFM.Contains(q)

                                )
                           select c;
                ViewBag.Count = list.Count();
                return View(list.ToList());
            }
            else
            {
                var list = from c in db.logbook_records_view
                           orderby c.RecordNumber descending
                           where (c.RecordNumber.Contains(q) || c.ProgrammeDesc.Contains(q)
                                || c.SubProgrammeDesc.Contains(q) || c.SourceDesc.Contains(q)
                                || c.OtherExternalSource.Contains(q) || c.SubSourceDesc.Contains(q)
                                || c.SourceFurtherInfo.Contains(q) || c.HKMLocationType.Contains(q)
                                || c.GovernorateDesc.Contains(q) || c.DistrictDesc.Contains(q)
                                || c.Gov2.Contains(q) || c.Dis2.Contains(q)
                                || c.Gov3.Contains(q) || c.Dis3.Contains(q)
                                || c.Gov4.Contains(q) || c.Dis4.Contains(q)
                                || c.Gov5.Contains(q) || c.Dis5.Contains(q)
                                || c.Gov6.Contains(q) || c.Dis6.Contains(q)
                                || c.Gov7.Contains(q) || c.Dis7.Contains(q)
                                || c.Gov8.Contains(q) || c.Dis8.Contains(q)
                                || c.Gov9.Contains(q) || c.Dis9.Contains(q)
                                || c.Gov10.Contains(q) || c.Dis10.Contains(q)
                                || c.StatusDesc.Contains(q) || c.CategoryDesc.Contains(q)
                                || c.SubCategoryDesc.Contains(q) || c.Details.Contains(q)
                                || c.ResponsibleUnitDesc.Contains(q) || c.ResponsiblePersonDesc.ToString().Contains(q)
                                || c.SeverityDesc.ToString().Contains(q) || c.Action.Contains(q)
                                || c.MonitoringActivity.Contains(q) || c.Municipality.Contains(q)
                                || c.Title.Contains(q)
                                || c.ActivityType1.Contains(q)
                                || c.ActivityType2.Contains(q)
                                || c.ActivityType3.Contains(q)
                                || c.ActivityType4.Contains(q)
                                || c.ActivityType5.Contains(q)
                                || c.ActivityType6.Contains(q)
                                || c.ActivityType7.Contains(q)
                                || c.ActivityType8.Contains(q)
                                || c.ActivityType9.Contains(q)
                                || c.ActivityType10.Contains(q)
                                || c.MainChallenges.Contains(q)
                                || c.MainFindings.Contains(q)
                                || c.IssuestoFollowUp.Contains(q)
                                || c.ActionTakenbyFM.Contains(q)
                               ) && (c.Staff == username || c.ResponsiblePersonDesc == FullName || ui.Programms.Contains<string>(c.Programme.ToString()) || ui.ResponsibleForUnits.Contains(c.ResponsibleUnit.ToString()))
                           select c;
                ViewBag.Count = list.Count();
                return View(list.ToList());
            }



            
            //if (Session["LogBookRights"] != null)
            //{
            //    CCRights = Session["LogBookRights"].ToString().Split('%');
            //}
            //var list = from c in db.logbook_records_view
            //       orderby c.RecordNumber descending
            //       where (c.RecordNumber.Contains(q) || c.RecordTypeDesc.Contains(q)
            //            || c.StatusDesc.Contains(q)      || c.SubProgrammeDesc.Contains(q)
            //            || c.Staff.Contains(q) || c.SourceDesc.Contains(q)
            //            || c.SubSourceDesc.Contains(q) || c.SeverityDesc.Contains(q)
            //            || c.CategoryDesc.Contains(q)  || c.SubCategoryDesc.Contains(q)
            //            || c.PartnerName.Contains(q)   || c.LocationTypeDesc.Contains(q)
            //            || c.GovernorateDesc.Contains(q) || c.District.Contains(q)
            //            || c.CaseNumber.Contains(q) || c.PhoneNumber.ToString().Contains(q)
            //            || c.SubCategory.ToString().Contains(q) || c.Details.Contains(q)
            //            || c.ResponsibleUnitDesc.Contains(q) || c.ResponsiblePersonDesc.Contains(q)
            //            || c.Action.Contains(q) || c.DateOfLastAction.Contains(q)
            //            || c.GenderDesc.Contains(q) || c.ClosedBy.Contains(q)
            //            || c.DateClosed.Contains(q) //|| c.ShopName.Contains(q)
            //           // || c.ShopBranch.Contains(q) 
            //           // || c.address.Contains(q)) 
            //           ) && (c.Staff == username || c.ResponsiblePersonDesc == FullName)
            //           select c;
            //if (Session["LogBookRights"] != null && CCRights.Contains("VA"))
            //{
            //    list = from c in db.logbook_records_view
            //               orderby c.RecordNumber descending
            //               where c.RecordNumber.Contains(q) || c.RecordTypeDesc.Contains(q)
            //            || c.StatusDesc.Contains(q) || c.SubProgrammeDesc.Contains(q)
            //            || c.Staff.Contains(q) || c.SourceDesc.Contains(q)
            //            || c.SubSourceDesc.Contains(q) || c.SeverityDesc.Contains(q)
            //            || c.CategoryDesc.Contains(q) || c.SubCategoryDesc.Contains(q)
            //            || c.PartnerName.Contains(q) || c.LocationTypeDesc.Contains(q)
            //            || c.GovernorateDesc.Contains(q) || c.District.Contains(q)
            //            || c.CaseNumber.Contains(q) || c.PhoneNumber.ToString().Contains(q)
            //            || c.SubCategory.ToString().Contains(q) || c.Details.Contains(q)
            //            || c.ResponsibleUnitDesc.Contains(q) || c.ResponsiblePersonDesc.Contains(q)
            //            || c.Action.Contains(q) || c.DateOfLastAction.Contains(q)
            //            || c.GenderDesc.Contains(q) || c.ClosedBy.Contains(q)
            //            || c.DateClosed.Contains(q) //|| c.ShopName.Contains(q)
            //           // || c.ShopBranch.Contains(q) || c.address.Contains(q)
            //           select c;
            //}
            //ViewBag.Count = list.Count();
            //return View(list.ToList());
        }
        private string GetMyRecords()
        {
            string username = Session["Username"] != null ? Session["Username"].ToString() : string.Empty;
            return CommonFunctions.GetValueFromDb("Select count(*) from logbook_records where Staff='" 
                + username + "' and date='" + DateTime.Now.ToString("yyyy-MM-dd") + "'");
        }
        private static string GetMainPurposeJson()
        {
            DataTable dt = CommonFunctions.GetDataFromDb(@"Select t1.ID,RankingDesc,TimeFrameDesc,EmailTo,EmailCC
                                                            from logbook_main_purposes t1
                                                            left join logbook_ranking t2 on t1.Ranking=t2.id
                                                            left join logbook_timeframe t3 on t1.TimeFrame=t3.id");
            foreach(DataRow row in dt.Rows)
            {
                string id = row["ID"].ToString();
                string EmailTo = row["EmailTo"].ToString();
                string EmailCC = row["EmailCC"].ToString();
                string NewEmailTo = CommonFunctions.GetCSV(@"SELECT t2.name FROM dbo.splitstring((select EmailTo 
                                                            from logbook_main_purposes where id = " + id + @")) as t1
                                                            inner join staff as t2 on t1.Name = t2.ID");
                string NewEmailCC = CommonFunctions.GetCSV(@"SELECT t2.name FROM dbo.splitstring((select EmailCC 
                                                            from logbook_main_purposes where id = "+id+@")) as t1
                                                            inner join staff as t2 on t1.Name = t2.ID");
                row["EmailTo"] = NewEmailTo.Replace("name", "").Trim().Replace(@"\r\n","<br />");
                row["EmailCC"] = NewEmailCC.Replace("name", "").Trim().Replace(@"\r\n", "<br />");
            }
            return CommonFunctions.GetJsonFromDataTable(dt);
        }
        private DataTable GetRecordsDtFromCSV()
        {
            DataSet ds = new DataSet("Temp");
            DataTable dt = new DataTable();
            var item = Request.Files["csvfile"];
            
            if (item.FileName.Length > 0)
            {
                //Split the file path to get file title
                string[] FileTitle = item.FileName.Split('\\');
                string NewDocPath = "~/Content/Downloads/" + FileTitle[FileTitle.Length - 1];
                NewDocPath = Server.MapPath(NewDocPath);
                //Save the file to the disk
                item.SaveAs(NewDocPath);
                OleDbConnection conn = new OleDbConnection
                       ("Provider=Microsoft.ACE.OLEDB.12.0; Data Source = " +
                         Path.GetDirectoryName(NewDocPath) +
                         "; Extended Properties = \"Text;HDR=YES;FMT=Delimited\"");
                conn.Open();
                OleDbDataAdapter adapter = new OleDbDataAdapter
                       ("SELECT * FROM [" + Path.GetFileName(NewDocPath)+"]", conn);
                adapter.Fill(ds);
                dt=ds.Tables[0];
                conn.Close();
            }
            return dt;
        }
    }


}
