﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HotlineMVC.Controllers
{
    public class AdminController : Controller
    {
        public ActionResult Index()
        {
            CommonFunctions.LogActivity("Opened the admin section");
            return View();
        }
        public ActionResult Create(int id)
        {
            string sql = "";
            string item = Request.Form["newitem"];
            switch (id)
            {
                case 1:
                    sql = string.Format("insert into logbook_sources (source) values ({0})",SqlValue(item,true));
                    break;
                case 2:
                    sql = string.Format("Insert into logbook_governorates (Governorate) values ({0})",SqlValue(item, true));
                    break;
                case 3:
                    sql = string.Format("Insert into logbook_gender (Gender) values ({0})", SqlValue(item, true));
                    break;
                case 4:
                    sql = string.Format("Insert into logbook_main_purposes (MainPurpose) values ({0})", SqlValue(item, true));
                    break;
                case 5:
                    sql = string.Format("Insert into logbook_shop_issues (ShopIssue) values ({0})", SqlValue(item, true));
                    break;
                case 6:
                    sql = string.Format("Insert into logbook_protection_issues (ProtectionIssue) values ({0})", SqlValue(item, true));
                    break;
                case 7:
                    sql = string.Format("Insert into logbook_cash_comparative_study (CashComparativeStudy) values ({0})", SqlValue(item, true));
                    break;
                case 8:
                    sql = string.Format("Insert into Units (Unit) values ({0})", SqlValue(item, true));
                    break;
            }
            int rows = CommonFunctions.RunCommand(sql);
            string response = "";
            if (rows > 0)
            {
                response = CommonFunctions.JsonResponse(200, "The record was saved successfully");
                CommonFunctions.LogActivity("Created a new item in the admin section: " + item +" in section no."+Section(id));
            }
            else
            {
                response = CommonFunctions.JsonResponse(400, "Error! The item could not be saved. Consult the System Administrator");
                CommonFunctions.LogActivity("Failed to create a new item in the admin section: " + item + " in section no." + Section(id));
            }
            return Content(response, "application/json");
        }
        public ActionResult Update(int tableid,int id, string value)
        {
            string sql = "";
            switch (tableid)
            {
                case 1:
                    sql = string.Format("update logbook_sources set source={0} where id={1}", SqlValue(value, true),id);
                    break;
                case 2:
                    sql = string.Format("Update logbook_governorates set Governorate={0} where id={1}", SqlValue(value, true),id);
                    break;
                case 3:
                    sql = string.Format("Update logbook_gender  set Gender={0} where id={1}", SqlValue(value, true),id);
                    break;
                case 4:
                    sql = string.Format("Update logbook_main_purposes  set MainPurpose={0} where id={1}", SqlValue(value, true),id);
                    break;
                case 5:
                    sql = string.Format("Update logbook_shop_issues  set ShopIssue={0} where id={1}", SqlValue(value, true),id);
                    break;
                case 6:
                    sql = string.Format("Update logbook_protection_issues  set ProtectionIssue={0} where id={1}", SqlValue(value, true),id);
                    break;
                case 7:
                    sql = string.Format("Update logbook_cash_comparative_study  set CashComparativeStudy={0} where id={1}", SqlValue(value, true),id);
                    break;
                case 8:
                    sql = string.Format("Update Units set Unit={0} where id={1}", SqlValue(value, true),id);
                    break;
            }
            int rows = CommonFunctions.RunCommand(sql);
            string response = "";
            if (rows > 0)
            {
                response = CommonFunctions.JsonResponse(200, "The record was saved successfully");
                CommonFunctions.LogActivity("Updated an item in the admin section: " + value + " in section no." + Section(tableid));
            }
            else
            {
                response = CommonFunctions.JsonResponse(400, "Error! The item could not be saved. Consult the System Administrator");
                CommonFunctions.LogActivity("Failed to update an item in the admin section: " + value + " in section no." + Section(tableid));
            }
            return Content(response, "application/json");
        }
        public ActionResult ShowTable(int id)
        {
            string sql = "";
            switch (id)
            {
                case 1:
                    sql = @"select id,Source from logbook_sources";
                    break;
                case 2:
                    sql = @"select id,Governorate from logbook_governorates";
                    break;
                case 3:
                    sql = @"select id,Gender from logbook_gender";
                    break;
                case 4:
                    sql = @"Select id,MainPurpose from logbook_main_purposes";
                    break;
                case 5:
                    sql = @"select id, ShopIssue from logbook_shop_issues";
                    break;
                case 6:
                    sql = @"select id, ProtectionIssue from logbook_protection_issues";
                    break;
                case 7:
                    sql = @"select id,CashComparativeStudy from logbook_cash_comparative_study";
                    break;
                case 8:
                    sql = @"select id, Unit from Units";
                    break;
            }
            DataTable dt = CommonFunctions.GetDataFromDb(sql);
            ViewBag.Data = dt;
            ViewBag.tableId = id;
            CommonFunctions.LogActivity("Viewed " + Section(id) + " in the admin section");
            return View();
        }
        private string SqlValue(string val, bool quote)
        {
            string tmp = "";
            if (quote)
                tmp = SqlStr(val);
            else
                tmp = val;
            if (tmp == "")
                tmp = "NULL";
            else if (quote)
                tmp = "'" + tmp + "'";
            return tmp;
        }
        private string SqlStr(string val)
        {
            if (val != null)
            {
                return val.Replace("'", "''");
            }
            else
            {
                return "";
            }
        }
        private string Section(int id)
        {
            IDictionary<int, string> section = new Dictionary<int, string>();
            section[1] = "Source";
            section[2] = "Governorates";
            section[3] = "Gender";
            section[4] = "Main Purposes";
            section[5] = "Shop issues";
            section[6] = "Protection issues";
            section[7] = "Cash comapartive study";
            section[8] = "Units";
            return section[id];
        }
    }
}
