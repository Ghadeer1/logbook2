﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using Newtonsoft.Json;
using System.Net.Mail;
using System.Net.Mime;

namespace HotlineMVC.Controllers
{
    public static class CommonFunctions
    {
        /// <summary>
        /// Common Functions to Boster's applications
        /// </summary>
        public static string ConnectionString = "LogBookDb";
        public static int RunCommand(string CommandName)
        {
            int rows = 0;
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings[ConnectionString].ToString()))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(CommandName, con);
                rows = cmd.ExecuteNonQuery();
            }
            return rows;
        }

        //Majd
        public static int RunSPCommand(SqlCommand command)
        {
            int rows = 0;
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings[ConnectionString].ToString()))
            {
                con.Open();               
                command.Connection = con;
                command.CommandType = CommandType.StoredProcedure;                
                rows = command.ExecuteNonQuery();
            }
            return rows;
        }

        public static string GetValueFromDb(string TableName)
        {
            String DataValue = "";

            try
            {
                using (SqlConnection conn = new SqlConnection())
                {
                    conn.ConnectionString = ConfigurationManager.ConnectionStrings[ConnectionString].ToString();
                    conn.Open();
                    SqlCommand cmd = new SqlCommand(TableName, conn);
                    DataValue = Convert.ToString(cmd.ExecuteScalar());
                }
            }
            finally { }
            return DataValue;
        }

        public static DataTable GetDataFromDb(string TableName)
        {
            DataTable dt = new DataTable();

            try
            {
                using (SqlConnection conn = new SqlConnection())
                {
                    conn.ConnectionString = ConfigurationManager.ConnectionStrings[ConnectionString].ToString();
                    conn.Open();
                    SqlCommand cmd = new SqlCommand(TableName, conn);
                    SqlDataAdapter sda = new SqlDataAdapter(cmd);
                    sda.Fill(dt);
                }
            }
            finally { }
            return dt;
        }


        //Majd
        public static DataTable GetDataFromDb_SP(SqlCommand cmd)
        {
            DataTable dt = new DataTable();

            try
            {
                using (SqlConnection conn = new SqlConnection())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    conn.ConnectionString = ConfigurationManager.ConnectionStrings[ConnectionString].ToString();
                    conn.Open();
                    cmd.Connection = conn;
                    SqlDataAdapter sda = new SqlDataAdapter(cmd);
                    sda.Fill(dt);
                }
            }
            finally { }
            return dt;
        }

        public static string GetCSV(string SQL)
        {
            StringBuilder sb = new StringBuilder();

            DataTable dt = GetDataFromDb(SQL);

            IEnumerable<string> columnNames = dt.Columns.Cast<DataColumn>().
                                              Select(column => column.ColumnName);
            sb.AppendLine(string.Join(",", columnNames));

            foreach (DataRow row in dt.Rows)
            {
                IEnumerable<string> fields = row.ItemArray.Select(field =>
                  string.Concat("\"", field.ToString().Replace("\"", "\"\""), "\""));
                sb.AppendLine(string.Join(",", fields));
            }
            return sb.ToString();
        }
        public static string GetEmailAddresses(string SQL)
        {
            StringBuilder sb = new StringBuilder();

            DataTable dt = GetDataFromDb(SQL);
            string addresses = "";

            foreach (DataRow row in dt.Rows)
            {
                addresses += row[0] + ";";
            }
            return addresses.Trim(new Char[] { ' ', ';' });
        }
        public static string GetJson(string SQL)
        {
            DataTable dt = new DataTable();
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings[ConnectionString].ToString()))
            {
                using (SqlCommand cmd = new SqlCommand(SQL, con))
                {
                    con.Open();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(dt);
                    System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                    Dictionary<string, object> row;
                    foreach (DataRow dr in dt.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in dt.Columns)
                        {
                            row.Add(col.ColumnName, dr[col]);
                        }
                        rows.Add(row);
                    }
                    return JsonConvert.SerializeObject(rows);
                }
            }
        }
        public static string GetJsonFromDataTable(DataTable dt)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            return JsonConvert.SerializeObject(rows);
        }
        public static void UpdateRecordAssignment(string SQL,string unit, string person)
        {
            DataTable dt = GetDataFromDb(SQL);
            string updateSQL = "";
            if (unit != "" && person != "")
            {
                foreach (DataRow row in dt.Rows)
                {
                    updateSQL = "Update logbook_records set ResponsibleUnit=" + unit + ", ResponsiblePerson=" 
                        + person +" where RecordNumber='"+row["RecordNumber"] + "'";
                    RunCommand(updateSQL);
                }
            }
        }
        public static void CloseRecords(string SQL, string Action)
        {
            DataTable dt = GetDataFromDb(SQL);
            string updateSQL = "";
            if (Action != "")
            {
                foreach (DataRow row in dt.Rows)
                {
                    updateSQL = "Update logbook_records set Status=2, Action="
                        + SqlValue(Action,true) + " where RecordNumber=" + row["RecordNumber"];
                    RunCommand(updateSQL);
                }
            }
        }
        public static void CloseUploadedRecords(DataTable dt)
        {
            string updateSQL = "";
            foreach (DataRow row in dt.Rows)
            {
                updateSQL = "Update logbook_records set Status=2, Action="
                    + SqlValue(row[1].ToString(), true) + " where RecordNumber=" + row[0];
                RunCommand(updateSQL);
            }
        }
        public static string GetExcel(string SQL,string tablename)
        {
            try
            {
                string filename = tablename + DateTime.Now.ToString("yyyyMMddHmmss") + ".xls";
                string path = HttpContext.Current.Server.MapPath("~/Content/Downloads/" + filename);
                using (StreamWriter writer = new StreamWriter(path))
                {
                    writer.WriteLine(@"<!DOCTYPE html>
                     <html>
                     <head>
                         <title> Exported data </title>
                        </head>
                        <body>");
                    DataTable dt = GetDataFromDb(SQL);
                    writer.WriteLine("<table border=1>");
                    writer.WriteLine("<tr style='background-color:darkblue;color:white'>");
                    for (int i = 0; i < dt.Columns.Count; i++)
                        writer.WriteLine("<td>" + dt.Columns[i].ColumnName + "</td>");
                    writer.WriteLine("</tr>");
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        writer.WriteLine("<tr>");
                        for (int j = 0; j < dt.Columns.Count; j++)
                            writer.WriteLine("<td>" + dt.Rows[i][j].ToString() + "</td>");
                        writer.WriteLine("</tr>");
                    }
                    writer.WriteLine(@"</table>
                            </body>
                            </html>");
                    return filename;
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        public static string MsgBox(string message, string pageAfter="#")
        {
            return @"<script>alert('" + message + @"');
                     window.location='" + pageAfter + @"';
                    </script>"; 
        }

        public static string getUploadPath(string DocType, FileUpload Upload)
        {
            string NewDocPath = "";
            try
            {
                if (Upload.PostedFile.FileName.Length > 0)
                {
                    //Split the file path to get file title
                    string[] FileTitle = Upload.PostedFile.FileName.Split('\\');

                    //Path where the documents are saved on the server
                    NewDocPath = DocType + "/" + FileTitle[FileTitle.Length - 1];

                    NewDocPath = HttpContext.Current.Server.MapPath(NewDocPath);
                    //Save the file to the disk
                    Upload.PostedFile.SaveAs(NewDocPath);
                }
            }
            catch (Exception ex)
            {
                string err = ex.Message;
            }
            return NewDocPath;
        }

        public static string getCriteria(string fieldName, string searchTerm, string compare)
        {
            string criteria = "";
            if (searchTerm.Length > 0)
            {
                string[] keywordList = searchTerm.Split(','); //comma separated keywords
                for (int i = 0; i < keywordList.Length; i++)
                {
                    criteria += compare + " " + fieldName + " LIKE '%" + keywordList[i] + "%'";
                }
            }
            return criteria;
        }

        public static string GetPermissions()
        {
            string permissions = "";
            string[] roles = Roles.GetRolesForUser(HttpContext.Current.User.Identity.Name);
            for (int i = 0; i < roles.Length; i++)
            {
                permissions += "'" + roles[i] + "',";
            }
            permissions = "(" + permissions.Trim(',') + ")";
            return permissions;
        }

        public static void LogActivity(string Activity,string Username="")
        {
            if (Username == "" && HttpContext.Current.Session["fullname"]!=null)
                Username = HttpContext.Current.Session["fullname"].ToString();
            string sql = string.Format(@"INSERT INTO [logbook_log]([activity],[doneby],[datetime])
                                             VALUES ({0},'{1}','{2}')",SqlValue(Activity,true), Username,DateTime.Now.ToString("yyyy-MM-dd H:m:s"));
            RunCommand(sql);
        }
        public static void LogRecordChanges(string RecordNumber,string Changes, string Username = "")
        {

            if (!(string.IsNullOrEmpty(Changes)))
            {
                if (Username == "" && HttpContext.Current.Session["fullname"] != null)
                    Username = HttpContext.Current.Session["fullname"].ToString();
                string sql = string.Format(@"INSERT INTO [logbook_record_changes](RecordNumber,Changes,doneby,datetime)
                                             VALUES ({0},{1},'{2}','{3}')", SqlValue(RecordNumber, true), SqlValue(Changes, true), Username, DateTime.Now.ToString("yyyy-MM-dd H:m:s"));
                RunCommand(sql);
            }

           
        }

        public static string Left(string text, int length)
        {
            if (length < 0)
                throw new ArgumentOutOfRangeException("length", length, "length must be > 0");
            else if (length == 0 || text.Length == 0)
                return "";
            else if (text.Length <= length)
                return text;
            else
                return text.Substring(0, length);
        }

        public static string RemoveSpecialCharacters(string str)
        {
            StringBuilder sb = new StringBuilder();
            foreach (char c in str)
            {
                if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') | c == '.' || c == '_')
                {
                    sb.Append(c);
                }
            }
            return sb.ToString().Trim();
        }

        public static string[] Split(string expression, char delimiter, char qualifier, bool ignoreCase)
        {
            try
            {

                expression = expression.Trim();

                if (ignoreCase)
                {

                    expression = expression.ToLower();

                    delimiter = char.ToLower(delimiter);

                    qualifier = char.ToLower(qualifier);

                }

                int len = expression.Length;

                List<string> list = new List<string>();

                int begField, endField; // text cursors

                for (begField = endField = 0; endField < len; begField = endField)
                {

                    char s = expression[endField];

                    bool entityContainsQualifiers = false;

                    // move to the delimiter

                    while (s != delimiter)
                    {

                        if (s != qualifier)
                        {

                            // consume and continue if possible

                            ++endField;

                            if (len <= endField) { break; }

                            else { s = expression[endField]; continue; }

                        }

                        #region Consume Text Within Two Qualifiers

                        // we have the qualifier symbol

                        // then move to the closing one

                        entityContainsQualifiers = true;

                        bool foundClosingQualifier = false;

                        for (endField = endField + 1; endField < len; ++endField)
                        {

                            s = expression[endField];

                            if (endField + 1 < len)
                            {

                                if (s == qualifier && expression[endField + 1] == delimiter)
                                {

                                    foundClosingQualifier = true;

                                    break;

                                }

                            }
                            else
                            {

                                if (s == qualifier)
                                {

                                    foundClosingQualifier = true;

                                    break;

                                }

                            }

                        }

                        if (false == foundClosingQualifier)
                        {

                            throw new ArgumentException

                                ("expression contains an unclosed qualifier symbol");

                        }

                        // consume the closing quantifier and continue if possible

                        ++endField;

                        if (len <= endField) { break; }

                        else { s = expression[endField]; continue; }

                        #endregion

                    }//while (s != delimiter)

                    // all what is in between begField and endField cursors is the entity...

                    string entity = expression.Substring(begField, endField - begField);

                    if (entityContainsQualifiers)
                    {

                        entity = entity.Replace(new string(qualifier, 1), "");

                    }

                    list.Add(entity);

                    // two possibilities:

                    // 1) we have found the delimiter

                    // 2) we have came to the end of the expression

                    // possibility (1)

                    if (s == delimiter)
                    {

                        // consume and continue if possible

                        ++endField;

                        if (len <= endField)
                        {

                            // this delimiter is the last symbol of the expression

                            // we should add the empty string as the last entity

                            // and leave

                            list.Add(string.Empty);

                            break;

                        }

                        else
                        {

                            // there are more entities in the expression

                            // proceed with collecting the entities

                            // note: s - initialization is done at the begining of the main cycle

                            continue;

                        }

                    }

                    else // possibility (2)
                    {
                        // leave the cycle
                        break;
                    }
                }
                return list.ToArray();
            }
            catch (Exception ex)
            {
                return ex.Message.Split(',');
            }
        }
        public static string ImportCSV(string FileName, string TableName, string AllowedFields, string AllowedTypes, string Season)
        {
            try
            {
                int b = 0;
                string msg = "";
                CultureInfo culture = CultureInfo.CreateSpecificCulture("en-GB");
                DateTimeStyles styles = DateTimeStyles.None;

                int LineNo = 1;
                string BadLineNos = "";
                DataSet ds = new DataSet();

                //Pass the file path and file name to the StreamReader constructor
                StreamReader sr = new StreamReader(FileName);

                string line = sr.ReadLine(); //first line contains fields
                if (line != null && "season," + line.ToLower() == AllowedFields.ToLower())
                {
                    string[] fields = AllowedFields.Split(',');
                    string[] types = AllowedTypes.Split(',');
                    using (SqlConnection conn = new SqlConnection())
                    {
                        conn.ConnectionString = ConfigurationManager.ConnectionStrings["RIDPConnectionString"].ToString();
                        conn.Open();
                        SqlDataAdapter sda = new SqlDataAdapter("Select " + AllowedFields + " from " + TableName, conn);
                        SqlCommandBuilder mySqlCommandBuilder = new SqlCommandBuilder(sda);
                        sda.MissingSchemaAction = MissingSchemaAction.AddWithKey;
                        sda.Fill(ds, TableName);
                        while (line != null)
                        {
                            line = sr.ReadLine();
                            LineNo++;
                            string[] data = Split(line, ',', '"', true);
                            DataRow dr = ds.Tables[TableName].NewRow();
                            if (data.Length == fields.Length - 1)
                            {
                                dr[0] = Season;
                                for (int i = 0; i < data.Length; i++)
                                {
                                    int j = i + 1;
                                    switch (types[j])
                                    {
                                        case "float":
                                            float floatValue;
                                            if (float.TryParse(data[i], out floatValue))
                                                dr[j] = floatValue;
                                            else
                                                dr[j] = SqlValue("NULL", true);
                                            break;
                                        case "datetime":
                                            DateTime dateValue = DateTime.Now;
                                            if (DateTime.TryParse(data[i], culture, styles, out dateValue))
                                                dr[j] = dateValue;
                                            else
                                                dr[j] = SqlValue("NULL", true);
                                            break;
                                        case "int":
                                            int intValue;
                                            if (Int32.TryParse(data[i], out intValue))
                                                dr[j] = intValue;
                                            else
                                                dr[j] = SqlValue("NULL", true);
                                            break;
                                        case "bit":
                                            if (data[i].ToLower().Trim() == "yes" || data[i] == "1")
                                                dr[j] = true;
                                            else if (data[i].ToLower().Trim() == "no" || data[i] == "0")
                                                dr[j] = false;
                                            else
                                                dr[j] = SqlValue("NULL", true);
                                            break;
                                        default:
                                            string stringValue = data[i];
                                            dr[j] = stringValue;
                                            break;
                                    }

                                }
                                ds.Tables[TableName].Rows.Add(dr);
                                sda.Update(ds, TableName);
                                ds.Tables[TableName].AcceptChanges();
                            }
                            else
                            {
                                //Bad record
                                b++;
                                BadLineNos = BadLineNos + LineNo + ", ";
                            }
                        }
                    }
                    sr.Close();
                    if (b == 0)
                        msg = "The data was usccessfully imported from the CSV file";
                    else
                        msg = "The data was imported with errors on line numbers: " + BadLineNos;
                }
                else
                {
                    msg = "ERROR! Incompatible data file. Please check the file";
                }
                return msg;
            }
            catch (Exception ex)
            {
                string msg = "ERROR! " + ex.Message;
                return msg;
            }
        }
        public static double Val(string Expression)
        {
            double dblValue;
            if (double.TryParse(Expression, out dblValue))
                return dblValue;
            else
                return 0;
        }
        public static int IntVal(string Expression)
        {
            int intValue;
            if (int.TryParse(Expression, out intValue))
                return intValue;
            else
                return 0;
        }
        public static string DateVal(string Expression)
        {
            DateTime dateValue;
            if (DateTime.TryParse(Expression, out dateValue))
                return dateValue.ToString("yyyy/MM/dd");
            else
                return "";
        }

        public static bool IsNumeric(string Number)
        {
            double myNum;
            if (Double.TryParse(Number, out myNum))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public static double NNull(object item, object NullValue)
        {
            if (item == null)
            {
                return Val(NullValue.ToString());
            }
            else
            {
                return Val(item.ToString());
            }
        }
        public static bool SearchTableForId(string Item, DataTable dt)
        {
            bool found = false;
            foreach (DataRow row in dt.Rows)
            {
                if (row[0].ToString() == Item)
                {
                    found = true;
                }
            }
            return found;
        }
        public static bool RecordExists(string Item, string FieldName, string TableName)
        {
            string SQL = String.Format("Select count(*) from [{0}] where [{1}] = '{2}'", TableName, FieldName, Item);
            string count = GetValueFromDb(SQL);
            if (int.Parse(count) > 0)
                return true;
            else
                return false;
        }
        public static bool RecordExists2(string SQL)
        {
            string count = GetValueFromDb(SQL);
            if (Val(count) > 0)
                return true;
            else
                return false;
        }
        public static List<SelectListItem> GetListFromDb(string SQL,bool addBlank=true)
        {
            DataTable dt = GetDataFromDb(SQL);
            List<SelectListItem> items = new List<SelectListItem>();
            if(addBlank)
                items.Add(new SelectListItem { Text = "", Value ="" });
            foreach (DataRow row in dt.Rows)
            {
                items.Add(new SelectListItem { Text = row["Text"].ToString(), Value = row["Value"].ToString()});
            }
            return items;
        }
        public static string JsonResponse(int Status,string StatusMessage, string Staff="")
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            Dictionary<string,string> response = new Dictionary<string,string>();
            response["status"] =Status.ToString();
            response["StatusMessage"] =StatusMessage;
            if(Staff != "")
                response["MyRecords"] = GetValueFromDb("Select count(*) from logbook_records where Staff='" + Staff + "' and date='" + DateTime.Now.ToString("yyyy-MM-dd") + "'");
            return JsonConvert.SerializeObject(response);
        }
        public static List<Dictionary<string, object>> GetList(string SQL)
        {
            DataTable dt = new DataTable();
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings[ConnectionString].ToString()))
            {
                using (SqlCommand cmd = new SqlCommand(SQL, con))
                {
                    con.Open();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(dt);
                    System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                    List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                    Dictionary<string, object> row;
                    foreach (DataRow dr in dt.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in dt.Columns)
                        {
                            row.Add(col.ColumnName, dr[col]);
                        }
                        rows.Add(row);
                    }
                    return rows;
                }
            }
        }
        public static bool CheckCCRights(string UserName,string UserRight)
        {
            string CCRights = CommonFunctions.GetValueFromDb(string.Format("Select aclogbook from access where login='{0}'", UserName));
            string[] AllRights = CCRights.Split('%');
            if (AllRights.Contains(UserRight))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public static void SendEmail(string From,string To, string Subject, string Message, string attachment="",string CC="")
        {
            try
            {
                //Change Back
                //To = "majd.wadi@wfp.org";
                //To = "oscar.lindow@wfp.org";
                SmtpClient client = new SmtpClient();
                MailMessage mailMessage = new MailMessage();
                mailMessage.From = new MailAddress(From);
                mailMessage.IsBodyHtml = true;

                string[] Tos = To.Split(';');
                for (int i = 0; i < Tos.Length; i++)
                {
                    if (Tos[i].Contains("@"))
                        mailMessage.To.Add(Tos[i]);
                }

                string[] CCs = CC.Split(';');
                for (int i = 0; i < CCs.Length; i++)
                {
                    if (CCs[i].Contains("@"))
                        mailMessage.CC.Add(CCs[i]);
                }

                mailMessage.Subject = Subject;
                mailMessage.Body = Message;
                if (attachment != "")
                {
                    Attachment data = new Attachment(attachment, MediaTypeNames.Application.Octet);
                    mailMessage.Attachments.Add(data);
                }

                client.Send(mailMessage);
            }
            catch
            {
                
            }
        }
        public static string GetStaffEmail(string staffid)
        {
            return GetValueFromDb("Select email from staff where Id='" + staffid + "'");
        }
        public static string GetStaffName(string staffid)
        {
            return GetValueFromDb("Select name from staff where Id='" + staffid + "'");
        }
        public static string GetFirstNameFromEmail(string email)
        {
            return CultureInfo.CurrentCulture.TextInfo.ToTitleCase(email.Split('.')[0].ToLower());
        }
        public static string RecordView(string record)
        {
            DataTable dt = GetDataFromDb("Select RecordNumber, Staff, Date,Time, GovernorateDesc, CaseNumber, PhoneNumber, GenderDesc, Action  from logbook_records_view where RecordNumber='" + record + "'");
            DataRow row = dt.Rows[0];
            string view = string.Format(@"<table border=0>
                <tr>
                    <td style=vertical-align:top>

                            <table style=width:400px border=1 style=border-collapse:collapse>
                                <tr>
                                    <td align=center style='color:white;background-color:#2A93FC;font-weight:bold' colspan=2>Ticket Info.</td>
                                </tr>
                                <tr>
                                    <td>Ticket Number:</td>
                                    <td>{0}</td>
                                </tr>
                                <tr>
                                    <td>Recorded by:</td>
                                    <td>{1}</td>
                                </tr>
                                <tr>
                                    <td>Date:</td>
                                    <td>{2}</td>
                                </tr>
                                <tr>
                                    <td>Time:</td>
                                    <td>{3}</td>
                                </tr>
                               
                                                              
                                

                                
                            </table>
                    </td>
                    
                            </table>
                    </td>
                </tr>
            </table>",
            row["RecordNumber"].ToString(),
            row["Staff"].ToString(),
            Convert.ToDateTime(row["Date"].ToString()).ToString("dd-MM-yyyy"),
            row["Time"].ToString());
            //row["GovernorateDesc"].ToString(),
            //row["CaseNumber"].ToString(),
            //row["PhoneNumber"].ToString(),            
            //row["GenderDesc"].ToString(),            
            //row["Action"].ToString());           
            return view;
        }


        public static string RecordViewForReport(string record)
        {
            DataTable dt = GetDataFromDb("Select RecordNumber, Staff, Date,Time, GovernorateDesc, CaseNumber, PhoneNumber, GenderDesc, Action, Title, MainFindings  from logbook_records_view where RecordNumber='" + record + "'");
            DataRow row = dt.Rows[0];
            string view = string.Format(@"<table border=0>
                <tr>
                    <td style=vertical-align:top>

                            <table style=width:400px border=1 style=border-collapse:collapse>
                                <tr>
                                    <td align=center style='color:white;background-color:#2A93FC;font-weight:bold' colspan=2>Report Info.</td>
                                </tr>
 <tr>
                                    <td>Report Title:</td>
                                    <td>{5}</td>
                                </tr>
                                <tr>
                                    <td>Report Number:</td>
                                    <td>{0}</td>
                                </tr>
                                <tr>
                                    <td>Recorded by:</td>
                                    <td>{1}</td>
                                </tr>
                                <tr>
                                    <td>Date:</td>
                                    <td>{2}</td>
                                </tr>
                                <tr>
                                    <td>Time:</td>
                                    <td>{3}</td>
                                </tr>
<tr>
                                    <td>Main Findings:</td>
                                    <td>{4}</td>
                                </tr>
                               
                                                              
                                

                                
                            </table>
                    </td>
                    
                            </table>
                    </td>
                </tr>
            </table>",
            row["RecordNumber"].ToString(),
            row["Staff"].ToString(),
            Convert.ToDateTime(row["Date"].ToString()).ToString("dd-MM-yyyy"),
            row["Time"].ToString(),
            row["MainFindings"].ToString(),
            row["Title"].ToString()

            );

            //row["GovernorateDesc"].ToString(),
            //row["CaseNumber"].ToString(),
            //row["PhoneNumber"].ToString(),            
            //row["GenderDesc"].ToString(),            
            //row["Action"].ToString());           
            return view;
        }



        public static string RecordView(string record, ref string CreatorEmail)
        {
            DataTable dt = GetDataFromDb("Select RecordNumber, Staff, Date,Time, GovernorateDesc, CaseNumber, PhoneNumber, GenderDesc, Action  from logbook_records_view where RecordNumber='" + record + "'");
            DataRow row = dt.Rows[0];
            string view = string.Format(@"<table border=0>
                <tr>
                    <td style=vertical-align:top>

                            <table style=width:400px border=1 style=border-collapse:collapse>
                                <tr>
                                    <td align=center style='color:white;background-color:#2A93FC;font-weight:bold' colspan=2>Ticket Info.</td>
                                </tr>
                                <tr>
                                    <td>Ticket Number:</td>
                                    <td>{0}</td>
                                </tr>
                                <tr>
                                    <td>Recorded by:</td>
                                    <td>{1}</td>
                                </tr>
                                <tr>
                                    <td>Date:</td>
                                    <td>{2}</td>
                                </tr>
                                <tr>
                                    <td>Time:</td>
                                    <td>{3}</td>
                                </tr>
                               
                                                              
                                

                                
                            </table>
                    </td>
                    
                            </table>
                    </td>
                </tr>
            </table>",
            row["RecordNumber"].ToString(),
            row["Staff"].ToString(),
            Convert.ToDateTime(row["Date"].ToString()).ToString("dd-MM-yyyy"),
            row["Time"].ToString());
            //row["GovernorateDesc"].ToString(),
            //row["CaseNumber"].ToString(),
            //row["PhoneNumber"].ToString(),            
            //row["GenderDesc"].ToString(),            
            //row["Action"].ToString());   
            CreatorEmail = row["Staff"].ToString() + "@wfp.org";
            return view;
        }
        public static string GetJSArray(string SQL)
        {
            DataTable dt = GetDataFromDb(SQL);
            string txt = "";
            foreach (DataRow row in dt.Rows)
            {
               txt+=row[0]+"','";
            }
            txt = txt.Substring(0, txt.Length - 2);
            txt = "['" + txt + "]";
            return txt;
        }
        public static string SqlValue(string val, bool quote)
        {
            string tmp = "";
            if (quote)
                tmp = SqlStr(val);
            else
                tmp = val;
            if (tmp == "")
                tmp = "NULL";
            else if (quote)
                tmp = "'" + tmp + "'";
            return tmp;
        }
        public static string SqlStr(string val)
        {
            if (val != null)
            {
                return val.Replace("'", "''");
            }
            else
            {
                return "";
            }
        }
        public static string GetStaffIdFromLogin(string login)
        {
            string ret = "0";
            if (login.Length > 0)
            {
                string SQL = "select id from staff where email='" + login + "@wfp.org'";
                ret=GetValueFromDb(SQL);
            }
            return ret;
        }
        public static string GetEmailAddressesFromUserRight(string right)
        {
            string SQL = "select dbo.GROUP_CONCAT(login) from access where aclogbook like '"+right+"'";
            string addresses= GetValueFromDb(SQL);
            addresses = addresses.Replace(",", "@wfp.org;");
            return addresses;
        }
        public static DataRow GetRowFromDb(string TableName)
        {
            DataTable dt = new DataTable();

            try
            {
                using (SqlConnection conn = new SqlConnection())
                {
                    conn.ConnectionString = ConfigurationManager.ConnectionStrings[ConnectionString].ToString();
                    conn.Open();
                    SqlCommand cmd = new SqlCommand(TableName, conn);
                    SqlDataAdapter sda = new SqlDataAdapter(cmd);
                    sda.Fill(dt);
                }
            }
            finally { }
            if (dt.Rows.Count > 0)
                return dt.Rows[0];
            else
                return null;
        }
        public static string GetTableFromSQL(string SQL)
        {
            StringBuilder sb = new StringBuilder();

            DataTable dt = GetDataFromDb(SQL);
            string html = "<table id='report-table'><thead>";
            //add header row
            html += "<tr>";
            for (int i = 0; i < dt.Columns.Count; i++)
                html += "<th>" + dt.Columns[i].ColumnName + "</th>";
            html += "</tr></thead><tbody>";
            //add rows
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                html += "<tr>";
                for (int j = 0; j < dt.Columns.Count; j++)
                    html += "<td>" + dt.Rows[i][j].ToString() + "</td>";
                html += "</tr>";
            }
            html += "</tbody></table>";
            return html;
        }
        public static string DataTable(string SQL, string[] aColumns)
        {
            return "";
            //            var param = HttpContext.Current.Request.QueryString;
            //            /* Ordering */
            //            string sOrder = "";
            //            if (param["iSortCol_0"]!=null)
            //            {
            //                sOrder = "ORDER BY  ";
            //                for ( int i = 0; i < int.Parse(param["iSortingCols"]); i++ ) {
            //                    if ( param["bSortable_"+int.Parse(param["iSortCol_"+$i])] == "true" ) {
            //                        sOrder+= aColumns[int.Parse(param["iSortCol_"+$i])]+"
            //                                          "+addslashes( param["sSortDir_"+$i] ) +", ";
            //                              }
            //                }
            //    $sOrder = substr_replace( $sOrder, "", -2);
            //                if ( $sOrder == "ORDER BY" ) {
            //        $sOrder = "";
            //                }
            //            }

            ///* Filtering */
            //$sWhere = "";
            //            if (isset($_GET['sSearch']) && $_GET['sSearch'] != "" ) {
            //    $sWhere = "WHERE (";
            //                for ( $i = 0; $i < count($aColumns) ; $i++ ) {
            //        $sWhere.= $aColumns[$i]." LIKE '%".addslashes( $_GET['sSearch'])."%' OR ";
            //                }
            //    $sWhere = substr_replace( $sWhere, "", -3);
            //    $sWhere.= ')';
            //            }
            //            /* Individual column filtering */
            //            for ( $i = 0; $i < count($aColumns) ; $i++ ) {
            //                if (isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )  {
            //                    if ( $sWhere == "" ) {
            //            $sWhere = "WHERE ";
            //                    } else {
            //            $sWhere.= " AND ";
            //                    }
            //        $sWhere.= $aColumns[$i]." LIKE '%".addslashes($_GET['sSearch_'.$i])."%' ";
            //                }
            //            }

            ///* Paging */
            //$top = (isset($_GET['iDisplayStart'])) ? ((int)$_GET['iDisplayStart']):0;
            //$limit = (isset($_GET['iDisplayLength'])) ? ((int)$_GET['iDisplayLength'] ):10;
            //    $sQuery = "SELECT TOP $limit ".implode(",",$aColumns)."
            //        FROM $sTable as t1
            //        $sWhere ".(($sWhere=="")?" WHERE ":" AND ")." $sIndexColumn NOT IN
            //        (
            //            SELECT $sIndexColumn FROM
            //            (
            //                SELECT TOP $top ".implode(", ",$aColumns)."
            //                FROM $sTable as t2
            //                $sWhere
            //                $sOrder
            //            )
            //            as [virtTable]
            //        )
            //        $sOrder";
            //$rResult = sqlsrv_query($gaSql['link'],$sQuery) or die("$sQuery: ".sqlsrv_errors());

            //$sQueryCnt = "SELECT * FROM $sTable as t3 $sWhere";
            //$rResultCnt = sqlsrv_query( $gaSql['link'], $sQueryCnt,$params, $options) or die (" $sQueryCnt: ".sqlsrv_errors());
            //$iFilteredTotal = sqlsrv_num_rows( $rResultCnt);

            //$sQuery = " SELECT * FROM $sTable as t4 ";
            //$rResultTotal = sqlsrv_query( $gaSql['link'], $sQuery,$params, $options) or die(sqlsrv_errors());
            //$iTotal = sqlsrv_num_rows( $rResultTotal);

            //$output = array(
            //    "sEcho" => int.Parse(isset($_GET['sEcho']) ?$_GET['sEcho']:0),
            //    "iTotalRecords" => $iTotal,
            //    "iTotalDisplayRecords" => $iFilteredTotal,
            //    "aaData" => array()
            //);

            //            while ( $aRow = sqlsrv_fetch_array( $rResult) ) {
            //    $row = array();
            //                for ( $i = 0; $i < count($aColumns) ; $i++ ) {
            //                    if ( $aColumns[$i] != ' ' ) {
            //            $column_name = str_replace("[", "",$aColumns[$i]);
            //            $column_name = str_replace("]", "",$column_name);
            //            $v = $aRow[ $column_name];
            //            $v = mb_check_encoding($v, 'UTF-8') ? $v: utf8_encode($v);
            //            $row[]=$v;
            //                    }
            //                }
            //                If(!empty($row)) { $output['aaData'][] = $row; }
            //            }
            //            echo json_encode( $output );
        }
    }

}
