﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.DirectoryServices.AccountManagement;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace HotlineMVC.Controllers
{
    public class HomeController : Controller
    {
        
        public ActionResult Index()
        {
            string OpenRecords = getOpenRecords();
            string AllRecords = getAllRecords();
            string ClosedRecords = getClosedRecords();
            double ServiceLevel= Math.Round((CommonFunctions.Val((ClosedRecords)) / CommonFunctions.Val(AllRecords)) * 100,1);
            ViewBag.OpenRecords = OpenRecords;
            ViewBag.AllRecords = AllRecords;
            ViewBag.Tickets = getTickets();
            ViewBag.MonitoringNotes = getMonitoringNotes();
            ViewBag.DailyRecordNumbers = getDailyRecordNumbers();
            ViewBag.PeriodicReport = getPeriodicReport();
            ViewBag.MonthlyReport = getMonthlyReport();
            ViewBag.RecordsByGovernorate = getGovernorateReport();
            ViewBag.GenderReport = getGenderReport();
            return View();
        }
        public ActionResult Login()
        {
            return View();
        }
        public ActionResult Logout()
        {
            Session.Abandon();
            FormsAuthentication.SignOut();
            return RedirectToAction("Login");
        }

        public ActionResult ProcessLogin()
        {
            string UserName = Request.Form["user"].Trim().Replace("@wfp.org", "");
            string Password = Request.Form["pass"].Trim();
            try
            {
                using (PrincipalContext pc = new PrincipalContext(ContextType.Domain, "GLOBAL.WFP.ORG"))
                {
                    //validate the credentials
                    if (UserName.Length == 0 || Password.Length == 0)
                    {
                        return RedirectToAction("Login", new { error = "NoCredentials" });
                    }
                    else
                    {
                        bool isValid = false;
                        string UserType = GetUserType(UserName);
                    if (UserType == "1")
                        isValid = pc.ValidateCredentials(UserName, Password);
                        else
                            isValid = AuthenticateLocalUser(UserName,Password);
                        if (isValid)
                        {
                            if (UserHasRights(UserName))
                            {
                                Session["LoggedIn"] = true;
                                Session["StaffId"] = CommonFunctions.GetStaffIdFromLogin(UserName);
                                Session["Username"] = UserName;
                                Session["LogBookRights"] = GetLogBookRights(UserName);
                                FormsAuthentication.SetAuthCookie(UserName, false);
                                if (Session["username"] != null)
                                {
                                    string[] names = Session["username"].ToString().Split('.');
                                    string firstname = names[0];
                                    string lastname = names.Length > 1 ? names[1] : "";
                                    Session["fullname"] = ToTitleCase(firstname) + " " + ToTitleCase(lastname);
                                    CommonFunctions.LogActivity("Successfully logged in", Session["fullname"].ToString());

                                    //Customized Permissions -- Majd
                                    SqlCommand cmd = new SqlCommand();
                                    cmd.CommandText = "LoadUserPermissions";
                                    cmd.Parameters.AddWithValue("Login", Session["Username"]);
                                    DataTable dt = CommonFunctions.GetDataFromDb_SP(cmd);
                                    if (dt.Rows.Count > 0)
                                    {
                                        UserInfo ui = new UserInfo();                                        
                                        ui.Username = Session["username"].ToString();
                                        ui.LogBookRights = Session["LogBookRights"].ToString();
                                        ui.FullName = Session["fullname"].ToString();
                                        ui.Programms = dt.Rows[0]["Programms"].ToString().Split(',');
                                        ui.CanViewTicket = Convert.ToBoolean(dt.Rows[0]["CanViewTicket"]);
                                        ui.CanEditTicket = Convert.ToBoolean(dt.Rows[0]["CanEditTicket"]);
                                        ui.CanViewNote = Convert.ToBoolean(dt.Rows[0]["CanViewNote"]);
                                        ui.CanEditNote = Convert.ToBoolean(dt.Rows[0]["CanEditNote"]);
                                        ui.CanViewDataVisualization = Convert.ToBoolean(dt.Rows[0]["CanViewDataVisualization"]);
                                        ui.CanCreateCustomReport = Convert.ToBoolean(dt.Rows[0]["CanCreateCustomReport"]);
                                        ui.CanViewLog = Convert.ToBoolean(dt.Rows[0]["CanViewLog"]);
                                        ui.CanAddTicket = Convert.ToBoolean(dt.Rows[0]["CanAddTicket"]);
                                        ui.CanAddMonitoringNote = Convert.ToBoolean(dt.Rows[0]["CanAddMonitoringNote"]);
                                        ui.ProtectionAdvisor = Convert.ToBoolean(dt.Rows[0]["ProtectionAdvisor"]);
                                        ui.GenderFocalPoint = Convert.ToBoolean(dt.Rows[0]["GenderFocalPoint"]);
                                        ui.ResponsibleForUnits = dt.Rows[0]["ResponsibleForUnits"].ToString();
                                        Session.Add("UserInfo", ui);
                                    }
                                    else
                                    {
                                        return RedirectToAction("Login", new { error = "NoRights" });
                                    }
                                }
                                if (Request.Form["destination"] != "")
                                {
                                    return Redirect(Request.Form["destination"]);
                                }
                                else
                                {
                                    if (UserType == "1")
                                        return RedirectToAction("Index");
                                    else
                                        return RedirectToAction("Index","Helpdesk");
                                }
                            }
                            else
                            {
                                CommonFunctions.LogActivity("Failed to login because of insufficient rights", UserName);
                                return RedirectToAction("Login", new { error = "NoRights" });
                            }
                        }
                        else
                        {
                            CommonFunctions.LogActivity("Failed to login because of invalid ceredentials", UserName);
                            return RedirectToAction("Login", new { error = "InvalidCredentials" });
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                CommonFunctions.LogActivity("An error ocurred while trying to login"+ex.Message, UserName);
                return RedirectToAction("Login", new { error = ex.Message });
            }
        }
        private string GetUserType(string login)
        {
            string SQL = string.Format("select usertype from access where login='{0}' and status='Y'", login);
            return CommonFunctions.GetValueFromDb(SQL);
        }
        bool AuthenticateLocalUser(string UserName, string Password)
        {
            string SQL = string.Format("select count(*) from access where status='Y' and login='{0}' and passwd=hashbytes('md5','{1}')", UserName, Password);
            string count= CommonFunctions.GetValueFromDb(SQL);
            return (int.Parse(count) > 0);
        }
 
        private string ToTitleCase(string str)
        {
            return CultureInfo.CurrentCulture.TextInfo.ToTitleCase(str.ToLower());
        }
        private bool UserHasRights(string Login)
        {
            string MainRights = CommonFunctions.GetValueFromDb(string.Format("Select acmain from access where login='{0}'", Login));
            string[] AllRights = MainRights.Split('%');
            if (AllRights.Contains("CC"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private string GetLogBookRights(string Login)
        {
            return CommonFunctions.GetValueFromDb(string.Format("Select aclogbook from access where login='{0}'", Login));
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";
            return View();
        }
        private string getOpenRecords()
        {
            return CommonFunctions.GetValueFromDb("Select count(*) from logbook_records where status=1");
        }
        private string getClosedRecords()
        {
            return CommonFunctions.GetValueFromDb("Select count(*) from logbook_records where status=3");
        }
        private string getAllRecords()
        {
            return CommonFunctions.GetValueFromDb("Select count(*) from logbook_records");
        }
        private string getShopRecords()
        {
            return CommonFunctions.GetValueFromDb("Select count(*) from logbook_records where ShopProblem>0");
        }
        private string getTickets()
        {
            return CommonFunctions.GetValueFromDb("Select count(*) from logbook_records where RecordType=1");
        }
        private string getMonitoringNotes()
        {
            return CommonFunctions.GetValueFromDb("Select count(*) from logbook_records where RecordType=2");
        }
        private DataTable getDailyRecordNumbers()
        {
            int Month = DateTime.Now.Month;
            string SQL = @"select [Date],logbook_status.[StatusDesc],count(*) from logbook_records  
                inner join [dbo].[logbook_status] on logbook_records.Status=logbook_status.id
                where month(date)="+Month+"group by Date, logbook_status.StatusDesc";
            return CommonFunctions.GetDataFromDb(SQL);
        }
        private IDictionary<string, string> getPeriodicReport()
        {
            int Month = DateTime.Now.Month;
            int Year = DateTime.Now.Year;
            string today = DateTime.Now.ToString("yyyy-MM-dd");
            string todayRecords = CommonFunctions.GetValueFromDb(string.Format("Select count(*) from logbook_records where date='{0}'", today));
            string yesterday= DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd");
            string yesterdayRecords = CommonFunctions.GetValueFromDb(string.Format("Select count(*) from logbook_records where date='{0}'", yesterday));
            string weekstart = DateTime.Now.StartOfWeek(DayOfWeek.Sunday).ToString("yyyy-MM-dd");
            string weekRecords = CommonFunctions.GetValueFromDb(string.Format("Select count(*) from logbook_records where date between '{0}' and '{1}'", weekstart,today));
            string monthRecords = CommonFunctions.GetValueFromDb(string.Format("Select count(*) from logbook_records where month(date)={0} and year(date)={1}", Month,Year));
            string yearRecords = CommonFunctions.GetValueFromDb(string.Format("Select count(*) from logbook_records where year(date)={0}",Year));
            string allRecords = getAllRecords();
            IDictionary<string, string> data = new Dictionary<string, string>();
            data["today"] = string.Format("{0:c}", todayRecords);
            data["yesterday"] = string.Format("{0:c}", yesterdayRecords);
            data["week"] = string.Format("{0:c}",weekRecords);
            data["month"] = string.Format("{0:c}",monthRecords);
            data["year"] = string.Format("{0:c}",yearRecords);
            data["all"]= string.Format("{0:c}",allRecords);
            return data;
        }
        private string getMonthlyReport()
        {
            DataTable dt = new DataTable();
            string SQL = @"select format(date,'MMM-yy') as Month, count(*) as Number from logbook_records 
                group by format(date,'MMM-yy'),Left(date,7)
                order by Left(date,7)";
            return CommonFunctions.GetJson(SQL);
        }
        private string getGovernorateReport()
        {
            string SQL = @"SELECT GovernorateDesc Governorate, COUNT(GovernorateDesc) Number FROM dbo.logbook_records_view GROUP BY GovernorateDesc";
            return CommonFunctions.GetJson(SQL);
        }
        private string getGenderReport()
        {
            string SQL = @"select logbook_gender.GenderDesc as Gender, count(*) as Number from logbook_records 
                inner join logbook_gender on logbook_records.Gender=logbook_gender.ID
                group by logbook_gender.GenderDesc";
            return CommonFunctions.GetJson(SQL);
        }
    }
    public static class DateTimeExtensions
    {
        public static DateTime StartOfWeek(this DateTime dt, DayOfWeek startOfWeek)
        {
            int diff = dt.DayOfWeek - startOfWeek;
            if (diff < 0)
            {
                diff += 7;
            }
            return dt.AddDays(-1 * diff).Date;
        }
    }
}