﻿using System.Collections;
using System.Collections.Generic;

namespace HotlineMVC.Controllers
{
    public class UserInfo
    {
        public int StaffId { get; set; }
        public string Username { get; set; }
        public string LogBookRights { get; set; }
        public string FullName { get; set; }

        public string[] Programms { get; set; }
        public bool CanViewTicket { get; set; }
        public bool CanEditTicket { get; set; }
        public bool CanViewNote { get; set; }
        public bool CanEditNote { get; set; }
        public bool CanViewDataVisualization { get; set; }
        public bool CanCreateCustomReport { get; set; }
        public bool CanViewLog { get; set; }
        public bool CanAddTicket { get; set; }
        public bool CanAddMonitoringNote { get; set; }
        public bool ProtectionAdvisor { get; set; }
        public bool GenderFocalPoint { get; set; }
        public string ResponsibleForUnits { get; set; }

    }
}