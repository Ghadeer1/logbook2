﻿msg = "";
error = false;
$(function () {
    pageGrids.ordersGrid.addFilterWidget(new CustomersFilterWidget());
    pageGrids.ordersGrid.addFilterWidget(new RecordsTypeWidget());
    pageGrids.ordersGrid.addFilterWidget(new SourcesWidget());
    pageGrids.ordersGrid.addFilterWidget(new StatusesWidget());
    pageGrids.ordersGrid.addFilterWidget(new StaffWidget());
	pageGrids.ordersGrid.addFilterWidget(new GovernorateWidget());	
	pageGrids.ordersGrid.addFilterWidget(new ProgramWidget());
	pageGrids.ordersGrid.addFilterWidget(new DistrictWidget());
	pageGrids.ordersGrid.addFilterWidget(new SubProgramWidget());
	pageGrids.ordersGrid.addFilterWidget(new ResponsibleUnitWidget());
	pageGrids.ordersGrid.addFilterWidget(new CategoryWidget());
});
var records = [];
var updated = false;
var updated = false;
$("#success-alert").hide();
//$("#number-of-records").html(numberWithCommas($(".grid-itemscount-caption").html()));

function changeProgramme() {
	$("#SubProgramme-tr").show();
	cascadeDropdown("Programme", "SubProgramme", SubProgramme);

	if (form1.RecordType.value != 2) {
		$("#Category-tr").show();
	}


	cascadeDropdown("Programme", "Category", Category);

	//Fill sub activites
	for (var i = 2; i <= 10; i++) {
		$('#subactivity' + i).empty();
	}
	
	$('#subactivity').empty();
	for (var i = 2; i <= 10; i++) {
		$('#numberofforms' + i).empty();
	}
	
	$('#numberofforms').empty();
	var prog = $('#Programme').val();

	if (prog == 2) { // res
		$('#subactivity').append($('<option>', { value: '', text: '' }));
		$('#subactivity').append($('<option>', { value: 'PDM', text: 'PDM' }));
		$('#subactivity').append($('<option>', { value: 'BCM', text: 'BCM' }));
		$('#subactivity').append($('<option>', { value: 'PAB', text: 'PAB' }));
		$('#subactivity').append($('<option>', { value: 'SCI', text: 'SCI' }));
		$('#subactivity').append($('<option>', { value: 'Activity Report', text: 'Activity Report' }));
		$('#subactivity').append($('<option>', { value: 'ABI', text: 'ABI' }));
		$('#subactivity').append($('<option>', { value: 'Cach Distribution', text: 'Cach Distribution' }));
		$('#numberofforms').append($('<option>', { value: 0, text: '0' }));
		$('#numberofforms').append($('<option>', { value: 1, text: '1' }));
		$('#numberofforms').append($('<option>', { value: 2, text: '2' }));
		$('#numberofforms').append($('<option>', { value: 3, text: '3' }));
		$('#numberofforms').append($('<option>', { value: 4, text: '4' }));
		$('#numberofforms').append($('<option>', { value: 5, text: '5' }));

		
	}
	else if (prog == 4) { // School
		$('#subactivity').append($('<option>', { value: '', text: '' }));
		$('#subactivity').append($('<option>', { value: 'Camp Checklist DB', text: 'Camp Checklist DB' }));
		$('#subactivity').append($('<option>', { value: 'HC Checklist DB', text: 'HC Checklist DB' }));
		$('#subactivity').append($('<option>', { value: 'HK Principle', text: 'HK Principle' }));
		$('#subactivity').append($('<option>', { value: 'HK Kitchen', text: 'HK Kitchen' }));
		$('#subactivity').append($('<option>', { value: 'HK Students', text: 'HK Students' }));
		$('#subactivity').append($('<option>', { value: 'Cach Distribution', text: 'Cach Distribution' }));
		$('#numberofforms').append($('<option>', { value: 0, text: '0' }));
		$('#numberofforms').append($('<option>', { value: 1, text: '1' }));
		$('#numberofforms').append($('<option>', { value: 2, text: '2' }));
		$('#numberofforms').append($('<option>', { value: 3, text: '3' }));
		$('#numberofforms').append($('<option>', { value: 4, text: '4' }));
		$('#numberofforms').append($('<option>', { value: 5, text: '5' }));
	}


	if (prog == 2) { // res		
		for (var i = 2; i <= 10; i++) {	
			$('#subactivity' + i).append($('<option>', { value: '', text: '' }));
			$('#subactivity' + i).append($('<option>', { value: 'PDM', text: 'PDM' }));
			$('#subactivity' + i).append($('<option>', { value: 'BCM', text: 'BCM' }));
			$('#subactivity' + i).append($('<option>', { value: 'PAB', text: 'PAB' }));
			$('#subactivity' + i).append($('<option>', { value: 'SCI', text: 'SCI' }));
			$('#subactivity' + i).append($('<option>', { value: 'Activity Report', text: 'Activity Report' }));
			$('#subactivity' + i).append($('<option>', { value: 'ABI', text: 'ABI' }));
			$('#subactivity' + i).append($('<option>', { value: 'Cach Distribution', text: 'Cach Distribution' }));
			$('#numberofforms' + i).append($('<option>', { value: 10, text: '0' }));
			$('#numberofforms' + i).append($('<option>', { value: 1, text: '1' }));
			$('#numberofforms' + i).append($('<option>', { value: 2, text: '2' }));
			$('#numberofforms' + i).append($('<option>', { value: 3, text: '3' }));
			$('#numberofforms' + i).append($('<option>', { value: 4, text: '4' }));
			$('#numberofforms' + i).append($('<option>', { value: 5, text: '5' }));
		}
	}
	else if (prog == 4) { // School
		for (var i = 2; i <= 10; i++) {
			$('#subactivity' + i).append($('<option>', { value: '', text: '' }));
			$('#subactivity' + i).append($('<option>', { value: 'Camp Checklist DB', text: 'Camp Checklist DB' }));
			$('#subactivity' + i).append($('<option>', { value: 'HC Checklist DB', text: 'HC Checklist DB' }));
			$('#subactivity' + i).append($('<option>', { value: 'HK Principle', text: 'HK Principle' }));
			$('#subactivity' + i).append($('<option>', { value: 'HK Kitchen', text: 'HK Kitchen' }));
			$('#subactivity' + i).append($('<option>', { value: 'HK Students', text: 'HK Students' }));
			$('#subactivity' + i).append($('<option>', { value: 'Cach Distribution', text: 'Cach Distribution' }));
			$('#numberofforms' + i).append($('<option>', { value: 0, text: '0' }));
			$('#numberofforms' + i).append($('<option>', { value: 1, text: '1' }));
			$('#numberofforms' + i).append($('<option>', { value: 2, text: '2' }));
			$('#numberofforms' + i).append($('<option>', { value: 3, text: '3' }));
			$('#numberofforms' + i).append($('<option>', { value: 4, text: '4' }));
			$('#numberofforms' + i).append($('<option>', { value: 5, text: '5' }));
		}
	}

	
	
}
function changeSource(val) {
   
    $("#SubSource-tr").show();
    $("#trSourceFurtherInfo").show();  
   
    

    //if (val == 2) {       
    //    $("#trOtherExternalSource").show();    }
    //else
    //{
      
    //    $("#trOtherExternalSource").hide();
    //}
    cascadeDropdown("Source", "SubSource", SubSource);
    
    
}
function changeLocationType(value, ind) {
   
    if (value == 1 || value==2) {
        

        if (ind == 1) {
            cascadeDropdown("LocationType", "Governorate", Governorate);
        }
        else {
            cascadeDropdown("LocationType" + ind, "Governorate" + ind, Governorate);

        }

    }
    
}
function numberOfLocationsSelect(v)
{
	

    for (var i = 1; i <= 10; i++) {
        $('#trLoc' + i).hide();
        $('#trLat' + i).hide();
        $('#trLong' + i).hide();
    }

    for ( var i  =1; i<=v; i++)
    {
        $('#trLoc' + i).show();
        $('#trLat' + i).show();
        $('#trLong' + i).show();
    }
}


function numberOfActivitiesSelect(v) {

	for (var i = 1; i <= 10; i++) {

		$('#trSubActivity' + i).hide();
		
	}

	for (var i = 1; i <= v; i++) {
		$('#trSubActivity' + i).show();
	}
}

function changeCategory() {
    $("#SubCategory-tr").show();
    cascadeDropdown("Category", "SubCategory", SubCategory);
}
function getPurposeInfo(details) {
    var html = "";
    html = "<table class='table table-striped table-bordered table-condensed'>"
            + "<tr><td style='font-size:10px;width:150px'>Case Priority:</td><td style='font-size:10px'>" + details["RankingDesc"] + "</td></tr>"
            + "<tr><td style='font-size:10px'>Action to be taken:</td><td style='font-size:10px'>" + details["TimeFrameDesc"] + "</td></tr>"
            + "<tr><td style='font-size:10px'>Email will be sent to:</td><td style='font-size:10px'>" + details["EmailTo"] + "</td></tr>"
            + "<tr><td style='font-size:10px'>Email CC:</td><td style='font-size:10px'>" + details["EmailCC"] + "</td></tr></table>";
    $("#purposeInfo").html(html);
}
function checkAll(checked) {
    var checks = document.querySelectorAll('#gridview input[type="checkbox"]');
    for (var i = 0; i < checks.length; i++) {
        var check = checks[i];
        if (checked) {
            check.checked = true;
            $.uniform.update();
            selectItem(checked, check.name);
        }
        else {
            check.checked = false;
            $.uniform.update();
            removeItem(checked, check.name);
        }
    }
}
function selectItem(checked, item) {
    if (checked) {
        records.push("'" + item + "'");
    }
    else {
        removeItem(records, "'" + item + "'");
    }
}
function removeItem(array, search_item) {
    for (var i = array.length - 1; i >= 0; i--) {
        if (array[i] === search_item) {
            array.splice(i, 1);
            break;
        }
    }
}
function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
function exportAll(type) {
    $('#loadingmessage').show();
    //Change Back
    var path = "/LogBook/LogBook/ExportExcel?tablename=" + type + "&clientflt=" + getGridFilters();
    //var path = "/LogBook/ExportExcel?tablename=" + type + "&clientflt=" + getGridFilters();
    if (type == "Search") {
        //Change Back
        path = "/LogBook/LogBook/ExportExcel?tablename=Search&q=" + getParameterByName("q") + "&clientflt=" + getGridFilters();
        //path = "/LogBook/ExportExcel?tablename=Search&q=" + getParameterByName("q") + "&clientflt=" + getGridFilters();
    }
    $.ajax({
        url: path,
        dataType: 'JSON',
        success: function (response) {
            if (response.status == "200") {
                location.href = "/LogBook/Content/Downloads/" + response.StatusMessage;
                $('#loadingmessage').hide();
            }
            else {
                $('#loadingmessage').html(response.StatusMessage);
            }
        },
        error: function (xhr, status, error) {
            $('#loadingmessage').html(xhr.responseText);
            alert("An error has occurred when creating the Excel file");
        }
    });
}
function getGridFilters() {
    var gridFilters = "";
    var params = getQueryParams(window.location.href);
    for (var i = 0; i < params.length; i++) {
        if (params[i].k.substring(params[i].k.length - 11) == "grid-filter") {
            gridFilters += params[i].v + "|";
        }
    }
    return gridFilters.trim("|");
}
function getQueryParams(qs) {
    qs = qs.split("+").join(" ");

    var params = [], tokens,
    re = /[?&]?([^=]+)=([^&]*)/g;

    while (tokens = re.exec(qs)) {
        params.push({ k: decodeURIComponent(tokens[1]), v: decodeURIComponent(tokens[2]) });
    }

    return params;
}
function exportSelected() {
    $('#loadingmessage').show();
    $.ajax({
        //Change Back
        url: "/LogBook/LogBook/ExportExcel?tablename=SelectedCalls&calls=" + records.join(),
        //url: "/LogBook/ExportExcel?tablename=SelectedCalls&calls=" + records.join(),
        dataType: 'JSON',
        success: function (response) {
            if (response.status == "200") {
                location.href = "/LogBook/Content/Downloads/" + response.StatusMessage;
                $('#loadingmessage').hide();
            }
            else {
                $('#loadingmessage').html(response.StatusMessage);
            }
        },
        error: function (xhr, status, error) {
            $('#loadingmessage').html(xhr.responseText);
            alert("An error has occurred when creating the Excel file");
        }
    });
}
function edit(id) {
    unlockForm();
    form1.act.value = "edit";
    document.getElementById('hfAct').value = 'edit';
    form1.SavedRecordID = id.toString();
   
    $('#update_data').modal({
        backdrop: 'static',
        keyboard: false
    });
    if ((id != null) && (id != NaN)) {
        //Change Back
        $.post("/LogBook/ajax/Edit/" + id, function (data, status) {
        //$.post("/ajax/Edit/" + id, function (data, status) {
            reset();
            debugger;
            form1.action.value = "edit";
            form1.act.value = "edit";
            document.getElementById('hfAct').value = 'edit';
            form1.CopyProtectionAdvisor.checked = data[0].CopyProtectionAdvisor==1;
            form1.CopyGenderFocalPoint.checked = data[0].CopyGenderFocalPoint==1;
            form1.Programme.value = data[0].Programme;
            form1.Programme.onchange();
            form1.SubProgramme.value = data[0].SubProgramme;
            form1.SubProgramme.onchange();
            form1.Source.value = data[0].SourceID;
            form1.Source.onchange();
            form1.SubSource.value = data[0].SubSource;
            form1.LocationType.value = data[0].LocationTypeID;
            form1.LocationType.onchange();
            form1.Governorate.value = data[0].Governorate;
            form1.Governorate.onchange();
            form1.District.value = data[0].District;
            form1.GPSCoordinatesLat.value = data[0].GPSCoordinateLat;
            form1.GPSCoordinatesLong.value = data[0].GPSCoordinateLong;
            form1.Category.value = data[0].CategoryID;
            form1.Category.onchange();
            form1.SubCategory.value = data[0].SubCategory;
            form1.Details.value = data[0].Details;
            form1.ResponsibleUnit.value = data[0].ResponsibleUnit;
            form1.ResponsibleUnit.onchange();
            form1.ResponsiblePerson.value = data[0].ResponsiblePerson;
            form1.Severity.value = data[0].Severity;
            //form1.ShopId.value = data[0].ShopID;            
            form1.txtOtherExternalSource.value = data[0].OtherExternalSource.toString();
            form1.txtSourceFurtherInfo.value = data[0].SourceFurtherInfo.toString();
            form1.Status.value = data[0].Status;
            form1.Action.value = data[0].Action;

            form1.LocationType2.value = data[0].LocType2;
            form1.LocationType2.onchange();
            form1.Governorate2.value = data[0].Gov2ID;
            form1.Governorate2.onchange();
            form1.District2.value = data[0].Dis2ID;
                        
            form1.LocationType3.value = data[0].LocType3;
            form1.LocationType3.onchange();
            form1.Governorate3.value = data[0].Gov3ID;
            form1.Governorate3.onchange();
            form1.District3.value = data[0].Dis3ID;
                        
            form1.LocationType4.value = data[0].LocType4;
            form1.LocationType4.onchange();
            form1.Governorate4.value = data[0].Gov4ID;
            form1.Governorate4.onchange();
            form1.District4.value = data[0].Dis4ID;
                        
            form1.LocationType5.value = data[0].LocType5;
            form1.LocationType5.onchange();
            form1.Governorate5.value = data[0].Gov5ID;
            form1.Governorate5.onchange();
            form1.District5.value = data[0].Dis5ID;
                        
            form1.LocationType6.value = data[0].LocType6;
            form1.LocationType6.onchange();
            form1.Governorate6.value = data[0].Gov6ID;
            form1.Governorate6.onchange();
            form1.District6.value = data[0].Dis6ID;
                        
            form1.LocationType7.value = data[0].LocType7;
            form1.LocationType7.onchange();
            form1.Governorate7.value = data[0].Gov7ID;
            form1.Governorate7.onchange();
            form1.District7.value = data[0].Dis7ID;

            form1.LocationType8.value = data[0].LocType8;
            form1.LocationType8.onchange();
            form1.Governorate8.value = data[0].Gov8ID;
            form1.Governorate8.onchange();
            form1.District8.value = data[0].Dis8ID;
                        
            form1.LocationType9.value = data[0].LocType9;
            form1.LocationType9.onchange();
            form1.Governorate9.value = data[0].Gov9ID;
            form1.Governorate9.onchange();
            form1.District9.value = data[0].Dis9ID;

            form1.LocationType10.value = data[0].LocType10;
            form1.LocationType10.onchange();
            form1.Governorate10.value = data[0].Gov10ID;
            form1.Governorate10.onchange();
            form1.District10.value = data[0].Dis10ID;

            
            //Selcet number of locations 
            form1.numberoflocations.value = 1;
            form1.numberoflocations.onchange();
            if (data[0].Gov10ID != 0 ) {
                form1.numberoflocations.value = 10;
                form1.numberoflocations.onchange();
            }
            else if (data[0].Gov9ID != 0) {
                form1.numberoflocations.value = 9;
                form1.numberoflocations.onchange();
            }
            else if (data[0].Gov8ID != 0) {
                form1.numberoflocations.value = 8;
                form1.numberoflocations.onchange();
            }
            else if (data[0].Gov7ID != 0) {
                form1.numberoflocations.value = 7;
                form1.numberoflocations.onchange();
            }
            else if (data[0].Gov6ID != 0) {
                form1.numberoflocations.value = 6;
                form1.numberoflocations.onchange();
            }
            else if (data[0].Gov5ID != 0) {
                form1.numberoflocations.value = 5;
                form1.numberoflocations.onchange();
            }
            else if (data[0].Gov4ID != 0) {
                form1.numberoflocations.value = 4;
                form1.numberoflocations.onchange();
            }
            else if (data[0].Gov3ID != 0) {
                form1.numberoflocations.value = 3;
                form1.numberoflocations.onchange();
            }
            else if (data[0].Gov2ID != 0) {
                form1.numberoflocations.value = 2;
                form1.numberoflocations.onchange();
            }


            form1.GPSCoordinatesLat2.value = data[0].GPSCoordinateLat2;
            form1.GPSCoordinatesLong2.value = data[0].GPSCoordinateLong2;

            form1.GPSCoordinatesLat3.value = data[0].GPSCoordinateLat3;
            form1.GPSCoordinatesLong3.value = data[0].GPSCoordinateLong3;

            form1.GPSCoordinatesLat4.value = data[0].GPSCoordinateLat4;
            form1.GPSCoordinatesLong4.value = data[0].GPSCoordinateLong4;
            form1.GPSCoordinatesLat5.value = data[0].GPSCoordinateLat5;
            form1.GPSCoordinatesLong5.value = data[0].GPSCoordinateLong5;
            form1.GPSCoordinatesLat6.value = data[0].GPSCoordinateLat6;
            form1.GPSCoordinatesLong6.value = data[0].GPSCoordinateLong6;
            form1.GPSCoordinatesLat7.value = data[0].GPSCoordinateLat7;
            form1.GPSCoordinatesLong7.value = data[0].GPSCoordinateLong7;
            form1.GPSCoordinatesLat8.value = data[0].GPSCoordinateLat8;
            form1.GPSCoordinatesLong8.value = data[0].GPSCoordinateLong8;
            form1.GPSCoordinatesLat9.value = data[0].GPSCoordinateLat9;
            form1.GPSCoordinatesLong9.value = data[0].GPSCoordinateLong9;
            form1.GPSCoordinatesLat10.value = data[0].GPSCoordinateLat10;
            form1.GPSCoordinatesLong10.value = data[0].GPSCoordinateLong10;
            //form1.OtherFMOs.value = data[0].OtherFMOs;

            var $select1 = $('#OtherFMOs').selectize();
            var control1 = $select1[0].selectize;
            if (data[0].OtherFMOs != '' && data[0].OtherFMOs != null) {
                for (var i = 0; i < data[0].OtherFMOs.split(',').length; i++) {

                    if (data[0].OtherFMOs.split(',')[i] != '0')
                    {
                        control1.addOption({ value: data[0].OtherFMOs.split(',')[i], text: data[0].OtherFMOs.split(',')[i] })
                        control1.addItem(data[0].OtherFMOs.split(',')[i]);
                    }                    
                }
            }
            

            form1.ddlMonitoringActivity.value = data[0].MonitoringActivityID;

           
            form1.ddlMonitoringActivity.onchange();
            form1.ddlMonitoringActivitySub.value = data[0].MonitoringActivitySubID;

            form1.txtFollowUp.value = data[0].FollowUpDate;
            form1.HKMLocationType.value = data[0].HKMLocationType;
            form1.SubProgrammeDetails.value = data[0].SubProgrammeDetails;
            
            setRecordType(data[0].RecordType);
			form1.hfRecordNo.value = id;

			form1.numberofactivities.value = 1;
			form1.numberofactivities.onchange();
			if (data[0].ActivityType10 != '') {
				form1.numberofactivities.value = 10;
				form1.numberofactivities.onchange();
			}
			else if (data[0].ActivityType9 != '') {
				form1.numberofactivities.value = 9;
				form1.numberofactivities.onchange();
			}
			else if (data[0].ActivityType8 != '') {
				form1.numberofactivities.value = 8;
				form1.numberofactivities.onchange();
			}
			else if (data[0].ActivityType7 != '') {
				form1.numberofactivities.value = 7;
				form1.numberofactivities.onchange();
			}
			else if (data[0].ActivityType6 != '') {
				form1.numberofactivities.value = 6;
				form1.numberofactivities.onchange();
			}
			else if (data[0].ActivityType5 != '') {
				form1.numberofactivities.value = 5;
				form1.numberofactivities.onchange();
			}
			else if (data[0].ActivityType4 != '') {
				form1.numberofactivities.value = 4;
				form1.numberofactivities.onchange();
			}
			else if (data[0].ActivityType3 != '') {
				form1.numberofactivities.value = 3;
				form1.numberofactivities.onchange();
			}
			else if (data[0].ActivityType2 != '') {
				form1.numberofactivities.value = 2;
				form1.numberofactivities.onchange();
			}

			
			//form1.Title.value = data[0].Title;
			form1.subactivity.value = data[0].ActivityType1;
			form1.numberofforms.value = data[0].NumberofForms1;
			form1.subactivity2.value = data[0].ActivityType2;
			form1.numberofforms2.value = data[0].NumberofForms2;
			form1.subactivity3.value = data[0].ActivityType3;
			form1.numberofforms3.value = data[0].NumberofForms3;
			form1.subactivity4.value = data[0].ActivityType4;
			form1.numberofforms4.value = data[0].NumberofForms4;
			form1.subactivity5.value = data[0].ActivityType5;
			form1.numberofforms5.value = data[0].NumberofForms5;
			form1.subactivity6.value = data[0].ActivityType6;
			form1.numberofforms6.value = data[0].NumberofForms6;
			form1.subactivity7.value = data[0].ActivityType7;
			form1.numberofforms7.value = data[0].NumberofForms7;
			form1.subactivity8.value = data[0].ActivityType8;
			form1.numberofforms8.value = data[0].NumberofForms8;
			form1.subactivity9.value = data[0].ActivityType9;
			form1.numberofforms9.value = data[0].NumberofForms9;
			form1.subactivity10.value = data[0].ActivityType10;
			form1.numberofforms10.value = data[0].NumberofForms10;
			form1.mainchallenges.value = data[0].MainChallenges;
			form1.MainFindings.value = data[0].MainFindings;
			form1.IssuesForFollowUp.value = data[0].IssuestoFollowUp;
			form1.ActionTakenByFM.value = data[0].ActionTakenbyFM;
			
			
            //form1.ShopId.value = data[0].ShopID;
            if (data[0].ResponsiblePersonDesc != null && data[0].ResponsiblePersonDesc.toLowerCase() == document.getElementById("Staff").value.replace(".", " "))
                lockForm();
            showHideControlsBasedonRecordType(data[0].RecordType);
            $('#trUnitsInCopy').hide();

            
               
                if (id.indexOf("T") != -1)
                {
                    $('#myModalLabel').html('Edit a Ticket')
                }
                else
                {
                    $('#myModalLabel').html('Edit a Monitoring Note')
                }

                $('#dvNotifyEmails').hide();
                form1.staff = data[0].staff;
           
        });
        $("#message").html("You are editing record no.: " );
        $("#RecordNumber").html(id);
        
        
    }
}
function lockForm() {
    //Disable all
    $('#form1 select').prop('disabled', true);
    $('#form1 input[type="text"]').prop("disabled", true);
    $('#form1 input[type="number"]').prop("disabled", true);
    $('#form1 textarea').prop("disabled", true);
    //Enable Status and action Taken
    $('#Status').prop('disabled', false);
    $('#Action').prop('disabled', false);
}
function unlockForm() {
    //Enable all
    $('#form1 select').prop('disabled', false);
    $('#form1 input[type="text"]').prop("disabled", false);
    $('#form1 input[type="number"]').prop("disabled", false);
    $('#form1 textarea').prop("disabled", false);
}
function view(id) {
    var dialog = new BootstrapDialog.show({
        title: 'Record Details',
        cssClass: 'view-dialog',
        //Change Back
        message: $('<div></div>').load('/LogBook/LogBook/ViewTicket/' + id)
        //message: $('<div></div>').load('/LogBook/ViewTicket/' + id)
    });
}
function history(id) {
    var dialog = new BootstrapDialog.show({
        title: 'Record History',
        cssClass: 'view-dialog',
        //Change Back
        message: $('<div></div>').load('/LogBook/LogBook/ViewHistory/' + id)
        //message: $('<div></div>').load('/LogBook/ViewHistory/' + id)
    });
}
function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
}
function saveRecord(close) {
    if (formIsValid()) {
        unlockForm();
        $("#loader2").show();
        var url = "";
        if (form1.act.value == "create") {
            //Change Back
            url = "/LogBook/ajax/Create";
            //url = "/ajax/Create";
        }
        else if (form1.act.value == "edit") {
            //Change Back
            url = "/LogBook/ajax/Update";
            //url = "/ajax/Update";
        }
        
        $.ajax({
            type: "POST",
            url: url,
            data: $('form').serialize(),
            success: function (msg) {
                if (msg.status == "200") {
                    $("#alert-message").html(msg.StatusMessage);
                    $("#success-alert").alert();
                    $("#success-alert").fadeTo(2000, 500).slideUp(500, function () {
                        $("#success-alert").slideUp(500);
                    });
                    $("#myrecords").html("Number of my records today: " + msg.MyRecords)
                    updated = true;
                    if (close) {
                        location.reload();
                        updated = false;
                    }
                    else {
                        addNew();
                    }
                }
                else {
                    alert(msg.StatusMessage);
                }
                $("#loader2").hide();
            },
            error: function (xhr, status, error) {
                var err = eval("(" + xhr.responseText + ")");
                $("#loader2").hide();
                alert(err.Message);
            }
        });
    }
}


function addNew(recordType) {
    reset();
    form1.act.value = "create";
    document.getElementById('hfAct').value = '';
    $("#message").html("You are creating a new record");
    form1.hfRecordNo.value = '';
    form1.numberoflocations.value = 0;
    form1.numberoflocations.onchange();
    $('#trUnitsInCopy').hide();
    showHideControlsBasedonRecordType(recordType);
    $("#RecordNumber").html('');
    
    if (recordType == 2 )
    {
        $('#myModalLabel').html('Create a Monitoring Report')
    }
    else 
    {
        $('#myModalLabel').html('Create a Ticket')
    }

    


  

}
function formIsValid() {
    msg = "";
    error = false;

    //monitoring
      if (form1.RecordType.value == 2) 
    {
          checkRequired("Programme", "Programme is required");
          checkRequired("SubProgramme", "Sub Programme is required");
          checkRequired("ddlMonitoringActivity", "Monitoring Activity is required");

          if ($('#ddlMonitoringActivitySub').children('option').length > 1)
          {
              checkRequired("ddlMonitoringActivitySub", "Sub Monitoring Activity is required");
          }
          
          //checkRequired("LocationType", "Location Type is required");
          //if ($('#LocationType').val() != '3') {
          //    checkRequired("Governorate", "Governorate is required");
          //    checkRequired("District", "District is required");
          //}
          checkRequired("Details", "Notes are required");
          //checkRequired("txtFollowUp", "Follow-up date is required");
          
          if ($('#SubProgramme').val() == '2') {
              checkRequired("HKMLocationType", "HKM Location Type is required");
          }
		  checkRequired("MainFindings", "Please enter main findings");
		  

          if (error) {
              alert(msg);
              return false;
          }
          else {
              return true;
          }
    }
    else
      {
          checkRequired("Programme", "Programme is required");
          checkRequired("SubProgramme", "Sub Programme is required");
          checkRequired("Source", "Source is required");
          checkRequired("Status", "Status is required");
		  checkRequired("SubSource", "Sub Source is required");

		  if ($('#numberoflocations').val() != '0') {

			  checkRequired("LocationType", "Location Type is required");
			  checkRequired("Governorate", "Governorate is required");
			  checkRequired("District", "District is required");
		  }

          



          checkRequired("Category", "Category is required");
          checkRequired("SubCategory", "SubCategory is required");
          checkRequired("Details", "Details is required");
          checkRequired("Severity", "Severity is required");

          if ($('#SubProgramme').val() == '2') {
              checkRequired("HKMLocationType", "HKM Location Type is required");
          }

          if (error) {
              alert(msg);
              return false;
          }
          else {
              return true;
          }
      }

    
}
function checkRequired(param, message) {
    debugger;
    if (document.getElementById(param).value == null
        || document.getElementById(param).value == '') {
        error = true;
        if (msg == "")
            msg = message;
        else
            msg = msg + "\n" + message;
    }
}
function checkNumber(param, message) {
    if (!$.isNumeric(document.getElementById(param).value)) {
        error = true;
        if (msg == "")
            msg = message;
        else
            msg = msg + "\n" + message;
    }
}
function hideForm() {
    reset();
    $('#update_data').modal('hide');
    if (updated) {
        location.reload();
        updated = false;
    }
}
function newRecord(recordType) {
    form1.RecordType.value = recordType;
    setRecordType(recordType);
    $('.datepicker').datepicker({ dateFormat: 'yy-mm-dd' });
    reset();
    $('#update_data').modal('show');
    form1.act.value = "create";
    document.getElementById('hfAct').value = '';
    addNew(recordType);
	
}
function setRecordType(recordType) {
    form1.RecordType.value = recordType;
    if (recordType == 1) {
        document.getElementById("recordTypeLabel").innerHTML = "Ticket";
        $("#ResponsibleUnit-tr").show();
        // $("#ResponsiblePerson-tr").show();
        $('#SendEmail').prop('checked', true);
    }
    else if (recordType == 2) {
        $("#ResponsibleUnit-tr").hide();
        // $("#ResponsiblePerson-tr").hide();
        $('#SendEmail').prop('checked', false);
        document.getElementById("recordTypeLabel").innerHTML = "Daily Monitoring Report";
    }
    else
        document.getElementById("recordTypeLabel").innerHTML = "Unknown record type";
}
function reset() {
    if(form1.act.value="create")
        $(".optional").hide();
    else
        $(".optional").show();
    unlockForm();
    form1.CopyProtectionAdvisor.checked=false;
    form1.CopyGenderFocalPoint.checked=false;
    form1.Programme.value = '';
    form1.SubProgramme.value = '';
    form1.Source.value = '';
    form1.SubSource.value = '';
    form1.LocationType.value = '';
    form1.Governorate.value = '';
    form1.District.value = '';
    form1.GPSCoordinatesLat.value = '';
    form1.GPSCoordinatesLong.value = '';
    form1.Category.value = '';
    form1.SubCategory.value = '';
    form1.Details.value = '';
    form1.ResponsibleUnit.value = '';
    form1.ResponsiblePerson.value = '';
    form1.SendEmail.checked=false;
    form1.Severity.value = '';
    //form1.ShopId.value = '';
    form1.Action.value = '';
    setRecordType(form1.RecordType.value);
    form1.txtOtherExternalSource.value = '';
    form1.txtSourceFurtherInfo.value = '';

    form1.LocationType2.value = '';
    form1.Governorate2.value = '';
    form1.District2.value = '';
    form1.LocationType3.value = '';
    form1.Governorate3.value = '';
    form1.District3.value = '';
    form1.LocationType4.value = '';
    form1.Governorate4.value = '';
    form1.District4.value = '';
    form1.LocationType5.value = '';
    form1.Governorate5.value = '';
    form1.District5.value = '';
    form1.LocationType6.value = '';
    form1.Governorate6.value = '';
    form1.District6.value = '';
    form1.LocationType7.value = '';
    form1.Governorate7.value = '';
    form1.District7.value = '';
    form1.LocationType8.value = '';
    form1.Governorate8.value = '';
    form1.District8.value = '';
    form1.LocationType9.value = '';
    form1.Governorate9.value = '';
    form1.District9.value = '';
    form1.LocationType10.value = '';
    form1.Governorate10.value = '';
    form1.District10.value = '';
    form1.ddlMonitoringActivity.value = '';
    form1.ddlMonitoringActivitySub.value = '';
    form1.txtFollowUp.value = '';
    form1.UnitsInCopy.value = '';
    form1.responsiblePerson = '';
    $('#us2-address1').val('');
    $('#us2-address2').val('');
    $('#us2-address3').val('');
    $('#us2-address4').val('');
    $('#us2-address5').val('');
    $('#us2-address6').val('');
    $('#us2-address7').val('');
    $('#us2-address8').val('');
    $('#us2-address9').val('');
    $('#us2-address10').val('');
   
    $('#Lat1').val('');
    $('#Lat2').val('');
    $('#Lat3').val('');
    $('#Lat4').val('');
    $('#Lat5').val('');
    $('#Lat6').val('');
    $('#Lat7').val('');
    $('#Lat8').val('');
    $('#Lat9').val('');
    $('#Lat10').val('');

    $('#Long1').val('');
    $('#Long2').val('');
    $('#Long3').val('');
    $('#Long4').val('');
    $('#Long5').val('');
    $('#Long6').val('');
    $('#Long7').val('');
    $('#Long8').val('');
    $('#Long9').val('');
    $('#Long10').val('');
    
    $('#GPSCoordinatesLat').val('');
    $('#GPSCoordinatesLat2').val('');
    $('#GPSCoordinatesLat3').val('');
    $('#GPSCoordinatesLat4').val('');
    $('#GPSCoordinatesLat5').val('');
    $('#GPSCoordinatesLat6').val('');
    $('#GPSCoordinatesLat7').val('');
    $('#GPSCoordinatesLat8').val('');
    $('#GPSCoordinatesLat9').val('');
    $('#GPSCoordinatesLat10').val('');
    
    $('#GPSCoordinatesLong').val('');
    $('#GPSCoordinatesLong2').val('');
    $('#GPSCoordinatesLong3').val('');
    $('#GPSCoordinatesLong4').val('');
    $('#GPSCoordinatesLong5').val('');
    $('#GPSCoordinatesLong6').val('');
    $('#GPSCoordinatesLong7').val('');
    $('#GPSCoordinatesLong8').val('');
    $('#GPSCoordinatesLong9').val('');
    $('#GPSCoordinatesLong10').val('');
    $('#Severity').val('');

    var $select = $('#UnitsInCopy').selectize();
    var control = $select[0].selectize;
    control.clear();

    var $select1 = $('#OtherFMOs').selectize();
    var control1 = $select1[0].selectize;
    control1.clear();

    $('#hfCopyOtherUnits').val('');
    form1.HKMLocationType.value = '';
    
	$("#UnitsInCopy").hide();

	$('#numberofactivities').val(1);
	$('#subactivity').val(1);
	$('#numberofforms').val(1);
	$('#subactivity2').val(1);
	$('#numberofforms2').val(1);
	$('#subactivity3').val(1);
	$('#numberofforms3').val(1);
	$('#subactivity4').val(1);
	$('#numberofforms4').val(1);
	$('#subactivity5').val(1);
	$('#numberofforms5').val(1);
	$('#subactivity6').val(1);
	$('#numberofforms6').val(1);
	$('#subactivity7').val(1);
	$('#numberofforms7').val(1);
	$('#subactivity8').val(1);
	$('#numberofforms8').val(1);
	$('#subactivity9').val(1);
	$('#numberofforms9').val(1);
	$('#subactivity10').val(1);
	$('#numberofforms10').val(1);
	$('#mainchallenges').val(1);
	$('#MainFindings').val('');
	$('#IssuesForFollowUp').val('');
	$('#ActionTakenByFM').val('');
	$('#Title').val('');

	//Hide Control
	$('#trTitle').hide();
	$('#numberoflocations').val(0);
	
   
}
function assignSelected() {
    if (records.length == 0) {
        alert("No records selected");
        return false;
    }
    $("#range").val("Selected");
    $("#filters").val(records.join());
    $('#assign_dialog').modal({
        backdrop: 'static',
        keyboard: false
    });
}
function assignAll() {
    if (getGridFilters() == "") {
        alert("You have to filter the data before assigning");
        return false;
    }
    $("#range").val("AllCalls");
    $("#filters").val(getGridFilters());
    $('#assign_dialog').modal({
        backdrop: 'static',
        keyboard: false
    });
}
function assignRecords() {
    if ($("#AssignedUnit").val() == null || $("#AssignedUnit").val() == '') {
        alert("Please select responsible unit");
    }
    else if ($("#AssignedPerson").val() == null || $("#AssignedPerson").val() == '') {
        alert("Please select responsible person");
    }
    else {
        $('#assign_dialog').modal('hide');
        $('#loadingmessage').show();
        $.ajax({
            //Change Back
            url: "/LogBook/LogBook/AssignRecords?tablename=" + $("#range").val() + "&Staff=" + $("#Staff").val()
            //url: "/LogBook/AssignRecords?tablename=" + $("#range").val() + "&Staff=" + $("#Staff").val()
                + "&unit=" + $("#AssignedUnit").val() + "&person=" + $("#AssignedPerson").val()
                + "&clientflt=" + $("#filters").val(),
            dataType: 'JSON',
            success: function (response) {
                if (response.status == "200") {
                    alert("The tickets were successfully assigned to the selected unit and person respectively");
                    $('#loadingmessage').hide();
                }
                else {
                    $('#loadingmessage').html(response.StatusMessage);
                }
            },
            error: function (xhr, status, error) {
                $('#loadingmessage').html(xhr.responseText);
                alert("An error has occurred while trying to assign records to the selected unit and person");
            }
        });
    }
}
function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}
function closeSelected() {
    if (records.length == 0) {
        alert("No records selected");
        return false;
    }
    $("#range").val("Selected");
    $("#filters").val(records.join());
    $('#close_dialog').modal({
        backdrop: 'static',
        keyboard: false
    });
}
function closeUpload() {
    $('#upload_dialog').modal({
        backdrop: 'static',
        keyboard: false
    });
}
function changeUnit(ddlUnitsID, ddlPersonsID) {


    cascadeDropdownResponsible(ddlUnitsID, ddlPersonsID, responsiblePerson);
    
}
function addDropDownItem(ddlName, optValue, optText) {
    var opt = document.createElement("option");
    opt.text = optText;
    opt.value = optValue;
    document.getElementById(ddlName).options.add(opt);
}
function clearDropdown(ddlName) {
    var selectbox = document.getElementById(ddlName);
    var i;
    for (i = selectbox.options.length - 1; i >= 0; i--) {
        selectbox.remove(i);
    }
}
function changeGovernorate(value, ind) {
    //if (value != "4" && value != "9" && value != "16") {
        //Do not show district when it's a camp
       // $("#District-tr").show();
        
    if (ind == 1)
    {
        cascadeDropdown("Governorate", "District", district);
    }
    else
    {
        cascadeDropdown("Governorate" + ind, "District" + ind, district);
    }

        


    //}
  //  else {
      //  $("#District-tr").hide();
        
   // }
    var shoplist = [
	{
	    "id": 1,
	    "shop_name": "Abu Hashem - Irbid - ",
	    "governorate_id": 7
	},
	{
	    "id": 2,
	    "shop_name": "Najm Al Wadi shop - Ras Al Ain - ",
	    "governorate_id": 2
	},
	{
	    "id": 3,
	    "shop_name": "Najm Al Wadi shop - Wehdat Camp - ",
	    "governorate_id": 2
	},
	{
	    "id": 4,
	    "shop_name": "Ahel AL Yameen Al Estihlakeya Al Shaabeyah - Amman - ",
	    "governorate_id": 2
	},
	{
	    "id": 5,
	    "shop_name": "HASAIN ABDALLAH MOHMD SALEH - Amman - ",
	    "governorate_id": 2
	},
	{
	    "id": 6,
	    "shop_name": "AL Aref Supermarket - Zarqa - ",
	    "governorate_id": 17
	},
	{
	    "id": 7,
	    "shop_name": "Al Dalla (Amin Farhan) - Mafraq - ",
	    "governorate_id": 13
	},
	{
	    "id": 8,
	    "shop_name": "AYLA COMMERCIAL MARKETS CO. - Aqaba - ",
	    "governorate_id": 3
	},
	{
	    "id": 9,
	    "shop_name": "Al Theib (Anas Samara) - Irbid - ",
	    "governorate_id": 7
	},
	{
	    "id": 10,
	    "shop_name": "Al-Farid General Supplier - Amman - Al Abdali",
	    "governorate_id": 2
	},
	{
	    "id": 11,
	    "shop_name": "Ahmed Al Khazaleh Foundation for Supply - Mafraq - ",
	    "governorate_id": 13
	},
	{
	    "id": 12,
	    "shop_name": "Khalid Al Zobi & Jamal Al Hamad Co. - Al Qallaz -  - (Qallaz 1)  Ramtha - Al Sham highway (1)",
	    "governorate_id": 7
	},
	{
	    "id": 13,
	    "shop_name": "Khalid Al Zobi & Jamal Al Hamad Co. - Al Qallaz -  - (Qallaz 2) Ramtha – South district ",
	    "governorate_id": 7
	},
	{
	    "id": 14,
	    "shop_name": "Al Rawda (Saeed Shahada) - Amman - ",
	    "governorate_id": 2
	},
	{
	    "id": 15,
	    "shop_name": "Al Faleh Market  - Madaba - ",
	    "governorate_id": 12
	},
	{
	    "id": 16,
	    "shop_name": "Golden Food Markets - Ajlun - ",
	    "governorate_id": 1
	},
	{
	    "id": 17,
	    "shop_name": "Gaza Markets - Jarash - ",
	    "governorate_id": 8
	},
	{
	    "id": 18,
	    "shop_name": "Aswaq Makka  - Irbid - Al Huson - Huson City",
	    "governorate_id": 7
	},
	{
	    "id": 19,
	    "shop_name": "Aswaq Makka  - Irbid  - Al Hafaq - Estiklal street - Prince hasan sports city (Al Nafaq)",
	    "governorate_id": 7
	},
	{
	    "id": 20,
	    "shop_name": "Aswaq Makka  - Irbid - Al Jamaah  -  University south gate (Al Yarmouk",
	    "governorate_id": 7
	},
	{
	    "id": 21,
	    "shop_name": "Aswaq Makka  - Irbid - Al Naseem, down twon - Al Naseem Circle",
	    "governorate_id": 7
	},
	{
	    "id": 22,
	    "shop_name": "Aswaq Makka  - west Irbid - Al Turkman area (west Irbid)",
	    "governorate_id": 7
	},
	{
	    "id": 23,
	    "shop_name": "Aswaq Makka  - Irbid - down twon - Huson Street",
	    "governorate_id": 7
	},
	{
	    "id": 24,
	    "shop_name": "Aswaq Makka  - Irbid - 30 St. - ",
	    "governorate_id": 7
	},
	{
	    "id": 25,
	    "shop_name": "Aswaq Makka  - Irbid - industrial city - Hakam industrial city",
	    "governorate_id": 7
	},
	{
	    "id": 26,
	    "shop_name": "Aswaq Makka  - west Irbid - Al Bayada - Estiklal circle ( Al Baiyada",
	    "governorate_id": 7
	},
	{
	    "id": 28,
	    "shop_name": "Ayman Al Ghawanmeh Foundation - Mafraq - Downtown",
	    "governorate_id": 13
	},
	{
	    "id": 29,
	    "shop_name": "Bait Hanon Foundation - Karak - Bayt Hanoun",
	    "governorate_id": 10
	},
	{
	    "id": 30,
	    "shop_name": "Amjad Al Bustanji and Sons Company - Aqaba - ",
	    "governorate_id": 3
	},
	{
	    "id": 31,
	    "shop_name": "Majid Al Fatim Hypermarkets - Jordan. carrefour - Irbid - Next to Hassan Sports City",
	    "governorate_id": 7
	},
	{
	    "id": 32,
	    "shop_name": "Majid Al Fatim Hypermarkets - Jordan. carrefour - Madaba - Near Shakhatreh gaz station (neerby Anagole)",
	    "governorate_id": 12
	},
	{
	    "id": 33,
	    "shop_name": "Majid Al Fatim Hypermarkets - Jordan. carrefour - Jabal Al Husain - ",
	    "governorate_id": 2
	},
	{
	    "id": 34,
	    "shop_name": "Majid Al Fatim Hypermarkets - Jordan. carrefour - Daheyat Al Yasameen - ",
	    "governorate_id": 2
	},
	{
	    "id": 35,
	    "shop_name": "Majid Al Fatim Hypermarkets - Jordan. carrefour - Yajooz (Al Rsayfeh) - ",
	    "governorate_id": 17
	},
	{
	    "id": 36,
	    "shop_name": "Majid Al Fatim Hypermarkets - Jordan. carrefour - Aqaba - Al Rahmeh mall",
	    "governorate_id": 3
	},
	{
	    "id": 37,
	    "shop_name": "Military Retirees Association for Zaatari - Mafraq - ",
	    "governorate_id": 13
	},
	{
	    "id": 38,
	    "shop_name": "Mefleh Al Sharari Market, ehsebha sah - Maan - ",
	    "governorate_id": 11
	},
	{
	    "id": 39,
	    "shop_name": "Faisal Al shaweesh foundation (AL Faisal Mall) - Maan - ",
	    "governorate_id": 11
	},
	{
	    "id": 40,
	    "shop_name": "Hind Ibrahim Abdelghani Supermarket (Waleed Harara) - Maan - ",
	    "governorate_id": 11
	},
	{
	    "id": 41,
	    "shop_name": "Highlight Stores - Maan - ",
	    "governorate_id": 11
	},
	{
	    "id": 42,
	    "shop_name": "Al Eltezam - Jarash - ",
	    "governorate_id": 8
	},
	{
	    "id": 43,
	    "shop_name": "Integarated Modern Commercial Markets (Irbid Mall) - Irbid - South district, Al Rabiya area",
	    "governorate_id": 7
	},
	{
	    "id": 44,
	    "shop_name": "Abd Al Ellah Al Shboul-Jawaharat Al Shajarah - Irbid - ",
	    "governorate_id": 7
	},
	{
	    "id": 45,
	    "shop_name": "Henyan Al Awn and Hani Al- Eissa - Jawharat Al Badiya - Mafraq - ",
	    "governorate_id": 13
	},
	{
	    "id": 46,
	    "shop_name": "Civil Service Consumer Corporation - Aiin Ul Basha - ",
	    "governorate_id": 5
	},
	{
	    "id": 47,
	    "shop_name": "Civil Service Consumer Corporation - South Shooneh - ",
	    "governorate_id": 5
	},
	{
	    "id": 48,
	    "shop_name": "Civil Service Consumer Corporation - Maddi - ",
	    "governorate_id": 5
	},
	{
	    "id": 49,
	    "shop_name": "Khaled AL Doghmi Foundation for food supplies  - Mafraq - ",
	    "governorate_id": 13
	},
	{
	    "id": 50,
	    "shop_name": "Mohammad Hamdan shop - Cyber City - ",
	    "governorate_id": 7
	},
	{
	    "id": 51,
	    "shop_name": "OMAR HUSAIN AL BAOUL SHOP (Abu Waheed) - Ajlun - ",
	    "governorate_id": 1
	},
	{
	    "id": 52,
	    "shop_name": "Professional Grocery - Zarqa - ",
	    "governorate_id": 17
	},
	{
	    "id": 53,
	    "shop_name": "The Jordnaian Company For Investment and Supply - Safeway - 7th Circle - ",
	    "governorate_id": 2
	},
	{
	    "id": 54,
	    "shop_name": "The Jordnaian Company For Investment and Supply - Safeway - Shmaisani - ",
	    "governorate_id": 2
	},
	{
	    "id": 55,
	    "shop_name": "The Jordnaian Company For Investment and Supply - Safeway - Zarqa - Wadi Al Hajar",
	    "governorate_id": 17
	},
	{
	    "id": 56,
	    "shop_name": "The Jordnaian Company For Investment and Supply - Safeway - Aqaba Branch - ",
	    "governorate_id": 3
	},
	{
	    "id": 57,
	    "shop_name": "The Jordnaian Company For Investment and Supply - Safeway - Irbed - ",
	    "governorate_id": 7
	},
	{
	    "id": 58,
	    "shop_name": "The Jordnaian Company For Investment and Supply - Safeway - Al Moqabalain - ",
	    "governorate_id": 2
	},
	{
	    "id": 59,
	    "shop_name": "The Jordnaian Company For Investment and Supply - Safeway - Marj Al Hamam- Al Dalleh Circle - ",
	    "governorate_id": 2
	},
	{
	    "id": 61,
	    "shop_name": "The Jordnaian Company For Investment and Supply - Safeway - Marj Al Hamam- Al Jundi Circle - ",
	    "governorate_id": 2
	},
	{
	    "id": 62,
	    "shop_name": "The Jordnaian Company For Investment and Supply - Safeway - Shafabdran - ",
	    "governorate_id": 2
	},
	{
	    "id": 63,
	    "shop_name": "The Jordnaian Company For Investment and Supply - Safeway - Zaatri Camp - ",
	    "governorate_id": 16
	},
	{
	    "id": 64,
	    "shop_name": "The Jordnaian Company For Investment and Supply - Safeway - 36 ST. - ",
	    "governorate_id": 17
	},
	{
	    "id": 65,
	    "shop_name": "Souq Sakker Al Mouaqar Al Tejari - Amman - ",
	    "governorate_id": 2
	},
	{
	    "id": 66,
	    "shop_name": "Sama Alexandria for commercial supply - Mafraq - ",
	    "governorate_id": 13
	},
	{
	    "id": 67,
	    "shop_name": "Sameh Mall Company - Irbid - near Irbid bus station",
	    "governorate_id": 7
	},
	{
	    "id": 68,
	    "shop_name": "Sameh Mall Company - Wehdat Camp - ",
	    "governorate_id": 2
	},
	{
	    "id": 69,
	    "shop_name": "Sameh Mall Company - Azraq Camp - ",
	    "governorate_id": 4
	},
	{
	    "id": 70,
	    "shop_name": "Sameh Mall Company - Air Port St. - ",
	    "governorate_id": 2
	},
	{
	    "id": 71,
	    "shop_name": "Sameh Mall Company - Daheit Al Yasameen - ",
	    "governorate_id": 2
	},
	{
	    "id": 72,
	    "shop_name": "Sameh Mall Company - Mecca St. - ",
	    "governorate_id": 2
	},
	{
	    "id": 73,
	    "shop_name": "Sameh Mall Company - Ras Al Ain - ",
	    "governorate_id": 2
	},
	{
	    "id": 74,
	    "shop_name": "Sameh Mall Company - Sawaileh - ",
	    "governorate_id": 2
	},
	{
	    "id": 75,
	    "shop_name": "Sameh Mall Company - Tabarboor - ",
	    "governorate_id": 2
	},
	{
	    "id": 76,
	    "shop_name": "Sameh Mall Company - Zarqa - liwa  Al qasabah - ",
	    "governorate_id": 17
	},
	{
	    "id": 77,
	    "shop_name": "Sameh Mall Company - New Zarqa - ",
	    "governorate_id": 17
	},
	{
	    "id": 78,
	    "shop_name": "Bait Hanon Foundation - Karak - Sanabel Al Khair",
	    "governorate_id": 10
	},
	{
	    "id": 79,
	    "shop_name": "Save Side - Salt City - Salasem",
	    "governorate_id": 5
	},
	{
	    "id": 80,
	    "shop_name": "Shehadeh Abu Sonbol (Sonbol Mall) - Marka - ",
	    "governorate_id": 2
	},
	{
	    "id": 81,
	    "shop_name": "Shehadeh Abu Sonbol (Sonbol Mall) - Al Rsaifeh - ",
	    "governorate_id": 17
	},
	{
	    "id": 82,
	    "shop_name": "ANAS MAJED MOUSA ALQAISI - Tafileh - ",
	    "governorate_id": 15
	},
	{
	    "id": 83,
	    "shop_name": "Mohammad Abu Jassar Foundation - Amman - ",
	    "governorate_id": 2
	},
	{
	    "id": 84,
	    "shop_name": "Takaful - KAP - ",
	    "governorate_id": 7
	},
	{
	    "id": 85,
	    "shop_name": "Tariq Mall - Amman - Amman - ",
	    "governorate_id": 2
	},
	{
	    "id": 86,
	    "shop_name": "Tazweed Commercial Solutions - Zaatri Camp - ",
	    "governorate_id": 16
	},
	{
	    "id": 87,
	    "shop_name": "Wardat Basman Company - Jarash - ",
	    "governorate_id": 8
	},
	{
	    "id": 88,
	    "shop_name": "Zahret Al Ordon Shop - Amman - ",
	    "governorate_id": 2
	},
	{
	    "id": 89,
	    "shop_name": "Zahrat Al Madaen - Tafileh - ",
	    "governorate_id": 15
	},
	{
	    "id": 90,
	    "shop_name": "Mohammed Ahmed Odeh Shop (Abu Jamal) - Mafraq - King Talal Street",
	    "governorate_id": 13
	},
	{
	    "id": 91,
	    "shop_name": "Hani AL Sarhan for food and dairy foundation (Al Thamin) - Mafraq - ",
	    "governorate_id": 13
	},
	{
	    "id": 92,
	    "shop_name": "Faisal Al Qayadah and Partners Supermarket - Bab Al Radwan - Madaba - ",
	    "governorate_id": 12
	},
	{
	    "id": 93,
	    "shop_name": "Feras AL Radaeydeh - Irbid - ",
	    "governorate_id": 7
	},
	{
	    "id": 94,
	    "shop_name": "Sama Ajloun Mini Market - Ajlun - ",
	    "governorate_id": 1
	},
	{
	    "id": 95,
	    "shop_name": "Al Maoon Foundation  - Irbed - ",
	    "governorate_id": 7
	},
	{
	    "id": 96,
	    "shop_name": "Mohammad Al Khazaleh Commercial Markets, Sugar and Rice - Mafraq - ",
	    "governorate_id": 13
	},
	{
	    "id": 97,
	    "shop_name": "Zyad Yousef Qazan and Partner Company - Mafraq - ",
	    "governorate_id": 13
	},
	{
	    "id": 98,
	    "shop_name": "Sameh Mall Company - Marka - Marka",
	    "governorate_id": 2
	},
	{
	    "id": 99,
	    "shop_name": "The Jordnaian Company For Investment and Supply - Safeway - Amman - Daheyat al Ameer Rashed",
	    "governorate_id": 2
	},
	{
	    "id": 100,
	    "shop_name": "The Jordnaian Company For Investment and Supply - Safeway - Amman - Jubaiha",
	    "governorate_id": 2
	},
	{
	    "id": 101,
	    "shop_name": "The Jordnaian Company For Investment and Supply - Safeway - Amman - Khalda \\ Tlaa al Ali",
	    "governorate_id": 2
	},
	{
	    "id": 102,
	    "shop_name": "The Jordnaian Company For Investment and Supply - Safeway - Amman - Wadi l Seer (Jomo Gaz)",
	    "governorate_id": 2
	},
	{
	    "id": 103,
	    "shop_name": "Al Eltezam - Mafraq - Hay l Dobat (near WFP)",
	    "governorate_id": 13
	},
	{
	    "id": 104,
	    "shop_name": "Al-Farid General Supplier  - Amman - Al Mgabalain",
	    "governorate_id": 2
	},
	{
	    "id": 105,
	    "shop_name": "Argan Markets Co - Amman - Jubaiha",
	    "governorate_id": 2
	},
	{
	    "id": 106,
	    "shop_name": "Argan Markets Co - Amman - Shafa Badran (Koom - Tareeq al nabaa)",
	    "governorate_id": 2
	},
	{
	    "id": 107,
	    "shop_name": "Argan Markets Co - Amman - Tabarbour",
	    "governorate_id": 2
	},
	{
	    "id": 108,
	    "shop_name": "Argan Markets Co - Amman - Tarek",
	    "governorate_id": 2
	},
	{
	    "id": 109,
	    "shop_name": "Al Yanee Stores - Irbed - Bait ras",
	    "governorate_id": 7
	},
	{
	    "id": 110,
	    "shop_name": "Al Yanee Stores - Irbid - end of street 30  (start of industrial street)",
	    "governorate_id": 7
	},
	{
	    "id": 111,
	    "shop_name": "Radi al Jdaifat Fondation - Mafraq - Al Mansheyah",
	    "governorate_id": 13
	},
	{
	    "id": 112,
	    "shop_name": "Takaful - Irbid - Kalthum (Dowar Al Kubbeh)",
	    "governorate_id": 7
	},
	{
	    "id": 113,
	    "shop_name": "Takaful - Irbid - West Ramtha (Al Baraka 4)",
	    "governorate_id": 7
	},
	{
	    "id": 114,
	    "shop_name": "Akram Yousef & Co. (Tulkarem Malls) - Aiin Ul Basha - Ain l basha",
	    "governorate_id": 5
	},
	{
	    "id": 115,
	    "shop_name": "Akram Yousef & Co. (Tulkarem Malls) - Amman - Daheyat al Rasheed",
	    "governorate_id": 2
	},
	{
	    "id": 116,
	    "shop_name": "Akram Yousef & Co. (Tulkarem Malls) - Amman - Shafa Badran",
	    "governorate_id": 2
	},
	{
	    "id": 117,
	    "shop_name": "Akram Yousef & Co. (Tulkarem Malls) - Amman - Sweileh",
	    "governorate_id": 2
	},
	{
	    "id": 118,
	    "shop_name": "Zahret Al Ordon Shop - Amman - Daheyat al Rasheed",
	    "governorate_id": 2
	},
	{
	    "id": 119,
	    "shop_name": "Save Side - Balqa - al akrad",
	    "governorate_id": 5
	},
	{
	    "id": 120,
	    "shop_name": "Save Side - Balqa - AL Dababneh",
	    "governorate_id": 5
	},
	{
	    "id": 121,
	    "shop_name": "Save Side - Balqa - Om Ateyah",
	    "governorate_id": 5
	},
	{
	    "id": 122,
	    "shop_name": "Ayman Al Ghawanmeh Foundation - Mafraq - Main Road",
	    "governorate_id": 13
	},
	{
	    "id": 123,
	    "shop_name": "Ayman Al Ghawanmeh Foundation - Mafraq - ",
	    "governorate_id": 13
	},
	{
	    "id": 124,
	    "shop_name": "Aswaq Makka  - Irbid - der abu saeed",
	    "governorate_id": 7
	},
	{
	    "id": 125,
	    "shop_name": "Aswaq Makka  - Irbid - Khairiah city",
	    "governorate_id": 7
	},
	{
	    "id": 126,
	    "shop_name": "Majid Al Fatim Hypermarkets - Jordan. carrefour - Amman - abu nsair (sahara)",
	    "governorate_id": 2
	},
	{
	    "id": 127,
	    "shop_name": "Majid Al Fatim Hypermarkets - Jordan. carrefour - Amman - al madina street",
	    "governorate_id": 2
	},
	{
	    "id": 128,
	    "shop_name": "Majid Al Fatim Hypermarkets - Jordan. carrefour - Amman - Al Nuzha  -  Daheyat al Aqsa",
	    "governorate_id": 2
	},
	{
	    "id": 129,
	    "shop_name": "Majid Al Fatim Hypermarkets - Jordan. carrefour - Amman - Gardenz street",
	    "governorate_id": 2
	},
	{
	    "id": 130,
	    "shop_name": "Majid Al Fatim Hypermarkets - Jordan. carrefour - Amman - Khalda (Al durra)",
	    "governorate_id": 2
	},
	{
	    "id": 131,
	    "shop_name": "Majid Al Fatim Hypermarkets - Jordan. carrefour - Amman - l Hashimi (Izmir mall)",
	    "governorate_id": 2
	},
	{
	    "id": 132,
	    "shop_name": "Majid Al Fatim Hypermarkets - Jordan. carrefour - Amman - Majdi Mall (Jordan uni street",
	    "governorate_id": 2
	},
	{
	    "id": 133,
	    "shop_name": "Majid Al Fatim Hypermarkets - Jordan. carrefour - Amman - Marj Al Hamam",
	    "governorate_id": 2
	},
	{
	    "id": 134,
	    "shop_name": "Majid Al Fatim Hypermarkets - Jordan. carrefour - Amman - Dowar al Madina - Mukhtar Mall",
	    "governorate_id": 2
	},
	{
	    "id": 135,
	    "shop_name": "Majid Al Fatim Hypermarkets - Jordan. carrefour - Amman - Al Mgabalain - Sakhra",
	    "governorate_id": 2
	},
	{
	    "id": 136,
	    "shop_name": "Majid Al Fatim Hypermarkets - Jordan. carrefour - Amman - Shmeesani",
	    "governorate_id": 2
	},
	{
	    "id": 137,
	    "shop_name": "Majid Al Fatim Hypermarkets - Jordan. carrefour - Amman - Sweifeyeh",
	    "governorate_id": 2
	},
	{
	    "id": 138,
	    "shop_name": "Majid Al Fatim Hypermarkets - Jordan. carrefour - Amman - sweifeyeh (Geleria mall)",
	    "governorate_id": 2
	},
	{
	    "id": 139,
	    "shop_name": "Majid Al Fatim Hypermarkets - Jordan. carrefour - Amman - Tabarbour (Ahwar)",
	    "governorate_id": 2
	},
	{
	    "id": 140,
	    "shop_name": "Majid Al Fatim Hypermarkets - Jordan. carrefour - Amman - Tabarbour (Karad)",
	    "governorate_id": 2
	},
	{
	    "id": 141,
	    "shop_name": "Civil Service Consumer Corporation - Ajlun - Kufranja",
	    "governorate_id": 1
	},
	{
	    "id": 142,
	    "shop_name": "Civil Service Consumer Corporation - Ajlun - Souq Ajloun",
	    "governorate_id": 1
	},
	{
	    "id": 144,
	    "shop_name": "Civil Service Consumer Corporation - Aqaba - Al quwairah",
	    "governorate_id": 3
	},
	{
	    "id": 145,
	    "shop_name": "Civil Service Consumer Corporation - Aqaba - Down town ",
	    "governorate_id": 3
	},
	{
	    "id": 146,
	    "shop_name": "Civil Service Consumer Corporation - Jarash - Jarahs Market (near historical city)",
	    "governorate_id": 8
	},
	{
	    "id": 147,
	    "shop_name": "Civil Service Consumer Corporation - Madaba - Souq Madaba",
	    "governorate_id": 12
	},
	{
	    "id": 148,
	    "shop_name": "Civil Service Consumer Corporation - Madaba - Theeban",
	    "governorate_id": 12
	},
	{
	    "id": 149,
	    "shop_name": "Civil Service Consumer Corporation - Tafileh - Ain Al Beddeh",
	    "governorate_id": 15
	},
	{
	    "id": 151,
	    "shop_name": "Civil Service Consumer Corporation - Tafileh - Bessireh",
	    "governorate_id": 15
	},
	{
	    "id": 152,
	    "shop_name": "Civil Service Consumer Corporation - Tafileh - Down town ",
	    "governorate_id": 15
	},
	{
	    "id": 153,
	    "shop_name": "Civil Service Consumer Corporation - Zarqa - Dlail city downtown",
	    "governorate_id": 17
	},
	{
	    "id": 154,
	    "shop_name": "Civil Service Consumer Corporation - Zarqa - al rusaifeh",
	    "governorate_id": 17
	},
	{
	    "id": 155,
	    "shop_name": "Civil Service Consumer Corporation - Zarqa - Al Sokhna ",
	    "governorate_id": 17
	},
	{
	    "id": 156,
	    "shop_name": "Civil Service Consumer Corporation - New Zarqa - ",
	    "governorate_id": 17
	},
	{
	    "id": 157,
	    "shop_name": "Civil Service Consumer Corporation - Zarqa - mujama al basat al qadeem",
	    "governorate_id": 17
	},
	{
	    "id": 158,
	    "shop_name": "Civil Service Consumer Corporation - Zarqa - mujama3 bank al eskan (downtown)",
	    "governorate_id": 17
	},
	{
	    "id": 159,
	    "shop_name": "Civil Service Consumer Corporation - Zarqa - Qasr al Halabat city",
	    "governorate_id": 17
	},
	{
	    "id": 160,
	    "shop_name": "Civil Service Consumer Corporation - Mafraq - AL Khaldeyah",
	    "governorate_id": 13
	},
	{
	    "id": 161,
	    "shop_name": "Civil Service Consumer Corporation - Mafraq - Balama",
	    "governorate_id": 13
	},
	{
	    "id": 162,
	    "shop_name": "Civil Service Consumer Corporation - Mafraq - Al Manseyeh",
	    "governorate_id": 13
	},
	{
	    "id": 163,
	    "shop_name": "Civil Service Consumer Corporation - Mafraq - Al Ruwaished",
	    "governorate_id": 13
	},
	{
	    "id": 164,
	    "shop_name": "Civil Service Consumer Corporation - Mafraq - Down town",
	    "governorate_id": 13
	},
	{
	    "id": 165,
	    "shop_name": "Civil Service Consumer Corporation - Mafraq - Umm al qutainal",
	    "governorate_id": 13
	},
	{
	    "id": 166,
	    "shop_name": "Civil Service Consumer Corporation - Mafraq - Zaatari",
	    "governorate_id": 13
	},
	{
	    "id": 167,
	    "shop_name": "Civil Service Consumer Corporation - Maan - Al Mregha",
	    "governorate_id": 11
	},
	{
	    "id": 168,
	    "shop_name": "Civil Service Consumer Corporation - Maan - Al Shaari",
	    "governorate_id": 11
	},
	{
	    "id": 169,
	    "shop_name": "Civil Service Consumer Corporation - Maan - Al Taybeh al Janoubeyeh",
	    "governorate_id": 11
	},
	{
	    "id": 170,
	    "shop_name": "Civil Service Consumer Corporation - Maan - Qurat al Nuaimat",
	    "governorate_id": 11
	},
	{
	    "id": 171,
	    "shop_name": "Civil Service Consumer Corporation - Maan - Souq Maan",
	    "governorate_id": 11
	},
	{
	    "id": 172,
	    "shop_name": "Civil Service Consumer Corporation - Maan - Wadi Musa",
	    "governorate_id": 11
	},
	{
	    "id": 173,
	    "shop_name": "Civil Service Consumer Corporation - Karak - Al Ghor al Safe",
	    "governorate_id": 10
	},
	{
	    "id": 174,
	    "shop_name": "Civil Service Consumer Corporation - Karak - Al Qaser",
	    "governorate_id": 10
	},
	{
	    "id": 175,
	    "shop_name": "Civil Service Consumer Corporation - Karak - Ghor al Mazraa",
	    "governorate_id": 10
	},
	{
	    "id": 176,
	    "shop_name": "Civil Service Consumer Corporation - Karak - Muaab",
	    "governorate_id": 10
	},
	{
	    "id": 178,
	    "shop_name": "Civil Service Consumer Corporation - Karak - Seerfa",
	    "governorate_id": 10
	},
	{
	    "id": 179,
	    "shop_name": "Civil Service Consumer Corporation - Karak - Souk Al Karak",
	    "governorate_id": 10
	},
	{
	    "id": 180,
	    "shop_name": "Civil Service Consumer Corporation - Karak - Souk Muta",
	    "governorate_id": 10
	},
	{
	    "id": 181,
	    "shop_name": "Civil Service Consumer Corporation - Karak - South Mazar",
	    "governorate_id": 10
	},
	{
	    "id": 182,
	    "shop_name": "Civil Service Consumer Corporation - Karak - That Ras",
	    "governorate_id": 10
	},
	{
	    "id": 184,
	    "shop_name": "Civil Service Consumer Corporation - Irbid - al Husun",
	    "governorate_id": 7
	},
	{
	    "id": 185,
	    "shop_name": "Civil Service Consumer Corporation - Irbid - al koora",
	    "governorate_id": 7
	},
	{
	    "id": 186,
	    "shop_name": "Civil Service Consumer Corporation - Irbid - al Mashare",
	    "governorate_id": 7
	},
	{
	    "id": 187,
	    "shop_name": "Civil Service Consumer Corporation - Irbid - Al Shajara",
	    "governorate_id": 7
	},
	{
	    "id": 188,
	    "shop_name": "Civil Service Consumer Corporation - Irbid - Bani Kinana",
	    "governorate_id": 7
	},
	{
	    "id": 189,
	    "shop_name": "Civil Service Consumer Corporation - Irbid - bergish",
	    "governorate_id": 7
	},
	{
	    "id": 190,
	    "shop_name": "Civil Service Consumer Corporation - Irbid - Irbid al sharqi",
	    "governorate_id": 7
	},
	{
	    "id": 191,
	    "shop_name": "Civil Service Consumer Corporation - Irbid - kofor Asad",
	    "governorate_id": 7
	},
	{
	    "id": 192,
	    "shop_name": "Civil Service Consumer Corporation - Irbid - north Mazar",
	    "governorate_id": 7
	},
	{
	    "id": 193,
	    "shop_name": "Civil Service Consumer Corporation - Irbid - north shouneh",
	    "governorate_id": 7
	},
	{
	    "id": 194,
	    "shop_name": "Civil Service Consumer Corporation - Irbid - nuaimiah",
	    "governorate_id": 7
	},
	{
	    "id": 195,
	    "shop_name": "Civil Service Consumer Corporation - Irbid - Ramtha",
	    "governorate_id": 7
	},
	{
	    "id": 196,
	    "shop_name": "Civil Service Consumer Corporation - Irbid - Souq Irbid",
	    "governorate_id": 7
	},
	{
	    "id": 197,
	    "shop_name": "Civil Service Consumer Corporation - Balqa - Aira wo yarga  ",
	    "governorate_id": 5
	},
	{
	    "id": 198,
	    "shop_name": "Civil Service Consumer Corporation - Balqa - al Saru ",
	    "governorate_id": 5
	},
	{
	    "id": 199,
	    "shop_name": "Civil Service Consumer Corporation - Balqa - Al subaihi",
	    "governorate_id": 5
	},
	{
	    "id": 200,
	    "shop_name": "Civil Service Consumer Corporation - Balqa - fuhais",
	    "governorate_id": 5
	},
	{
	    "id": 201,
	    "shop_name": "Civil Service Consumer Corporation - Balqa - Sooq al salt (near bus station)",
	    "governorate_id": 5
	},
	{
	    "id": 202,
	    "shop_name": "Civil Service Consumer Corporation - Amman - ain ghazal",
	    "governorate_id": 2
	},
	{
	    "id": 203,
	    "shop_name": "Civil Service Consumer Corporation - Amman - Al Baiader",
	    "governorate_id": 2
	},
	{
	    "id": 204,
	    "shop_name": "Civil Service Consumer Corporation - Amman - Al jubaiha",
	    "governorate_id": 2
	},
	{
	    "id": 205,
	    "shop_name": "Civil Service Consumer Corporation - Amman - Jwaideh",
	    "governorate_id": 2
	},
	{
	    "id": 206,
	    "shop_name": "Civil Service Consumer Corporation - Amman - Marj Al Hamam",
	    "governorate_id": 2
	},
	{
	    "id": 207,
	    "shop_name": "Civil Service Consumer Corporation - Amman - Naoor",
	    "governorate_id": 2
	},
	{
	    "id": 208,
	    "shop_name": "Civil Service Consumer Corporation - Amman - Ras Al Ain",
	    "governorate_id": 2
	},
	{
	    "id": 209,
	    "shop_name": "Civil Service Consumer Corporation - Amman - Sahab",
	    "governorate_id": 2
	},
	{
	    "id": 211,
	    "shop_name": "Integarated Modern Commercial Markets (Irbid Mall) - Irbid - Kufur Yuba cross road",
	    "governorate_id": 7
	},
	{
	    "id": 212,
	    "shop_name": "Integarated Modern Commercial Markets (Irbid Mall) - Irbid - down twon - Down town",
	    "governorate_id": 7
	},
	{
	    "id": 213,
	    "shop_name": "Integarated Modern Commercial Markets (Irbid Mall) - Mafraq - Al AlBait University St",
	    "governorate_id": 13
	},
	{
	    "id": 214,
	    "shop_name": "Khalid Al Zobi & Jamal Al Hamad Co. - Al Qallaz - Irbid - (Qallaz 3) Ramtha - Al Sham highway (2).",
	    "governorate_id": 7
	},
	{
	    "id": 215,
	    "shop_name": "Mohammad Rashid Yaagoub Shdaifat - Mafraq - Al Hashimi - Aydoon Street",
	    "governorate_id": 13
	},
	{
	    "id": 217,
	    "shop_name": "Mohammad Rashid Yaagoub Shdaifat - Mafraq - Al Mansheyah",
	    "governorate_id": 13
	},
	{
	    "id": 218,
	    "shop_name": "Al-Farid General Supplier  - Balqa - ",
	    "governorate_id": 5
	},
	{
	    "id": 219,
	    "shop_name": "Tazweed Commercial Solutions - Azraq Camp - ",
	    "governorate_id": 4
	},
	{
	    "id": 220,
	    "shop_name": "Kassab Bany Khaled Market – Ali Mall - Mafraq - Main Street - Al Hamra",
	    "governorate_id": 13
	},
	{
	    "id": 221,
	    "shop_name": "Bab Al Deera Markets - Amman - Main Street- Sahab",
	    "governorate_id": 2
	},
	{
	    "id": 222,
	    "shop_name": "Faris Abuolam for Food Trade establishment - Mafraq - Southern District - Mafraq city",
	    "governorate_id": 13
	}]
    //clearDropdown("ShopId");
    //addDropDownItem("ShopId", "", "");
    //var governorate = document.getElementById("Governorate").value;
    //for (var item in shoplist) {
    //    shop = shoplist[item];
    //    if (shop.governorate_id == governorate) {
    //        addDropDownItem("ShopId", shop.id, shop.shop_name);
    //    }
    //}
}
function cascadeDropdown(parent,child, itemList) {
    clearDropdown(child);
    addDropDownItem(child, "", "");
    var key = document.getElementById(parent).value;
    for (var item in itemList) {
        var thisItem = itemList[item];
        if (thisItem.pkey == key) {
            addDropDownItem(child, thisItem.id, thisItem.name);
        }
    }
    document.getElementById(child).value = "";
}
function cascadeDropdownResponsible(parent, child, itemList) {
    clearDropdown(child);
    //addDropDownItem(child, "", "");
    var key = document.getElementById(parent).value;
    for (var item in itemList) {
        var thisItem = itemList[item];
        if (thisItem.pkey == key) {
            addDropDownItem(child, thisItem.id, thisItem.name);
        }
    }
   // document.getElementById(child).value = "";
}
function   changeMonitoringActivity(val)
{
    debugger;
    if (val != "") {
        $("#ddlMonitoringActivitySub").show();
        cascadeDropdown("ddlMonitoringActivity", "ddlMonitoringActivitySub", ddlMonitoringActivitySub);
        if ($('#ddlMonitoringActivitySub').children('option').length > 1) {
            $('#trMonitoringActivitySub').show();
        }
        else {
            $('#trMonitoringActivitySub').hide();
        }
        var ma = $('#ddlMonitoringActivity').val();

        if (ma == 14) { // //outbound calls
            $('#subactivity').append($('<option>', { value: '', text: '' }));
            $('#subactivity').append($('<option>', { value: 'PDM', text: 'PDM' }));
            $('#subactivity').append($('<option>', { value: 'BCM', text: 'BCM' }));
            $('#subactivity').append($('<option>', { value: 'PAB', text: 'PAB' }));
            $('#subactivity').append($('<option>', { value: 'SCI', text: 'SCI' }));
            $('#subactivity').append($('<option>', { value: 'Activity Report', text: 'Activity Report' }));
            $('#subactivity').append($('<option>', { value: 'ABI', text: 'ABI' }));
            $('#subactivity').append($('<option>', { value: 'Cach Distribution', text: 'Cach Distribution' }));

            $('#numberofforms').append($('<option>', { value: 0, text: '0' }));
            $('#numberofforms').append($('<option>', { value: 1, text: '1' }));
            $('#numberofforms').append($('<option>', { value: 2, text: '2' }));
            $('#numberofforms').append($('<option>', { value: 3, text: '3' }));
            $('#numberofforms').append($('<option>', { value: 4, text: '4' }));
            $('#numberofforms').append($('<option>', { value: 5, text: '5' }));
        }
        /*
        if (prog == 14) { //outbound calls
            for (var i = 2; i <= 10; i++) {
                $('#subactivity' + i).append($('<option>', { value: '', text: '' }));
                $('#subactivity' + i).append($('<option>', { value: 'PDM', text: 'PDM' }));
                $('#subactivity' + i).append($('<option>', { value: 'BCM', text: 'BCM' }));
                $('#subactivity' + i).append($('<option>', { value: 'PAB', text: 'PAB' }));
                $('#subactivity' + i).append($('<option>', { value: 'SCI', text: 'SCI' }));
                $('#subactivity' + i).append($('<option>', { value: 'Activity Report', text: 'Activity Report' }));
                $('#subactivity' + i).append($('<option>', { value: 'ABI', text: 'ABI' }));
                $('#subactivity' + i).append($('<option>', { value: 'Cach Distribution', text: 'Cach Distribution' }));
                $('#numberofforms' + i).append($('<option>', { value: 10, text: '0' }));
                $('#numberofforms' + i).append($('<option>', { value: 1, text: '1' }));
                $('#numberofforms' + i).append($('<option>', { value: 2, text: '2' }));
                $('#numberofforms' + i).append($('<option>', { value: 3, text: '3' }));
                $('#numberofforms' + i).append($('<option>', { value: 4, text: '4' }));
                $('#numberofforms' + i).append($('<option>', { value: 5, text: '5' }));
            }
        }
        */
    }
}
function showHideControlsBasedonRecordType(recordType)
{
    //monitoring note 
    if (form1.RecordType.value == 2) {
        $('#trMonitoringActivity').show();
        $('#trSource').hide();
        $('#trOtherExternalSource').hide();
        $('#SubSource-tr').hide();
        $('#trSourceFurtherInfo').hide();
        $('#Category-tr').hide();
        $('#SubCategory-tr').hide();
        $('#ResponsibleUnit-tr').hide();
        // $('#ResponsiblePerson-tr').hide();
        $('#SendEmail').prop('checked', false);
        $('#trRanking').hide();
        $('#trAction').hide();
        $('#trStatus').hide();
        $('#dvNotifyEmails').show();
        $('#dvNotesLabel').text('Notes:');
        $('#trFollowUp').show();
        $('#trUnitsInCopy').hide();
        $('#dvNotifyEmails').hide();
        $('#ResponsiblePerson').val('');
		$('#Severity').val('');

		$('#trNumberofSubActivities').show();
		$('#trSubActivities').show();
		$('#trMainChallenges').show();
		$('#trMainFindings').show();
		$('#trIssuesForFollowUp').show();
		$('#trActionTakenByFM').show();
		
		$('#trTitle').show();
		$('#createticket').hide();
		
		
		
		
        
    }
    else {

        $('#trMonitoringActivity').hide();
        $('#trSource').show();

        

        if ($('#Source').val() == 2) {
            $("#trOtherExternalSource").show();
        }
        else {

            $("#trOtherExternalSource").hide();
        }

        
        $('#SubSource-tr').show();
        $('#trSourceFurtherInfo').show();
        $('#Category-tr').show();
        $('#SubCategory-tr').show();
        $('#ResponsibleUnit-tr').show();
        //$('#ResponsiblePerson-tr').show();
        $('#SendEmail').prop('checked', true);
        $('#trRanking').show();
        $('#trAction').show();
        $('#trStatus').show();
        $('#dvNotifyEmails').show();
        $('#dvNotesLabel').text('Detailed Description of the issue:');
        $('#trFollowUp').hide();
        $('#trUnitsInCopy').show();
        $('#CopyProtectionAdvisor').show();
        $('#dvNotifyEmails').show();
        
		$('#trNumberofSubActivities').hide();
		$('#trSubActivities').hide();
		$('#trMainChallenges').hide();
		$('#trMainFindings').hide();
		$('#trIssuesForFollowUp').hide();
		$('#trActionTakenByFM').hide();
		$('#trTitle').hide();
		$('#createticket').hide();
    }
}
function subprogram_change()
{
    $('#HKMLocationType').val('');
    $('#trHKMLocationType').hide();
    $('#SubProgrammeDetails').val('');
    $('#SubProgrammeDetails-tr').hide();

    if ($('#SubProgramme').val() == '2')
    {
        $('#trHKMLocationType').show();
    }
    else if ($('#SubProgramme').val() == '3')
    {
        $('#SubProgrammeDetails-tr').show();
    }
    else
    {
        $('#HKMLocationType').val('');
        $('#trHKMLocationType').hide();
	}



	var sp = $('#SubProgramme').val();
	if (sp == 14) { // //outbound calls
		$('#subactivity').append($('<option>', { value: '', text: '' }));
		
		$('#subactivity').append($('<option>', { value: 'BCM', text: 'BCM' }));
		

		$('#numberofforms').append($('<option>', { value: 0, text: '0' }));
		$('#numberofforms').append($('<option>', { value: 1, text: '1' }));
		$('#numberofforms').append($('<option>', { value: 2, text: '2' }));
		$('#numberofforms').append($('<option>', { value: 3, text: '3' }));
		$('#numberofforms').append($('<option>', { value: 4, text: '4' }));
		$('#numberofforms').append($('<option>', { value: 5, text: '5' }));



	}


	if (sp == 14) { //outbound calls
		for (var i = 2; i <= 10; i++) {

			$('#subactivity' + i).append($('<option>', { value: '', text: '' }));
			$('#subactivity' + i).append($('<option>', { value: 'BCM', text: 'BCM' }));
			
			$('#numberofforms' + i).append($('<option>', { value: 10, text: '0' }));
			$('#numberofforms' + i).append($('<option>', { value: 1, text: '1' }));
			$('#numberofforms' + i).append($('<option>', { value: 2, text: '2' }));
			$('#numberofforms' + i).append($('<option>', { value: 3, text: '3' }));
			$('#numberofforms' + i).append($('<option>', { value: 4, text: '4' }));
			$('#numberofforms' + i).append($('<option>', { value: 5, text: '5' }));
		}
	}








}
function showUploadModal() {    
        $('#modalUploadFiles').modal('show');
}
//File upload

window.onload = function () {
    document.getElementById('uploader').onsubmit = function () {
        var SavedRecordID = $('#SavedRecordID').val();
        var formdata = new FormData(); //FormData object
        var fileInput = document.getElementById('fileInput');
        //Iterating through each files selected in fileInput
        for (i = 0; i < fileInput.files.length; i++) {
            //Appending each file to FormData object
            formdata.append(fileInput.files[i].name, fileInput.files[i]);
        }
        //Creating an XMLHttpRequest and sending
        var xhr = new XMLHttpRequest();
        xhr.open('POST', '/LogBook/ajax/Upload/');
        xhr.send(formdata);
        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4 && xhr.status == 200) {
                alert(xhr.responseText);
            }
        }
        return false;
	}

	
    //location 1
    bindMap('dvLocation1', 'Lat1', 'Long1', 'us2-address1', 'save-changes', 'discard-changes', 'modal_location1', 'GPSCoordinatesLat', 'GPSCoordinatesLong')
    //location 2
    bindMap('dvLocation2', 'Lat2', 'Long2', 'us2-address2', 'save-changes2', 'discard-changes2', 'modal_location2', 'GPSCoordinatesLat2', 'GPSCoordinatesLong2')
    //location 3
    bindMap('dvLocation3', 'Lat3', 'Long3', 'us2-address3', 'save-changes3', 'discard-changes3', 'modal_location3', 'GPSCoordinatesLat3', 'GPSCoordinatesLong3')
    //location 4
    bindMap('dvLocation4', 'Lat4', 'Long4', 'us2-address4', 'save-changes4', 'discard-changes4', 'modal_location4', 'GPSCoordinatesLat4', 'GPSCoordinatesLong4')
    //location 5
    bindMap('dvLocation5', 'Lat5', 'Long5', 'us2-address5', 'save-changes5', 'discard-changes5', 'modal_location5', 'GPSCoordinatesLat5', 'GPSCoordinatesLong5')
    //location 6
    bindMap('dvLocation6', 'Lat6', 'Long6', 'us2-address6', 'save-changes6', 'discard-changes6', 'modal_location6', 'GPSCoordinatesLat6', 'GPSCoordinatesLong6')
    //location 7
    bindMap('dvLocation7', 'Lat7', 'Long7', 'us2-address7', 'save-changes7', 'discard-changes7', 'modal_location7', 'GPSCoordinatesLat7', 'GPSCoordinatesLong7')
    //location 8
    bindMap('dvLocation8', 'Lat8', 'Long8', 'us2-address8', 'save-changes8', 'discard-changes8', 'modal_location8', 'GPSCoordinatesLat8', 'GPSCoordinatesLong8')
    //location 9
    bindMap('dvLocation9', 'Lat9', 'Long9', 'us2-address9', 'save-changes9', 'discard-changes9', 'modal_location9', 'GPSCoordinatesLat9', 'GPSCoordinatesLong9')
    //location 10
    bindMap('dvLocation10', 'Lat10', 'Long10', 'us2-address10', 'save-changes10', 'discard-changes10', 'modal_location10', 'GPSCoordinatesLat10', 'GPSCoordinatesLong10')
} //Load


// Bind Google map
function bindMap(LocationDIVID, LatControlID, LongControlID, AddressControlID,SaveButtonID,DiscardButtonID, ModalID, LatOnFormID, LongOnFormID )
{
    // google maps API
    $('#' + LocationDIVID).locationpicker({
        location: {
            latitude: 31.95938887925673,
            longitude: 35.85771271534418
        },
        inputBinding: {
            latitudeInput: $('#' + LatControlID),
            longitudeInput: $('#' + LongControlID),
            locationNameInput: $('#' + AddressControlID)
        },
        enableAutocomplete: true,
        radius: 0

    });

    // Current Location and initialize the plugin
    navigator.geolocation.getCurrentPosition(function (position) {
        // You can set it the plugin   
        $('#' + LatControlID).val(position.coords.latitude);
        $('#' + LongControlID).val(position.coords.longitude);
        $('#' + LocationDIVID).locationpicker({
            location: {
                latitude: position.coords.latitude,
                longitude: position.coords.longitude
            },
            inputBinding: {
                latitudeInput: $('#' + LatControlID),
                longitudeInput: $('#' + LongControlID),
                locationNameInput: $('#' + AddressControlID)
            },
            enableAutocomplete: true,
            radius: 0
        });
    });

    // Save Button
    $('#' + SaveButtonID).click(function () {
        $('#' + ModalID).modal('hide');
        $('#' + LatOnFormID).val($('#' + LatControlID).val());
        $('#' + LongOnFormID).val($('#' + LongControlID).val());
    });
    $('#' + DiscardButtonID).click(function () {
        $('#' + ModalID).modal('hide');
       
    });
    
}


