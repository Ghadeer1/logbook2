﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HotlineMVC.Controllers
{
    public class ReportsController : Controller
    {
        // GET: Reports
        public ActionResult Index()
        {
            CommonFunctions.LogActivity("Viewed Reports");
            return View();
        }
        public ActionResult ShowReport(int id)
        {
            string DateCriteria = "";
            if (Request.QueryString["date_from"] != null && Request.QueryString["date_from"] != "")
                DateCriteria += " and date>='" + Request.QueryString["date_from"]+"'";
            if (Request.QueryString["date_to"] != null && Request.QueryString["date_to"] != "")
                DateCriteria += " and date<='" + Request.QueryString["date_to"]+"'";
            string sql = "";
            switch (id)
            {
                case 1:
                    sql = @"select 1 as id, '7 - 30 days' as Age, count(*) as Records, count(*)*100.0/(select count(*) from  logbook_records where status=1) as [percent]
                            from logbook_records where datediff(day,[date],getdate())>7 and  datediff(day,[date],getdate())<=30 and status=1"+DateCriteria+ @"
                            union select 2 as id, '>30 - 60 days' as Age, count(*) as Records,count(*)*100.0/(select count(*) from  logbook_records where status=1) as [percent] 
                            from logbook_records where datediff(day,[date],getdate())>30 and  datediff(day,[date],getdate())<=60 and status=1" + DateCriteria + @"
                            union select 3 as id, '>60 - 90 days' as Age, count(*) as Records,count(*)*100.0/(select count(*) from  logbook_records where status=1) as [percent] 
                            from logbook_records where datediff(day,[date],getdate())>60 and  datediff(day,[date],getdate())<=90 and status=1" + DateCriteria + @"
                            union select 4 as id, 'over 90 days' as Age, count(*) as Records ,count(*)*100.0/(select count(*) from  logbook_records where status=1) as [percent]
                            from logbook_records where datediff(day,[date],getdate())>90 and status=1"+DateCriteria;
                    break;
                case 2:
                    sql = @"select logbook_status.id,logbook_status.status,count(*) as number,count(*)*100.0/(select count(*) from logbook_records) as [percent]  from logbook_records
                            inner join logbook_status on logbook_records.Status=logbook_status.id where RecordNumber>0 " + DateCriteria + @"
                            group by logbook_status.status,logbook_status.id";
                    break;
                case 3:
                    sql = @"select logbook_main_purposes.id,logbook_main_purposes.MainPurpose,count(*) as number,count(*)*100.0/(select count(*) from logbook_records) as [percent]  from logbook_records
                            inner join logbook_main_purposes on logbook_records.MainPurpose=logbook_main_purposes.id where RecordNumber>0 " + DateCriteria + @"
                            group by logbook_main_purposes.MainPurpose,logbook_main_purposes.id";
                    break;
                case 4:
                    sql = @"select logbook_shop_issues.id,logbook_shop_issues.ShopIssue,count(*) as number,count(*)*100.0/(select count(*) from logbook_records where ShopProblem>0) as [percent]  from logbook_records
                            inner join logbook_shop_issues on logbook_records.ShopProblem=logbook_shop_issues.id where RecordNumber>0 " + DateCriteria + @"
                            group by logbook_shop_issues.ShopIssue,logbook_shop_issues.id";
                    break;
                case 5:
                    sql = @"select logbook_protection_issues.id,logbook_protection_issues.ProtectionIssue,count(*) as number,count(*) * 100.0 / (select count(*) from logbook_records where ProtectionIssue > 0) as [percent]  from logbook_records
                           inner join logbook_protection_issues on logbook_records.ProtectionIssue = logbook_protection_issues.id where RecordNumber>0 " + DateCriteria + @"
                           group by logbook_protection_issues.ProtectionIssue,logbook_protection_issues.id";
                    break;
                case 6:
                    sql = @"select logbook_main_purposes.id,logbook_main_purposes.MainPurpose,count(*) as number,count(*)*100.0/(select count(*) from logbook_records where logbook_records.MainPurpose in (1,4,7,8,13,14,18)) as [percent]  from logbook_records
                            inner join logbook_main_purposes on logbook_records.MainPurpose=logbook_main_purposes.id
                            where logbook_records.MainPurpose in (1,4,7,8,13,14,18)" + DateCriteria + @"
                            group by logbook_main_purposes.MainPurpose,logbook_main_purposes.id";
                    break;
                case 7:
                    sql = @"select logbook_governorates.ID,logbook_governorates.Governorate,count(*) as number,count(*)*100.0/(select count(*) from logbook_records) as [percent]  from logbook_records
                            inner join logbook_governorates on logbook_records.Governorate=logbook_governorates.ID where RecordNumber>0 " + DateCriteria + @"
                            group by logbook_governorates.Governorate,logbook_Governorates.ID";
                    break;
                case 8:
                    sql = @"select logbook_referral.id,logbook_referral.Referral,count(*) as number,count(*)*100.0/(select count(*) from logbook_records where logbook_records.Referral>0) as [percent]  from logbook_records
                            inner join logbook_referral on logbook_records.referral=logbook_referral.id where RecordNumber>0 " + DateCriteria + @"
                            group by logbook_referral.Referral,logbook_referral.id";
                    break;
            }
            DataTable dt = CommonFunctions.GetDataFromDb(sql);
            ViewBag.Data = dt;
            ViewBag.ReportId = id;
            return View();
        }
        public ActionResult ShowDetails(jQueryDataTableParamModel param)
        {
            string DateCriteria = "";
            if (Request.QueryString["date_from"] != null && Request.QueryString["date_from"] != "")
                DateCriteria += " and date>='" + Request.QueryString["date_from"] + "'";
            if (Request.QueryString["date_to"] != null && Request.QueryString["date_to"] != "")
                DateCriteria += " and date<='" + Request.QueryString["date_to"] + "'";

            string[] criteria = Request.QueryString["criteria"].Split('|');
            int reportid = int.Parse(criteria[0]);
            int itemid = int.Parse(criteria[1]);

            string sql = "";
            string condition = "";
            string select = @"select RecordNumber,Staff,cast(Date as nvarchar(10)) as Date,CaseNumber,
                            PhoneNumber,CallerNumber,Action,ActionTakenBy,Comments,StatusDesc,ReferralDesc,
                            GovernorateDesc,ShopIssueDesc,MainPurposeDesc,GenderDesc,CashComparativeStudyDesc,SourceDesc,ProtectionIssueDesc,
							ResponsibleUnitDesc,ShopName,ShopBranch,ResponsiblePersonDesc
                            from logbook_records_view ";
            switch (reportid) {
                case 1: //Age report
                    if (itemid == 1)
                    {
                        condition=" where datediff(day,[date],getdate())>7 and  datediff(day,[date],getdate())<=30 and status=1";
                    }
                    else if (itemid == 2)
                    {
                        condition=" where datediff(day,[date], getdate()) > 30 and datediff(day,[date], getdate())<= 60 and status = 1";
                    }
                    else if (itemid == 3)
                    {
                        condition=" where datediff(day,[date], getdate()) > 60 and datediff(day,[date], getdate())<= 90 and status = 1";
                    }
                    else if (itemid == 4)
                    {
                        condition=" where datediff(day,[date],getdate())>90 and status=1";
                    }
                    break;
                case 2:
                    condition=" where logbook_records_view.Status=" + itemid;
                    break;
                case 3:
                    condition=" where logbook_records_view.MainPurpose=" + itemid;
                    break;
                case 4:
                    condition=" where logbook_records_view.ShopProblem=" + itemid;
                    break;
                case 5:
                    condition=" where logbook_records_view.ProtectionIssue = " + itemid;
                    break;
                case 6:
                    condition=" where logbook_records_view.MainPurpose=" + itemid;
                    break;
                case 7:
                    condition=" where logbook_records_view.Governorate=" + itemid;
                    break;
                case 8:
                    condition=" where logbook_records_view.referral=" + itemid;
                    break;
            }
            sql = select + condition + DateCriteria;
            string jsonString = CommonFunctions.GetJson(sql);
            var result= Content(jsonString, "application/json");
            string records = CommonFunctions.GetValueFromDb("select count(*) from logbook_records_view " + condition);
            return Json(new
            {
                sEcho = param.sEcho,
                iTotalRecords = records,
                iTotalDisplayRecords = records,
                aaData = CommonFunctions.GetList(sql)
            },JsonRequestBehavior.AllowGet);
        }
        public ActionResult CreateReport()
        {
            return View();
        }
        public ActionResult DetailedReport()
        {
            List<SelectListItem> RecordType = CommonFunctions.GetListFromDb("select id as Value, RecordTypeDesc as Text from logbook_record_types",false);
            List<SelectListItem> Category = CommonFunctions.GetListFromDb("select id as Value, CategoryDesc as Text from logbook_categories", false);
            List<SelectListItem> MonitoringActivity = CommonFunctions.GetListFromDb("select id as Value, MonitoringActivity as Text from logbook_monitoring_activities_types", false);
            List<SelectListItem> Program = CommonFunctions.GetListFromDb("select id as Value, ProgrammeDesc as Text from logbook_programmes", false);
            List<SelectListItem> SubProgram = CommonFunctions.GetListFromDb("select id as Value, SubProgrammeDesc as Text from logbook_subprogrammes", false);
            List<SelectListItem> Severity = CommonFunctions.GetListFromDb("select id as Value, SeverityDesc as Text from logbook_severity", false);
            List<SelectListItem> Status = CommonFunctions.GetListFromDb("select id as Value, StatusDesc as Text from logbook_status", false);
            List<SelectListItem> Governorate = CommonFunctions.GetListFromDb("select id as Value, GovernorateDesc as Text from logbook_governorates", false);
            List<SelectListItem> District = CommonFunctions.GetListFromDb("select id as Value, DistrictDesc as Text from logbook_districts", false);
            List<SelectListItem> Municipality = CommonFunctions.GetListFromDb("select id as Value, Municipality as Text from logbook_municipality", false);
            List<SelectListItem> ResponsibleUnit = CommonFunctions.GetListFromDb("select id as Value, Unit as Text from units", false);
            //List<SelectListItem> Forms = CommonFunctions.GetListFromDb("select id as Value, RecordTypeDesc as Text from logbook_sources");
            ViewBag.RecordType = RecordType;
            ViewBag.Category = Category;
            ViewBag.MonitoringActivity = MonitoringActivity;
            ViewBag.Program = Program;
            ViewBag.SubProgram = SubProgram;
            ViewBag.Severity = Severity;
            ViewBag.Status = Status;
            ViewBag.Governorate = Governorate;
            ViewBag.District = District;
            ViewBag.Municipality = Municipality;
            ViewBag.ResponsibleUnit = ResponsibleUnit;
            return View();
        }
        [HttpPost]
        public ActionResult ShowDetailedReport(FormCollection field)
        {
            string RecordTypeFilter = field["RecordType"]!=null && field["RecordType"].Trim().Length > 0 ? " AND RecordType in (" + field["RecordType"] + ")" : "";
            string CategoryFilter = field["Category"] != null && field["Category"].Trim().Length > 0 ? " AND Category in (" + field["Category"] + ")" : "";
            string MonitoringActivityFilter = field["MonitoringActivity"] != null && field["MonitoringActivity"].Trim().Length > 0 ? " AND MonitoringActivityID in (" + field["MonitoringActivity"] + ")" : "";
            string CreatedFromFilter = field["CreatedFrom"] != null && field["CreatedFrom"].Trim().Length > 0 ? " AND Date >= '" + field["CreatedFrom"] + "'" : "";
            string CreatedToFilter = field["CreatedTo"] != null && field["CreatedTo"].Trim().Length > 0 ? " AND Date <= '" + field["CreatedTo"]+"'" : "";
            string ProgramFilter = field["Program"] != null && field["Program"].Trim().Length > 0 ? " AND Programme in (" + field["Program"] + ")" : "";
            string SubProgramFilter = field["SubProgram"] != null && field["SubProgram"].Trim().Length > 0 ? " AND SubProgramme in (" + field["SubProgram"] + ")" : "";
            string SeverityFilter = field["Severity"] != null && field["Severity"].Trim().Length > 0 ? " AND Severity in (" + field["Severity"] + ")" : "";
            string StatusFilter = field["Status"] != null && field["Status"].Trim().Length > 0 ? " AND Status in (" + field["Status"] + ")" : "";
            string DateClosedFromFilter = field["DateClosedFrom"] != null && field["DateClosedFrom"].Trim().Length > 0 ? " AND format(cast(DateClosed as date),'yyyy-MM-dd') >='" + field["DateClosedFrom"] +"'": "";
            string DateClosedToFilter = field["DateClosedTo"] != null && field["DateClosedTo"].Trim().Length > 0 ? " AND format(cast(DateClosed as date),'yyyy-MM-dd') <='" + field["DateClosedTo"] +"'": "";
            string GovernorateFilter = field["Governorate"] != null && field["Governorate"].Trim().Length > 0 ? " AND Governorate in (" + field["Governorate"] + ")" : "";
            string DistrictFilter = field["District"] != null && field["District"].Trim().Length > 0 ? " AND District in (" + field["District"] + ")" : "";
            string MunicipalityFilter = field["Municipality"] != null && field["Municipality"].Trim().Length > 0 ? " AND Municipality in (" + field["Municipality"] + ")" : "";
            string ResponsibleUnitFilter = field["ResponsibleUnit"] != null && field["ResponsibleUnit"].Trim().Length > 0 ? " AND ResponsibleUnit in (" + field["ResponsibleUnit"] + ")" : "";
            string FormsFilter = field["Forms"] != null && field["Forms"].Trim().Length > 0 ? " AND (ActivityType1 in ('" + field["Forms"].Replace(",","','") + "')"
                + " OR ActivityType1 in ('" + field["Forms"].Replace(",", "','") + "')" 
                + " OR ActivityType2 in ('" + field["Forms"].Replace(",", "','") + "')"
                + " OR ActivityType3 in ('" + field["Forms"].Replace(",", "','") + "')"
                + " OR ActivityType4 in ('" + field["Forms"].Replace(",", "','") + "')"
                + " OR ActivityType5 in ('" + field["Forms"].Replace(",", "','") + "')"
                + " OR ActivityType6 in ('" + field["Forms"].Replace(",", "','") + "')"
                + " OR ActivityType7 in ('" + field["Forms"].Replace(",", "','") + "')"
                + " OR ActivityType8 in ('" + field["Forms"].Replace(",", "','") + "')"
                + " OR ActivityType9 in ('" + field["Forms"].Replace(",", "','") + "')"
                + " OR ActivityType10 in ('" + field["Forms"].Replace(",", "','") + "'))": "";
            string Filter = RecordTypeFilter + CategoryFilter + MonitoringActivityFilter +
                CreatedFromFilter + CreatedToFilter + ProgramFilter + SubProgramFilter
                + SeverityFilter + StatusFilter + DateClosedFromFilter + DateClosedToFilter
                + GovernorateFilter + DistrictFilter + MunicipalityFilter + ResponsibleUnitFilter
                + FormsFilter;
            string SQL = @"SELECT        RecordTypeDesc AS [Record Type], RecordNumber AS [Record Number], StatusDesc AS Status, Staff AS [Recorded By], Date, Time, ProgrammeDesc AS Programme, SubProgrammeDesc AS [Sub Programme], 
                         GovernorateDesc AS Governorate, DistrictDesc AS District, GenderDesc AS Gender, CategoryDesc AS Category, SubCategoryDesc AS [Sub Category],
                         ResponsibleUnitDesc AS [Responsible Unit],DateClosed,ClosedBy
                        FROM            dbo.logbook_records_view where RecordNumber is not NULL " + Filter;
            ViewBag.ReportHTML = CommonFunctions.GetTableFromSQL(SQL);
            ViewBag.Filter = Filter;
            return View();
        }
        [HttpPost]
        public ActionResult ExportDetailedReport(FormCollection field)
        {
            string sql = @"select RecordNumber,RecordTypeDesc as [Record Type],StatusDesc as Status,
                            SubProgrammeDesc as [Sub Programme],
                            Staff as [Created By],Date,Time,SourceDesc as Source,SubSourceDesc as [Sub Source],
                            SeverityDesc as Severity,CategoryDesc as Category,SubCategoryDesc as [Sub Category],
                            PartnerName as [Partner Name],LocationTypeDesc as [Location Type],
                            GovernorateDesc as Governorate,District,GPSCoordinateLat as Latitude,
                            GPSCoordinateLong as Longitude,CaseNumber,PhoneNumber,Gender,SubCategory,
                            Details,Severity,ShopID,ResponsibleUnitDesc as [Responsible Unit],
                            ResponsiblePersonDesc as [Responsible Person],Action as [Action Taken],
                            DateOfLastAction as [Date of Last Action],ActionTakenBy as [Action Taken By],
                            ClosedBy as [Closed by],DateClosed as [Date Closed],
                            ShopName as [Shop Name],GenderDesc as Gender,
                            ShopBranch as [Shop Branch],[address] as [Shop Address]
                            from [dbo].[logbook_records_view] where RecordNumber is not null " + field["filter"];
            string result = CommonFunctions.GetExcel(sql, "DetailedReport");
            string response = "";
            if (result.Substring(result.Length - 3) == "xls")
            {
                response = CommonFunctions.JsonResponse(200, result);
            }
            else
            {
                response = CommonFunctions.JsonResponse(400, "The excel file couldn't be created because of error: " + result);
            }
            return Content(response, "application/json");
        }
    }
    /// <summary>
    /// Class that encapsulates most common parameters sent by DataTables plugin
    /// </summary>
    public class jQueryDataTableParamModel
    {
        /// <summary>
        /// Request sequence number sent by DataTable,
        /// same value must be returned in response
        /// </summary>       
        public string sEcho { get; set; }

        /// <summary>
        /// Text used for filtering
        /// </summary>
        public string sSearch { get; set; }

        /// <summary>
        /// Number of records that should be shown in table
        /// </summary>
        public int iDisplayLength { get; set; }

        /// <summary>
        /// First record that should be shown(used for paging)
        /// </summary>
        public int iDisplayStart { get; set; }

        /// <summary>
        /// Number of columns in table
        /// </summary>
        public int iColumns { get; set; }

        /// <summary>
        /// Number of columns that are used in sorting
        /// </summary>
        public int iSortingCols { get; set; }

        /// <summary>
        /// Comma separated list of column names
        /// </summary>
        public string sColumns { get; set; }
    }
}