﻿using HotlineMVC.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HotlineMVC.Controllers
{
    public class LogController : Controller
    {
        // GET: Log
        public ActionResult Index()
        {
            List<LogItem> list = GetLogFromDb("select id,activity,doneby,datetime from logbook_log order by id desc");
            return View(list.ToList());
        }
        public static List<LogItem> GetLogFromDb(string SQL)
        {
            DataTable dt = CommonFunctions.GetDataFromDb(SQL);
            List<LogItem> items = new List<LogItem>();
            foreach (DataRow row in dt.Rows)
            {
                items.Add(new LogItem { Activity = row["activity"].ToString(), DoneBy = row["doneby"].ToString(),
                    DateTime = row["datetime"].ToString()
                });
            }
            return items;
        }
    }
}