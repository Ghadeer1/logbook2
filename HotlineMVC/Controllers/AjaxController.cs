﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace HotlineMVC.Controllers
{
    public class AjaxController : Controller
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["LogBookDb"].ToString());
        // GET: Ajax
        public ActionResult Index()
        {
            return Json(new { foo = "bar", baz = "Blech" });
        }
        public ActionResult LoadData()
        {
            //string sql = @"select RecordNumber,RecordTypeDesc as [Record Type],StatusDesc as Status,
            //                SubProgrammeDesc as [Sub Programme],
            //                Staff as [Created By],Date,Time,SourceDesc as Source,SubSourceDesc as [Sub Source],
            //                SeverityDesc as Severity,CategoryDesc as Category,SubCategoryDesc as [Sub Category],
            //                PartnerName as [Partner Name],LocationTypeDesc as [Location Type],
            //                GovernorateDesc as Governorate,District,GPSCoordinateLat as Latitude,
            //                GPSCoordinateLong as Longitude,CaseNumber,PhoneNumber,Gender,SubCategory,
            //                Details,Severity,ShopID,ResponsibleUnitDesc as [Responsible Unit],
            //                ResponsiblePersonDesc as [Responsible Person],Action as [Action Taken],
            //                DateOfLastAction as [Date of Last Action],ActionTakenBy as [Action Taken By],
            //                ClosedBy as [Closed by],DateClosed as [Date Closed],
            //                ShopName as [Shop Name],GenderDesc as Gender,
            //                ShopBranch as [Shop Branch],[address] as [Shop Address]
            //                from logbook_records_view";


            string sql = @"select RecordTypeDesc [Record Type], Staff [Created By],Date [Date Created], Time [Time Created],"
                          + "Details, ResponsibleUnitDesc [Responsible Unit], ResponsiblePersonDesc [Responsible Staff],"
                          + "ProgrammeDesc Programme, SubProgrammeDesc [Sub Programme], SubProgrammeDetails [Sub Programme Details],"
                          + "SourceDesc Source, SubSourceDesc [Sub Source]," 
                          + "GovernorateDesc Governorate, DistrictDesc District,"
                          + "CategoryDesc Category, SubCategoryDesc [Sub Category]," 
                          + "SeverityDesc Severity, StatusDesc Status, MonitoringActivity [Monitoring Activity], MonitoringActivitySub [Sub Monitoring Activity], HKMLocationType [HKM Location Type]," 
                          + "ClosedBy [ClosedBy], DateClosed as [Date Closed]  "
                          + "from logbook_records_view";
            
            return Content(CommonFunctions.GetCSV(sql));
        }
        public ActionResult LoadHelpdesk()
        {
            string sql = @"SELECT TOP (1000) [ID]
                  ,[RecordNumber]
                  ,[Date]
                  ,[Time]
                  ,[Governorate]
                  ,[Partner]
                  ,[CBO]
                  ,[Staff]
                  ,[AddressedOnSpot]
                  ,[ReplacementRequired]
                  ,[ResponsibleUnit]
                  ,[GovernorateDesc]
                  ,[PartnerDesc]
                  ,[CBODesc]
                  ,[AddressedOnSpotDesc]
                  ,[ReplacementRequiredDesc]
                  ,[ResponsibleUnitDesc]
                  ,[Status]
                  ,[CaseNumber]
                  ,[PhoneNumber]
                  ,[Gender]
                  ,[GenderDesc]
                  ,[StatusDesc]
                  ,[Subcategory]
                  ,[CategoryDesc]
                  ,[SubcategoryDesc]
                  ,[Category]
              FROM [logbook].[dbo].[logbook_helpdesk_view]";
            return Content(CommonFunctions.GetCSV(sql));
        }

        public ActionResult GetMainPurpose()
        {
            string SQL = @"SELECT DISTINCT 0 AS value, CASE WHEN SubCategoryDesc IS NULL THEN '' ELSE SubCategoryDesc END as Text  FROM dbo.logbook_records_view  ";
            string jsonString = CommonFunctions.GetJson(SQL);
            return Content(jsonString, "application /json");
        }


        public ActionResult getRecordsTypesForFilter()
        {
            string SQL = @"select 1 as Value, 'Ticket' as Text UNION select 2 as Value, 'Monitoring Note' as Text";
            string jsonString = CommonFunctions.GetJson(SQL);
            return Content(jsonString, "application /json");
        }

        public ActionResult getSourcesForFilter()
        {
            string SQL = @"select 1 as Value, 'Internal' as Text UNION select 2 as Value, 'External' as Text";
            string jsonString = CommonFunctions.GetJson(SQL);
            return Content(jsonString, "application /json");
        }

        public ActionResult getStatusesForFilter()
        {
            string SQL = @"select 1 as Value, 'Open' as Text UNION select 2 as Value, 'Closed' as Text";
            string jsonString = CommonFunctions.GetJson(SQL);
            return Content(jsonString, "application /json");
        }

        public ActionResult getRecordedByForFilter()
        {
            string SQL = @"select distinct '0' as Value, Staff as Text from logbook_records";
            string jsonString = CommonFunctions.GetJson(SQL);
            return Content(jsonString, "application /json");
        }

        public ActionResult getGovernorateForFilter()
        {
            string SQL = @"select distinct '0' as Value, GovernorateDesc as Text from logbook_Governorates";
            string jsonString = CommonFunctions.GetJson(SQL);
            return Content(jsonString, "application /json");
        }
        public ActionResult getDistrictForFilter()
        {
            string SQL = @"select distinct '0' as Value, LTRIM(RTRIM([DistrictDesc])) as Text from logbook_districts";
            string jsonString = CommonFunctions.GetJson(SQL);
            return Content(jsonString, "application /json");
        }

        public ActionResult getSubProgramForFilter()
        {
            string SQL = @"select distinct '0' as Value, LTRIM(RTRIM([SubProgrammeDesc])) as Text from logbook_subprogrammes";
            string jsonString = CommonFunctions.GetJson(SQL);
            return Content(jsonString, "application /json");
        }

        public ActionResult getResponsibleUnitForFilter()
        {
            string SQL = @"select distinct '0' as Value, LTRIM(RTRIM([Unit])) as Text from units";
            string jsonString = CommonFunctions.GetJson(SQL);
            return Content(jsonString, "application /json");
        }
        public ActionResult checkBeneficiaryId(string id)
        {
            string SQL = @"select count(*) from [jordandb].[dbo].[prepaid_reload_wfp] 
                where Beneficiary_ID='"+id+"';";
            string rows = CommonFunctions.GetValueFromDb(SQL);
            if (int.Parse(rows) > 0)
            {
                return Content(CommonFunctions.JsonResponse(200, "Beneficiary exists"));
            }
            else
            {
                return Content(CommonFunctions.JsonResponse(400, "Unknown Beneficiairy"));
            }
        }

        public ActionResult getPartnerForFilter()
        {
            string SQL = @"select distinct '0' as Value, LTRIM(RTRIM([PartnerDesc])) as Text from logbook_helpdesk_view";
            string jsonString = CommonFunctions.GetJson(SQL);
            return Content(jsonString, "application /json");
        }

        public ActionResult getCBOForFilter()
        {
            string SQL = @"select distinct '0' as Value, LTRIM(RTRIM([CBODesc])) as Text from logbook_helpdesk_view";
            string jsonString = CommonFunctions.GetJson(SQL);
            return Content(jsonString, "application /json");
        }
        public ActionResult getHelpdeskCategoryForFilter()
        {
            string SQL = @"select distinct '0' as Value, LTRIM(RTRIM([CategoryDesc])) as Text from logbook_helpdesk_view";
            string jsonString = CommonFunctions.GetJson(SQL);
            return Content(jsonString, "application /json");
        }
        public ActionResult getHelpdeskSubcategoryForFilter()
        {
            string SQL = @"select distinct '0' as Value, LTRIM(RTRIM([SubcategoryDesc])) as Text from logbook_helpdesk_view";
            string jsonString = CommonFunctions.GetJson(SQL);
            return Content(jsonString, "application /json");
        }

        public ActionResult getAddressedOnSpotForFilter()
        {
            string SQL = @"select distinct '0' as Value, LTRIM(RTRIM([AddressedOnSpotDesc])) as Text from logbook_helpdesk_view";
            string jsonString = CommonFunctions.GetJson(SQL);
            return Content(jsonString, "application /json");
        }

        public ActionResult getCategoryForFilter()
        {
            string SQL = @"select distinct '0' as Value, LTRIM(RTRIM([CategoryDesc])) as Text from [dbo].[logbook_categories]";
            string jsonString = CommonFunctions.GetJson(SQL);
            return Content(jsonString, "application /json");
        }

        public ActionResult getGenderForFilter()
        {
            string SQL = @"select distinct '0' as Value, LTRIM(RTRIM([GenderDesc])) as Text from [dbo].[logbook_gender]";
            string jsonString = CommonFunctions.GetJson(SQL);
            return Content(jsonString, "application /json");
        }
        public ActionResult getProgramForFilter()
        {
            string SQL = @"select 1 as Value, 'GFA' as Text UNION select 2 as Value, 'Resilience' as Text UNION select 2 as Value, 'School Meals' as Text  UNION select 1 as Value, 'GFA/OSM' as Text  UNION select 1 as Value, 'GFA/Price Monitoring' as Text UNION select 7 as Value, 'Forestry' as Text UNION select 8 as Value, 'Rehab' as Text ";
            string jsonString = CommonFunctions.GetJson(SQL);
            return Content(jsonString, "application /json");
        }

        [HttpPost]
        public ActionResult Create(FormCollection field)
        {

            try
            {
                string response = "";
                string ClosedBy = "";
                string DateClosed = "";
                if (field["Status"] == "2")
                {
                    string str = field["Staff"].Replace(".", " ");
                    ClosedBy = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(str.ToLower());
                    DateClosed = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");
                }
                string prefix = "";
                if (field["RecordType"] == "1")
                    prefix = "T";
                else if (field["RecordType"] == "2")
                    prefix = "M";

                if (field["Status"] == "2")
                {
                    field["ClosedBy"] = field["Staff"];
                    field["DateClosed"] = DateTime.Now.ToString();
                }

                string newRecordSql = "";
                string sql = "";
                int rows = 0;
                string recType = "";
                ArrayList InsertedMonitoringNotes = new ArrayList();
                if (field["RecordType"].ToString() == "1") //Ticket
                {
                    recType = "T";
                    newRecordSql = "select Concat('" + prefix + "',(max(    cast(SUBSTRING(recordnumber,2,50) as int)   )+1)) from logbook_records where RecordType=" + field["RecordType"];
                    sql = string.Format(@"INSERT INTO logbook_records (RecordNumber,RecordType,Status,SubProgramme,
                Staff,Date,Time,CopyProtectionAdvisor,CopyGenderFocalPoint,SubSource,PartnerName,Governorate,
                District,GPSCoordinateLat,GPSCoordinateLong,CaseNumber,PhoneNumber,Gender,SubCategory,Details,
                Severity,ShopID,ResponsibleUnit,ResponsiblePerson,Action,DateOfLastAction,ActionTakenBy,
                ClosedBy,DateClosed,ReportPath, OtherExternalSource, SourceFurtherInfo, Location2Gov, Location2Dis, Location3Gov, Location3Dis, Location4Gov, Location4Dis, Location5Gov, Location5Dis, Location6Gov, Location6Dis, Location7Gov, Location7Dis, Location8Gov, Location8Dis, Location9Gov, Location9Dis, Location10Gov, Location10Dis, MonitoringActivityID, MonitoringActivitySubID, FollowUpDate, HKMLocationType, SubProgrammeDetails, GPSCoordinateLat2, GPSCoordinateLong2,GPSCoordinateLat3, GPSCoordinateLong3,GPSCoordinateLat4, GPSCoordinateLong4,GPSCoordinateLat5, GPSCoordinateLong5,GPSCoordinateLat6, GPSCoordinateLong6, GPSCoordinateLat7, GPSCoordinateLong7,GPSCoordinateLat8, GPSCoordinateLong8,GPSCoordinateLat9, GPSCoordinateLong9,GPSCoordinateLat10, GPSCoordinateLong10, OtherFMOs, Title, ActivityType1,NumberofForms1, ActivityType2,NumberofForms2,ActivityType3,NumberofForms3,ActivityType4,NumberofForms4,ActivityType5,NumberofForms5,ActivityType6,NumberofForms6,ActivityType7,NumberofForms7, ActivityType8,NumberofForms8,ActivityType9,NumberofForms9,ActivityType10,NumberofForms10,MainChallenges,MainFindings,IssuestoFollowUp,ActionTakenbyFM) Select (" + newRecordSql + @"),
                {0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},
                {20},{21},{22},{23},{24},{25},{26},{27},{28}, {29},{30}, {31}, {32}, {33}, {34}, {35}, {36}, {37}, {38}, {39}, {40}, {41}, {42}, {43}, {44}, {45}, {46}, {47}, {48}, {49},{50}, {51}, {52}, {53},    {54}, {55}, {56}, {57}, {58}, {59}, {60}, {61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {70}, {71}, {72}, {73},{74},{75},{76},{77},{78},{79},{80},{81},{82},{83},{84},{85},{86},{87},{88},{89},{90},{91},{92},{93},{94},{95},{96},{97} ",
                       SqlValue(field["RecordType"], true),
                       SqlValue(field["Status"], true),
                       SqlValue(field["SubProgramme"], true),
                       SqlValue(field["Staff"], true),
                       SqlValue(DateTime.Now.ToString("yyyy-MM-dd"), true),
                       SqlValue(DateTime.Now.ToString("hh:mm:ss"), true),
                       SqlValue(field["CopyProtectionAdvisor"], true),
                       SqlValue(field["CopyGenderFocalPoint"], true),
                       SqlValue(field["SubSource"], true),
                       SqlValue(field["PartnerName"], true),
                       SqlValue(field["Governorate"], true),
                       SqlValue(field["District"], true),
                       SqlValue(field["GPSCoordinatesLat"], true),
                       SqlValue(field["GPSCoordinatesLong"], true),
                       SqlValue(field["CaseNumber"], true),
                       SqlValue(field["PhoneNumber"], true),
                       SqlValue(field["Gender"], true),
                       SqlValue(field["SubCategory"], true),
                       SqlValue(field["Details"], true),
                       SqlValue(field["Severity"], true),
                       SqlValue(field["ShopID"], true),
                       SqlValue(field["ResponsibleUnit"], true),
                       SqlValue(field["ResponsiblePerson"], true),
                       SqlValue(field["Action"], true),
                       SqlValue(field["DateOfLastAction"], true),
                       SqlValue(field["ActionTakenBy"], true),

                       SqlValue(field["ClosedBy"], true),
                       SqlValue(field["DateClosed"], true),
                       SqlValue(field["ReportPath"], true),
                       SqlValue(field["txtOtherExternalSource"], true),
                       SqlValue(field["txtSourceFurtherInfo"], true),

                       SqlValue(field["Governorate2"], true),
                       SqlValue(field["District2"], true),
                       SqlValue(field["Governorate3"], true),
                       SqlValue(field["District3"], true),
                       SqlValue(field["Governorate4"], true),
                       SqlValue(field["District4"], true),
                       SqlValue(field["Governorate5"], true),
                       SqlValue(field["District5"], true),
                       SqlValue(field["Governorate6"], true),
                       SqlValue(field["District6"], true),
                       SqlValue(field["Governorate7"], true),
                       SqlValue(field["District7"], true),
                       SqlValue(field["Governorate8"], true),
                       SqlValue(field["District8"], true),
                       SqlValue(field["Governorate9"], true),
                       SqlValue(field["District9"], true),
                       SqlValue(field["Governorate10"], true),
                       SqlValue(field["District10"], true),
                       SqlValue(field["ddlMonitoringActivity"], true),
                       SqlValue(field["ddlMonitoringActivitySub"], true),
                       SqlValue(field["txtFollowUp"], true),
                       SqlValue(field["HKMLocationType"], true),
                       SqlValue(field["SubProgrammeDetails"], true),
                       SqlValue(field["GPSCoordinatesLat2"], true),
                       SqlValue(field["GPSCoordinatesLong2"], true),
                       SqlValue(field["GPSCoordinatesLat3"], true),
                       SqlValue(field["GPSCoordinatesLong3"], true),
                       SqlValue(field["GPSCoordinatesLat4"], true),
                       SqlValue(field["GPSCoordinatesLong4"], true),
                       SqlValue(field["GPSCoordinatesLat5"], true),
                       SqlValue(field["GPSCoordinatesLong5"], true),
                       SqlValue(field["GPSCoordinatesLat6"], true),
                       SqlValue(field["GPSCoordinatesLong6"], true),
                       SqlValue(field["GPSCoordinatesLat7"], true),
                       SqlValue(field["GPSCoordinatesLong7"], true),
                       SqlValue(field["GPSCoordinatesLat8"], true),
                       SqlValue(field["GPSCoordinatesLong8"], true),
                       SqlValue(field["GPSCoordinatesLat9"], true),
                       SqlValue(field["GPSCoordinatesLong9"], true),
                       SqlValue(field["GPSCoordinatesLat10"], true),
                       SqlValue(field["GPSCoordinatesLong10"], true),
                       SqlValue(field["OtherFMOs"], true),
                       SqlValue(field["Title"], true),
                       SqlValue(field["subactivity"], true),
                       SqlValue(field["numberofforms"], true),
                       SqlValue(field["subactivity2"], true),
                       SqlValue(field["numberofforms2"], true),
                       SqlValue(field["subactivity3"], true),
                       SqlValue(field["numberofforms3"], true),
                       SqlValue(field["subactivity4"], true),
                       SqlValue(field["numberofforms4"], true),
                       SqlValue(field["subactivity5"], true),
                       SqlValue(field["numberofforms5"], true),
                       SqlValue(field["subactivity6"], true),
                       SqlValue(field["numberofforms6"], true),
                       SqlValue(field["subactivity7"], true),
                       SqlValue(field["numberofforms7"], true),
                       SqlValue(field["subactivity8"], true),
                       SqlValue(field["numberofforms8"], true),
                       SqlValue(field["subactivity9"], true),
                       SqlValue(field["numberofforms9"], true),
                       SqlValue(field["subactivity10"], true),
                       SqlValue(field["numberofforms10"], true),
                       SqlValue(field["mainchallenges"], true),
                       SqlValue(field["MainFindings"], true),
                       SqlValue(field["IssuesForFollowUp"], true),
                       SqlValue(field["ActionTakenByFM"], true)
                       
                    );
                    rows = CommonFunctions.RunCommand(sql);
                }
                else
                {
                    recType = "M";
                    int numberOfLocations = Convert.ToInt16(field["numberoflocations"]);
                    string IDindex;
                    int i = 0;
                    if (i == 0)
                    {
                        IDindex = "";
                    }
                    else
                    {
                        IDindex = (i + 1).ToString();
                    }

                    rows = insertMonitoringNote(field, prefix, SqlValue(field[string.Format("Governorate{0}", IDindex)], true), SqlValue(field[string.Format("District{0}", IDindex)], true), SqlValue(field[string.Format("GPSCoordinatesLat{0}", IDindex)], true), SqlValue(field[string.Format("GPSCoordinatesLong{0}", IDindex)], true));
                    if (rows > 0)
                    {
                        string latest = CommonFunctions.GetValueFromDb(string.Format("Select '{0}'+ cast(max(cast(SUBSTRING(recordnumber,2,50) as int)) as varchar(10)) from logbook_records where RecordType=", recType) + field["RecordType"]);
                        InsertedMonitoringNotes.Add(latest);
                    }
                }
                if (rows > 0)
                {
                    string latest = CommonFunctions.GetValueFromDb(string.Format("Select '{0}'+ cast(max(cast(SUBSTRING(recordnumber,2,50) as int)) as varchar(10)) from logbook_records where RecordType=", recType) + field["RecordType"]);
                    field["SavedRecordID"] = latest.ToString();

                    //check if there are files uploaded and add their info to DB
                    string username = Session["username"].ToString();
                    SqlCommand cmd = new SqlCommand();
                    string[] files = System.IO.Directory.GetFiles(Server.MapPath("~/Attatchments"), string.Format("*{0}*", username), System.IO.SearchOption.TopDirectoryOnly);
                    if (files.Length > 0)
                    {
                        Directory.CreateDirectory(Server.MapPath(string.Format("~/Attatchments/{0}", username)));
                        foreach (string s in files)
                        {
                            cmd.Parameters.Clear();
                            cmd.CommandText = "DocumentAdd";
                            cmd.Parameters.AddWithValue("Path", username + "/" + Path.GetFileName(s));
                            cmd.Parameters.AddWithValue("RecordNumber", latest);
                            CommonFunctions.RunSPCommand(cmd);

                            FileInfo f = new FileInfo(s);
                            f.MoveTo(string.Format(Server.MapPath("~/Attatchments/{0}/" + Path.GetFileName(s)), username));


                            if (InsertedMonitoringNotes.Count > 0)
                            {
                                foreach (string item in InsertedMonitoringNotes)
                                {
                                    cmd.Parameters.Clear();
                                    cmd.CommandText = "DocumentAdd";
                                    cmd.Parameters.AddWithValue("Path", username + "/" + Path.GetFileName(s));
                                    cmd.Parameters.AddWithValue("RecordNumber", item);
                                    CommonFunctions.RunSPCommand(cmd);
                                }
                            }

                        }
                    }



                    bool emailRecordExists = false;
                    response = CommonFunctions.JsonResponse(200, "The record was saved successfully with record number " + latest, field["Staff"]);
                    CommonFunctions.LogActivity("Created record number: " + latest, field["Staff"]);
                    CommonFunctions.LogRecordChanges(latest, "Record Created", field["Staff"]);
                    string From = field["Staff"] + "@wfp.org";

                    string CC = "";
                    string RecordUrl = "https://cbttriangulation.jor.wfp.org/LogBook/logbook?grid-filter=RecordNumber__1__" + latest;


                    cmd.Connection = con;
                    // Notify list of people 
                    if (field["RecordType"].ToString() != "2")
                    {
                        string[] ListToNotify = { "joan.sherko@wfp.org", "rana.alrefaay@wfp.org", "rabab.mosleh@wfp.org", "fathi.dado@wfp.org", "oscar.lindow@wfp.org" };

                        for (int i = 0; i <= ListToNotify.Length - 1; i++)
                        {
                            string Message = "Dear " + CommonFunctions.GetFirstNameFromEmail(ListToNotify[i]) + ",<br /><br />" +
                           "This is to inform you that a new ticket was created in the logbook system. See the details below:<br /><br />"
                           + CommonFunctions.RecordView(latest)
                           + "<br /> Click " + string.Format("<a href='{0}'>Here", RecordUrl) + "</a>" + " to view the ticket<br />"
                           + "<br /> Thanks, <br />" + CommonFunctions.GetFirstNameFromEmail(From);

                            emailRecordExists = Logbook_Sent_Emails_Log_Check(cmd, latest, ListToNotify[i]);
                            if (!(emailRecordExists))
                            {
                                CommonFunctions.SendEmail(From, ListToNotify[i], "Log Book Ticket#: " + latest, Message, "", CC);
                                Logbook_Sent_Emails_Log_Insert(cmd, latest, ListToNotify[i]);
                            }

                        }
                    }


                    // Assigned to Unit
                    string To = "";
                    if (field["SendEmail"] == "on" && field["ResponsiblePerson"].Length > 0)
                    {
                        To = (To.Length > 5 ? To + ";" : "") + CommonFunctions.GetStaffEmail(field["ResponsiblePerson"]);
                    }
                    if (To.Length > 5 || CC.Length > 5)
                    {

                        string Message = "Dear " + CommonFunctions.GetFirstNameFromEmail(To) + ",<br /><br />" +
                            "This is to inform you that your unit was assigned a ticket by the logbook system. See the details below:<br /><br />"
                            + CommonFunctions.RecordView(latest)
                            + "<br /> Click " + string.Format("<a href='{0}'>Here", RecordUrl) + "</a>" + " to view the ticket<br />"
                            + "<br /> Thanks, <br />" + CommonFunctions.GetFirstNameFromEmail(From);

                        emailRecordExists = Logbook_Sent_Emails_Log_Check(cmd, latest, To);
                        if (!(emailRecordExists))
                        {
                            CommonFunctions.SendEmail(From, To, "Log Book Ticket#: " + latest, Message, "", CC);
                            Logbook_Sent_Emails_Log_Insert(cmd, latest, To);
                        }


                    }

                    string UnitsInCopy = "";
                    To = "";
                    if (!(string.IsNullOrEmpty(field["hfCopyOtherUnits"].ToString())))
                    {
                        UnitsInCopy = field["hfCopyOtherUnits"].ToString();
                    }

                    // Copy other units 
                    if (!(string.IsNullOrEmpty(UnitsInCopy)))
                    {
                        foreach (string StaffID in UnitsInCopy.Split(','))
                        {
                            To = CommonFunctions.GetStaffEmail(StaffID);
                            string Message = "Dear " + CommonFunctions.GetFirstNameFromEmail(To) + ",<br /><br />" +
                           "This is to inform you that a new ticket was created in the logbook system. See the details below:<br /><br />"
                           + CommonFunctions.RecordView(latest)
                           + "<br /> Click " + string.Format("<a href='{0}'>Here", RecordUrl) + "</a>" + " to view the ticket<br />"
                           + "<br /> Thanks, <br />" + CommonFunctions.GetFirstNameFromEmail(From);
                            emailRecordExists = Logbook_Sent_Emails_Log_Check(cmd, latest, To);
                            if (!(emailRecordExists))
                            {
                                CommonFunctions.SendEmail(From, To, "Log Book Ticket#: " + latest, Message, "", CC);
                                Logbook_Sent_Emails_Log_Insert(cmd, latest, To);
                            }

                        }
                    }

                    // Copy HOP if severity is high
                    if (field["Severity"] == "3")
                    {

                        To = CommonFunctions.GetValueFromDb("SELECT email FROM logbook_responsiblePerson_vw WHERE unit = 'Programme – HoP'");
                        string Message = "Dear " + CommonFunctions.GetFirstNameFromEmail(To) + ",<br /><br />" +
                       "This is to inform you that a new ticket was created in the logbook system with high severity. See the details below:<br /><br />"
                       + CommonFunctions.RecordView(latest)
                       + "<br /> Click " + string.Format("<a href='{0}'>Here", RecordUrl) + "</a>" + " to view the ticket<br />"
                       + "<br /> Thanks, <br />" + CommonFunctions.GetFirstNameFromEmail(From);

                        emailRecordExists = Logbook_Sent_Emails_Log_Check(cmd, latest, To);
                        if (!(emailRecordExists))
                        {
                            CommonFunctions.SendEmail(From, To, "Log Book Ticket#: " + latest, Message, "", CC);
                            Logbook_Sent_Emails_Log_Insert(cmd, latest, To);
                        }

                        To = CommonFunctions.GetValueFromDb("SELECT email FROM logbook_responsiblePerson_vw WHERE unit = 'Programme - M & E'");
                        Message = "Dear " + CommonFunctions.GetFirstNameFromEmail(To) + ",<br /><br />" +
                       "This is to inform you that a new ticket was created in the logbook system with high severity. See the details below:<br /><br />"
                       + CommonFunctions.RecordView(latest)
                       + "<br /> Click " + string.Format("<a href='{0}'>Here", RecordUrl) + "</a>" + " to view the ticket<br />"
                       + "<br /> Thanks, <br />" + CommonFunctions.GetFirstNameFromEmail(From);

                        emailRecordExists = Logbook_Sent_Emails_Log_Check(cmd, latest, To);
                        if (!(emailRecordExists))
                        {
                            CommonFunctions.SendEmail(From, To, "Log Book Ticket#: " + latest, Message, "", CC);
                            Logbook_Sent_Emails_Log_Insert(cmd, latest, To);
                        }

                        To = CommonFunctions.GetValueFromDb("SELECT email FROM logbook_responsiblePerson_vw WHERE unit = 'VAM'");
                        Message = "Dear " + CommonFunctions.GetFirstNameFromEmail(To) + ",<br /><br />" +
                       "This is to inform you that a new ticket was created in the logbook system with high severity. See the details below:<br /><br />"
                       + CommonFunctions.RecordView(latest)
                       + "<br /> Click " + string.Format("<a href='{0}'>Here", RecordUrl) + "</a>" + " to view the ticket<br />"
                       + "<br /> Thanks, <br />" + CommonFunctions.GetFirstNameFromEmail(From);

                        emailRecordExists = Logbook_Sent_Emails_Log_Check(cmd, latest, To);
                        if (!(emailRecordExists))
                        {
                            CommonFunctions.SendEmail(From, To, "Log Book Ticket#: " + latest, Message, "", CC);
                            Logbook_Sent_Emails_Log_Insert(cmd, latest, To);
                        }

                    }

                    //Copy protection advisor or gender focal point
                    string recordType = "";
                    if (field["RecordType"].ToString() == "2")
                    {
                        recordType = "monitoring note";
                    }
                    else
                    {
                        recordType = "ticket";
                    }

                    // if copy protection advisor is checked 
                    if (field["CopyProtectionAdvisor"] != null)
                    {
                        if (field["CopyProtectionAdvisor"].ToString() == "1")
                        {
                            To = "khaled.abdelfadil@wfp.org";
                            string Message = "Dear " + "khaled" + ",<br /><br />" +
                           string.Format("This is to inform you that a new {0} was created in the logbook system. See the details below:<br /><br />", recordType)
                           + CommonFunctions.RecordView(latest)
                           + "<br /> Click " + string.Format("<a href='{0}'>Here", RecordUrl) + "</a>" + " to view the ticket<br />"
                           + "<br /> Thanks, <br />" + CommonFunctions.GetFirstNameFromEmail(From);
                            emailRecordExists = Logbook_Sent_Emails_Log_Check(cmd, latest, To);
                            if (!(emailRecordExists))
                            {
                                CommonFunctions.SendEmail(From, To, "Log Book Ticket#: " + latest, Message, "", CC);
                                Logbook_Sent_Emails_Log_Insert(cmd, latest, To);
                            }

                        }
                    }

                    // if copy gender focal point is checked 
                    if (field["CopyGenderFocalPoint"] != null)
                    {
                        if ((!(string.IsNullOrEmpty(field["CopyGenderFocalPoint"]))) & field["CopyGenderFocalPoint"].ToString() == "1")
                        {
                            To = "rawan.alabbas@wfp.org";
                            string Message = "Dear " + "Rawan" + ",<br /><br />" +
                           string.Format("This is to inform you that a new {0} was created in the logbook system. See the details below:<br /><br />", recordType)
                           + CommonFunctions.RecordView(latest)
                           + "<br /> Click " + string.Format("<a href='{0}'>Here", RecordUrl) + "</a>" + " to view the ticket<br />"
                           + "<br /> Thanks, <br />" + CommonFunctions.GetFirstNameFromEmail(From);

                            emailRecordExists = Logbook_Sent_Emails_Log_Check(cmd, latest, To);
                            if (!(emailRecordExists))
                            {
                                CommonFunctions.SendEmail(From, To, "Log Book Ticket#: " + latest, Message, "", CC);
                                Logbook_Sent_Emails_Log_Insert(cmd, latest, To);
                            }
                        }
                    }

                    if (recordType == "monitoring note")
                    {
                        string[] ListToNotify;
                        string headEmail =  CommonFunctions.GetValueFromDb(string.Format("select top 1 HeadEmail from vw_Staff_SO where StaffEmail = '{0}'", field["Staff"] + "@wfp.org"));
                        if (headEmail == "ali.alhebshi@wfp.org")
                            ListToNotify = new string[] {headEmail, "reema.alnajjar@wfp.org", "hiba.mohammad@wfp.org" };
                        else
                            ListToNotify = new string[] { headEmail, "fathi.dado@wfp.org" };
                        for (int i = 0; i <= ListToNotify.Length - 1; i++)
                        {
                        string Message = "Dear " + CommonFunctions.GetFirstNameFromEmail(ListToNotify[i]) + ",<br /><br />" +
                           "This is to inform you that a new report was created in the logbook system. See the details below:<br /><br />"
                           + CommonFunctions.RecordViewForReport(latest)
                           + "<br /> Click " + string.Format("<a href='{0}'>Here", RecordUrl) + "</a>" + " to view the report<br />"
                           + "<br /> Thanks, <br />" + CommonFunctions.GetFirstNameFromEmail(From);

                            emailRecordExists = Logbook_Sent_Emails_Log_Check(cmd, latest, ListToNotify[i]);
                            if (!(emailRecordExists))
                            {
                                CommonFunctions.SendEmail(From, ListToNotify[i], "Log Book report#: " + latest, Message, "", CC);
                                Logbook_Sent_Emails_Log_Insert(cmd, latest, ListToNotify[i]);
                            }
                        }
                    }
                }
                else
                {
                    response = CommonFunctions.JsonResponse(400, "Error! The record could not be created. Consult the System Administrator");
                    CommonFunctions.LogActivity("Failed to create a record because of an error", field["Staff"]);
                }
                return Content(response, "application/json");
            }
            catch (Exception ex)
            {
                // Exception handling
                string filePath = @"~/Exceptions_Log.txt";
                StringBuilder str = new StringBuilder();
                str.Append("Message :" + ex.Message + Environment.NewLine + "StackTrace :" + ex.StackTrace);
                str.Append("" + Environment.NewLine + "Date :" + DateTime.Now.ToString());
                str.Append("" + Environment.NewLine + "UserName :" + Session["username"].ToString());
                str.Append(Environment.NewLine + "-----------------------------------------------------------------------------" + Environment.NewLine + Environment.NewLine + Environment.NewLine);
                System.IO.File.AppendAllText(Server.MapPath(filePath), str.ToString());

                string response = CommonFunctions.JsonResponse(400, "Error! The record could not be created. Consult the System Administrator");
                return Content(response, "application/json");
            }
        }

        public ActionResult CreateHelpdesk(FormCollection field)
        {
            string response = "";
            //string ClosedBy = "";
            //string DateClosed = "";
            //if (field["Status"] == "2")
            //{
            //    string str = field["Staff"].Replace(".", " ");
            //    ClosedBy = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(str.ToLower());
            //    DateClosed = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");
            //}
            //string MainPurpose = Request.QueryString["isother"] == "true" ? field["OtherIssue"] : field["MainPurpose"];
            string newRecordSql = "select (select Concat('H',(max(cast(SUBSTRING(recordnumber,2,50) as int)   )+1)) from logbook_helpdesk)";
            string sql = string.Format(@"INSERT INTO [dbo].[logbook_helpdesk]
           ([RecordNumber],[Status],[CaseNumber],[PhoneNumber],[Gender],[Date],[Time],[Governorate]
           ,[Partner],[CBO],[Staff],[Subcategory],[Details],[AddressedOnSpot],[ReplacementRequired],[ResponsibleUnit])"
            + newRecordSql+ ",{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14}",
                    SqlValue(field["Status"], true),
                    SqlValue(field["CaseNumber"], true),
                    SqlValue(field["PhoneNumber"], true),
                    SqlValue(field["Gender"], true),
                    SqlValue(DateTime.Now.ToString("yyyy-MM-dd"), true),
                    SqlValue(DateTime.Now.ToString("hh:mm:ss"), true),
                    SqlValue(field["Governorate"], true),
                    SqlValue(field["Partner"], true),
                    SqlValue(field["CBO"], true),
                    SqlValue(field["Staff"], true),
                    SqlValue(field["Subcategory"], true),
                    SqlValue(field["Details"], true),
                    SqlValue(field["AddressedOnSpot"], true),
                    SqlValue(field["ReplacementRequired"], true),
                    SqlValue(field["ResponsibleUnit"], true)
             );
            int rows = CommonFunctions.RunCommand(sql);

            if (rows > 0)
            {
                string latest = CommonFunctions.GetValueFromDb("Select Max(RecordNumber) from logbook_helpdesk");
                response = CommonFunctions.JsonResponse(200, "The record was saved successfully with record number " + latest, field["Staff"]);
                CommonFunctions.LogActivity("Created record number: " + latest, field["Staff"]);
                //CommonFunctions.LogTicketChanges(latest, "Ticket Created", field["Staff"]);
            }
            else
            {
                response = CommonFunctions.JsonResponse(400, "Error! The record could not be created. Consult the System Administrator");
                CommonFunctions.LogActivity("Failed to create the record because of an error", field["Staff"]);
            }
            return Content(response, "application/json");
        }
        public int insertMonitoringNote(FormCollection field, string prefix, string Governorate, string District, string GPSCoordinatesLat, string GPSCoordinatesLong)
        {
            string newRecordSql;
            string sql;
           newRecordSql = "select Concat('" + prefix + "',(max(    cast(SUBSTRING(recordnumber,2,50) as int)   )+1)) from logbook_records where RecordType=" + field["RecordType"];
            sql = string.Format(@"INSERT INTO logbook_records (RecordNumber,RecordType,Status,SubProgramme,
                Staff,Date,Time,CopyProtectionAdvisor,CopyGenderFocalPoint,SubSource,PartnerName,Governorate,
                District,GPSCoordinateLat,GPSCoordinateLong,CaseNumber,PhoneNumber,Gender,SubCategory,Details,
                Severity,ShopID,ResponsibleUnit,ResponsiblePerson,Action,DateOfLastAction,ActionTakenBy,
                ClosedBy,DateClosed,ReportPath, OtherExternalSource, SourceFurtherInfo, Location2Gov, Location2Dis, Location3Gov, Location3Dis, Location4Gov, Location4Dis, Location5Gov, Location5Dis, Location6Gov, Location6Dis, Location7Gov, Location7Dis, Location8Gov, Location8Dis, Location9Gov, Location9Dis, Location10Gov, Location10Dis, MonitoringActivityID, MonitoringActivitySubID, FollowUpDate, HKMLocationType, SubProgrammeDetails, GPSCoordinateLat2, GPSCoordinateLong2,GPSCoordinateLat3, GPSCoordinateLong3,GPSCoordinateLat4, GPSCoordinateLong4,GPSCoordinateLat5, GPSCoordinateLong5,GPSCoordinateLat6, GPSCoordinateLong6, GPSCoordinateLat7, GPSCoordinateLong7,GPSCoordinateLat8, GPSCoordinateLong8,GPSCoordinateLat9, GPSCoordinateLong9,GPSCoordinateLat10, GPSCoordinateLong10, OtherFMOs, Title, ActivityType1,NumberofForms1, ActivityType2,NumberofForms2,ActivityType3,NumberofForms3,ActivityType4,NumberofForms4,ActivityType5,NumberofForms5,ActivityType6,NumberofForms6,ActivityType7,NumberofForms7, ActivityType8,NumberofForms8,ActivityType9,NumberofForms9,ActivityType10,NumberofForms10,MainChallenges,MainFindings,IssuestoFollowUp,ActionTakenbyFM) Select (" + newRecordSql + @"),
                {0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},
                {20},{21},{22},{23},{24},{25},{26},{27},{28}, {29},{30}, {31}, {32}, {33}, {34}, {35}, {36}, {37}, {38}, {39}, {40}, {41}, {42}, {43}, {44}, {45}, {46}, {47}, {48}, {49},{50}, {51}, {52}, {53},    {54}, {55}, {56}, {57}, {58}, {59}, {60}, {61}, {62}, {63}, {64}, {65}, {66}, {67}, {68}, {69}, {70}, {71}, {72}, {73},{74},{75},{76},{77},{78},{79},{80},{81},{82},{83},{84},{85},{86},{87},{88},{89},{90},{91},{92},{93},{94},{95},{96},{97} ",
               SqlValue(field["RecordType"], true),
               SqlValue(field["Status"], true),
               SqlValue(field["SubProgramme"], true),
               SqlValue(field["Staff"], true),
               SqlValue(DateTime.Now.ToString("yyyy-MM-dd"), true),
               SqlValue(DateTime.Now.ToString("hh:mm:ss"), true),
               SqlValue(field["CopyProtectionAdvisor"], true),
               SqlValue(field["CopyGenderFocalPoint"], true),
               SqlValue(field["SubSource"], true),
               SqlValue(field["PartnerName"], true),
               Governorate,
               District,
               GPSCoordinatesLat,
               GPSCoordinatesLong,
               SqlValue(field["CaseNumber"], true),
               SqlValue(field["PhoneNumber"], true),
               SqlValue(field["Gender"], true),
               SqlValue(field["SubCategory"], true),
               SqlValue(field["Details"], true),
               SqlValue(field["Severity"], true),
               SqlValue(field["ShopID"], true),
               SqlValue(field["ResponsibleUnit"], true),
               SqlValue(field["ResponsiblePerson"], true),
               SqlValue(field["Action"], true),
               SqlValue(field["DateOfLastAction"], true),
               SqlValue(field["ActionTakenBy"], true),

               SqlValue(field["ClosedBy"], true),
               SqlValue(field["DateClosed"], true),
               SqlValue(field["ReportPath"], true),
               SqlValue(field["txtOtherExternalSource"], true),
               SqlValue(field["txtSourceFurtherInfo"], true),

               SqlValue(field["Governorate2"], true),
               SqlValue(field["District2"], true),
               SqlValue(field["Governorate3"], true),
               SqlValue(field["District3"], true),
               SqlValue(field["Governorate4"], true),
               SqlValue(field["District4"], true),
               SqlValue(field["Governorate5"], true),
               SqlValue(field["District5"], true),
               SqlValue(field["Governorate6"], true),
               SqlValue(field["District6"], true),
               SqlValue(field["Governorate7"], true),
               SqlValue(field["District7"], true),
               SqlValue(field["Governorate8"], true),
               SqlValue(field["District8"], true),
               SqlValue(field["Governorate9"], true),
               SqlValue(field["District9"], true),
               SqlValue(field["Governorate10"], true),
               SqlValue(field["District10"], true),
               SqlValue(field["ddlMonitoringActivity"], true),
               SqlValue(field["ddlMonitoringActivitySub"], true),
               SqlValue(field["txtFollowUp"], true),
               SqlValue(field["HKMLocationType"], true),
               SqlValue(field["SubProgrammeDetails"], true),
               SqlValue(field["GPSCoordinatesLat2"], true),
               SqlValue(field["GPSCoordinatesLong2"], true),
               SqlValue(field["GPSCoordinatesLat3"], true),
               SqlValue(field["GPSCoordinatesLong3"], true),
               SqlValue(field["GPSCoordinatesLat4"], true),
               SqlValue(field["GPSCoordinatesLong4"], true),
               SqlValue(field["GPSCoordinatesLat5"], true),
               SqlValue(field["GPSCoordinatesLong5"], true),
               SqlValue(field["GPSCoordinatesLat6"], true),
               SqlValue(field["GPSCoordinatesLong6"], true),
               SqlValue(field["GPSCoordinatesLat7"], true),
               SqlValue(field["GPSCoordinatesLong7"], true),
               SqlValue(field["GPSCoordinatesLat8"], true),
               SqlValue(field["GPSCoordinatesLong8"], true),
               SqlValue(field["GPSCoordinatesLat9"], true),
               SqlValue(field["GPSCoordinatesLong9"], true),
               SqlValue(field["GPSCoordinatesLat10"], true),
               SqlValue(field["GPSCoordinatesLong10"], true),
               SqlValue(field["OtherFMOs"], true),
               SqlValue(field["Title"], true),
               SqlValue(field["subactivity"], true),
               SqlValue(field["numberofforms"], true),
               SqlValue(field["subactivity2"], true),
               SqlValue(field["numberofforms2"], true),
               SqlValue(field["subactivity3"], true),
               SqlValue(field["numberofforms3"], true),
               SqlValue(field["subactivity4"], true),
               SqlValue(field["numberofforms4"], true),
               SqlValue(field["subactivity5"], true),
               SqlValue(field["numberofforms5"], true),
               SqlValue(field["subactivity6"], true),
               SqlValue(field["numberofforms6"], true),
               SqlValue(field["subactivity7"], true),
               SqlValue(field["numberofforms7"], true),
               SqlValue(field["subactivity8"], true),
               SqlValue(field["numberofforms8"], true),
               SqlValue(field["subactivity9"], true),
               SqlValue(field["numberofforms9"], true),
               SqlValue(field["subactivity10"], true),
               SqlValue(field["numberofforms10"], true),
               SqlValue(field["mainchallenges"], true),
               SqlValue(field["MainFindings"], true),
               SqlValue(field["IssuesForFollowUp"], true),
               SqlValue(field["ActionTakenByFM"], true)
            );

            return CommonFunctions.RunCommand(sql);
        }
        public ActionResult EditHelpdesk(string id)
        {
            string SQL = string.Format(@"select [RecordNumber]
                      ,[Date]
                      ,[Time]
                      ,[Governorate]
                      ,[Partner]
                      ,[CBO]
                      ,[Staff]
                      ,[AddressedOnSpot]
                      ,[ResponsibleUnit]
                      ,[Status]
                      ,[CaseNumber]
                      ,[PhoneNumber]
                      ,[Gender]
                      ,[Category]
                      ,[Subcategory]
                      ,[Details]
                from logbook_helpdesk_view where RecordNumber='{0}'", id);
            string jsonString = CommonFunctions.GetJson(SQL);
            return Content(jsonString, "application/json");
        }
        public ActionResult Edit(string id)
        {
            try
            {
                string SQL = @"SELECT isnull(Details,'') Details, ISNULL(CopyProtectionAdvisor,0) CopyProtectionAdvisor, ISNULL(CopyGenderFocalPoint,0) CopyGenderFocalPoint, ISNULL(Programme,0) Programme, ISNULL(SubProgramme,0) SubProgramme, ISNULL(SourceID,0) SourceID, ISNULL(SubSource,0) SubSource, ISNULL(LocationTypeID,0) LocationTypeID,ISNULL(Governorate,0) Governorate,ISNULL(District,'') District,ISNULL(GPSCoordinateLat,0) GPSCoordinateLat,ISNULL(GPSCoordinateLong,0) GPSCoordinateLong,ISNULL(CategoryID,0) CategoryID,ISNULL(SubCategory,0) SubCategory,ISNULL(Details,'') DetailsResponsibleUnit,ISNULL(ResponsibleUnit, 0)  ResponsibleUnit,ISNULL(ResponsiblePerson,0) ResponsiblePerson,ISNULL(Severity,0) Severity,     ISNULL(ShopID, 0) ShopID, ISNULL(OtherExternalSource,'') OtherExternalSource, ISNULL(SourceFurtherInfo,'') SourceFurtherInfo, ISNULL(Status,0) Status,  ISNULL(Action,'') Action, ISNULL(RecordType, 0) RecordType, ISNULL(RecordNumber, '') RecordNumber, RecordID, isnull(Gov2,0) Gov2,isnull(Gov3,0) Gov3,isnull(Gov4,0) Gov4,isnull(Gov5,0) Gov5,isnull(Gov6,0) Gov6,isnull(Gov7,0) Gov7,isnull(Gov8,0) Gov8,isnull(Gov9,0) Gov9,isnull(Gov10,0) Gov10, isnull(Dis2,0) Dis2, isnull(Dis3,0) Dis3, isnull(Dis4,0) Dis4, isnull(Dis5,0) Dis5, isnull(Dis6,0) Dis6, isnull(Dis7,0) Dis7, isnull(Dis8,0) Dis8, isnull(Dis9,0) Dis9, isnull(Dis10,0) Dis10, isnull(LocType2,0) LocType2, isnull(LocType3,0) LocType3, isnull(LocType4,0) LocType4, isnull(LocType5,0) LocType5, isnull(LocType6,0) LocType6, isnull(LocType7,0) LocType7, isnull(LocType8,0) LocType8, isnull(LocType9,0) LocType9, isnull(LocType10,0) LocType10, isnull(Gov2ID,0) Gov2ID,isnull(Gov3ID,0) Gov3ID,isnull(Gov4ID,0) Gov4ID,isnull(Gov5ID,0) Gov5ID,isnull(Gov6ID,0) Gov6ID,isnull(Gov7ID,0) Gov7ID,isnull(Gov8ID,0) Gov8ID,isnull(Gov9ID,0) Gov9ID,isnull(Gov10ID,0) Gov10ID, isnull(Dis2ID,0) Dis2ID, isnull(Dis3ID,0) Dis3ID, isnull(Dis4ID,0) Dis4ID, isnull(Dis5ID,0) Dis5ID, isnull(Dis6ID,0) Dis6ID, isnull(Dis7ID,0) Dis7ID, isnull(Dis8ID,0) Dis8ID, isnull(Dis9ID,0) Dis9ID, isnull(Dis10ID,0) Dis10ID, isnull(MonitoringActivityID,0) MonitoringActivityID, isnull(MonitoringActivitySubID,0) MonitoringActivitySubID, isnull(MonitoringActivitySub,'') MonitoringActivitySub, isnull(MonitoringActivity,'') MonitoringActivity, isnull(CONVERT (varchar(10), FollowUpDate, 101) ,'') FollowUpDate, isnull(HKMLocationType,'') HKMLocationType, isnull(SubProgrammeDetails,'') SubProgrammeDetails, ISNULL(GPSCoordinateLat2,0) GPSCoordinateLat2,ISNULL(GPSCoordinateLong2,0) GPSCoordinateLong2, ISNULL(GPSCoordinateLat3,0) GPSCoordinateLat3,ISNULL(GPSCoordinateLong3,0) GPSCoordinateLong3,ISNULL(GPSCoordinateLat4,0) GPSCoordinateLat4,ISNULL(GPSCoordinateLong4,0) GPSCoordinateLong4,ISNULL(GPSCoordinateLat5,0) GPSCoordinateLat5,ISNULL(GPSCoordinateLong5,0) GPSCoordinateLong5,ISNULL(GPSCoordinateLat6,0) GPSCoordinateLat6,ISNULL(GPSCoordinateLong6,0) GPSCoordinateLong6,ISNULL(GPSCoordinateLat7,0) GPSCoordinateLat7,ISNULL(GPSCoordinateLong7,0) GPSCoordinateLong7,ISNULL(GPSCoordinateLat8,0) GPSCoordinateLat8,ISNULL(GPSCoordinateLong8,0) GPSCoordinateLong8,ISNULL(GPSCoordinateLat9,0) GPSCoordinateLat9,ISNULL(GPSCoordinateLong9,0) GPSCoordinateLong9,ISNULL(GPSCoordinateLat9,0) GPSCoordinateLat10,ISNULL(GPSCoordinateLong10,0) GPSCoordinateLong10,Staff,ISNULL(OtherFMOs,0) OtherFMOs, isnull(Title,'') Title, isnull(ActivityType1,'') ActivityType1, isnull(NumberofForms1,0) NumberofForms1, isnull(ActivityType2,'') ActivityType2, isnull(NumberofForms2,0) NumberofForms2, isnull(ActivityType3,'') ActivityType3, isnull(NumberofForms3,0) NumberofForms3, isnull(ActivityType4,'') ActivityType4, isnull(NumberofForms4,0) NumberofForms4, isnull(ActivityType5,'') ActivityType5, isnull(NumberofForms5,0) NumberofForms5, isnull(ActivityType6,'') ActivityType6, isnull(NumberofForms6,0) NumberofForms6, isnull(ActivityType7,'') ActivityType7, isnull(NumberofForms7,0) NumberofForms7, isnull(ActivityType8,'') ActivityType8, isnull(NumberofForms8,0) NumberofForms8, isnull(ActivityType9,'') ActivityType9, isnull(NumberofForms9,0) NumberofForms9, isnull(ActivityType10,'') ActivityType10, isnull(NumberofForms10,0) NumberofForms10, isnull(MainChallenges,'') MainChallenges, isnull(MainFindings,'') MainFindings, isnull(IssuestoFollowUp,'') IssuestoFollowUp, isnull(ActionTakenbyFM,'') ActionTakenbyFM  FROM logbook_records_view where RecordNumber='" + id + "'";
                string jsonString = CommonFunctions.GetJson(SQL);
                return Content(jsonString, "application/json");
            }
            catch (Exception ex)
            {
                // Exception handling
                string filePath = @"~/Exceptions_Log.txt";
                StringBuilder str = new StringBuilder();
                str.Append("Message :" + ex.Message + Environment.NewLine + "StackTrace :" + ex.StackTrace);
                str.Append("" + Environment.NewLine + "Date :" + DateTime.Now.ToString());
                str.Append("" + Environment.NewLine + "UserName :" + Session["username"].ToString());
                str.Append(Environment.NewLine + "-----------------------------------------------------------------------------" + Environment.NewLine + Environment.NewLine + Environment.NewLine);
                System.IO.File.AppendAllText(Server.MapPath(filePath), str.ToString());

                string response = CommonFunctions.JsonResponse(400, "Error! The record could not be created. Consult the System Administrator");
                return Content(response, "application/json");
            }
        }

        public ActionResult Update(FormCollection field)
        {
            try
            {


                bool emailRecordExists = false;
                string response = "";
                //string prevSQL = @"Select RecordNumber,RecordType,Status,SubProgramme,Staff,Date,Time,CopyProtectionAdvisor,CopyGenderFocalPoint,SubSource,PartnerName,Governorate,District,GPSCoordinateLat,GPSCoordinateLong,CaseNumber,PhoneNumber,Gender,SubCategory,Details, Severity,ShopID,ResponsibleUnit,ResponsiblePerson,Action,DateOfLastAction,ActionTakenBy,ClosedBy,DateClosed,ReportPath FROM logbook_records where RecordNumber = '" + field["hfRecordNo"] + "'";
                SqlCommand cmdPrev = new SqlCommand();
                cmdPrev.CommandText = "LogRecordLoad";
                cmdPrev.Parameters.AddWithValue("RecordNumber", field["hfRecordNo"].ToString());

                DataTable dtPrevious = CommonFunctions.GetDataFromDb_SP(cmdPrev);
                string PreviousStatus = dtPrevious.Rows[0]["StatusID"].ToString();
                string PreviousAction = dtPrevious.Rows[0]["Action Taken"].ToString();

                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "LogbookRecord_Update";
                cmd.Parameters.AddWithValue("Status", field["Status"]);
                cmd.Parameters.AddWithValue("SubProgramme", field["SubProgramme"]);
                cmd.Parameters.AddWithValue("CopyProtectionAdvisor", field["CopyProtectionAdvisor"]);
                cmd.Parameters.AddWithValue("CopyGenderFocalPoint", field["CopyGenderFocalPoint"]);
                cmd.Parameters.AddWithValue("SubSource", field["SubSource"]);
                cmd.Parameters.AddWithValue("PartnerName", field["PartnerName"]);
                cmd.Parameters.AddWithValue("Governorate", field["Governorate"]);
                cmd.Parameters.AddWithValue("District", field["District"]);
                cmd.Parameters.AddWithValue("GPSCoordinateLat", field["GPSCoordinatesLat"]);
                cmd.Parameters.AddWithValue("GPSCoordinateLong", field["GPSCoordinatesLong"]);

                cmd.Parameters.AddWithValue("GPSCoordinateLat2", field["GPSCoordinatesLat2"]);
                cmd.Parameters.AddWithValue("GPSCoordinateLong2", field["GPSCoordinatesLong2"]);
                cmd.Parameters.AddWithValue("GPSCoordinateLat3", field["GPSCoordinatesLat3"]);
                cmd.Parameters.AddWithValue("GPSCoordinateLong3", field["GPSCoordinatesLong3"]);
                cmd.Parameters.AddWithValue("GPSCoordinateLat4", field["GPSCoordinatesLat4"]);
                cmd.Parameters.AddWithValue("GPSCoordinateLong4", field["GPSCoordinatesLong4"]);
                cmd.Parameters.AddWithValue("GPSCoordinateLat5", field["GPSCoordinatesLat5"]);
                cmd.Parameters.AddWithValue("GPSCoordinateLong5", field["GPSCoordinatesLong5"]);
                cmd.Parameters.AddWithValue("GPSCoordinateLat6", field["GPSCoordinatesLat6"]);
                cmd.Parameters.AddWithValue("GPSCoordinateLong6", field["GPSCoordinatesLong6"]);
                cmd.Parameters.AddWithValue("GPSCoordinateLat7", field["GPSCoordinatesLat7"]);
                cmd.Parameters.AddWithValue("GPSCoordinateLong7", field["GPSCoordinatesLong7"]);
                cmd.Parameters.AddWithValue("GPSCoordinateLat8", field["GPSCoordinatesLat8"]);
                cmd.Parameters.AddWithValue("GPSCoordinateLong8", field["GPSCoordinatesLong8"]);
                cmd.Parameters.AddWithValue("GPSCoordinateLat9", field["GPSCoordinatesLat9"]);
                cmd.Parameters.AddWithValue("GPSCoordinateLong9", field["GPSCoordinatesLong9"]);
                cmd.Parameters.AddWithValue("GPSCoordinateLat10", field["GPSCoordinatesLat10"]);
                cmd.Parameters.AddWithValue("GPSCoordinateLong10", field["GPSCoordinatesLong10"]);
                cmd.Parameters.AddWithValue("OtherFMOs", field["OtherFMOs"]);

                cmd.Parameters.AddWithValue("CaseNumber", field["CaseNumber"]);
                cmd.Parameters.AddWithValue("PhoneNumber", field["PhoneNumber"]);
                cmd.Parameters.AddWithValue("Gender", field["Gender"]);
                cmd.Parameters.AddWithValue("SubCategory", field["SubCategory"]);
                cmd.Parameters.AddWithValue("Details", field["Details"]);
                cmd.Parameters.AddWithValue("Severity", field["Severity"]);
                cmd.Parameters.AddWithValue("ShopID", field["ShopID"]);
                cmd.Parameters.AddWithValue("ResponsibleUnit", field["ResponsibleUnit"]);
                cmd.Parameters.AddWithValue("ResponsiblePerson", field["ResponsiblePerson"]);
                cmd.Parameters.AddWithValue("Action", field["Action"]);
                cmd.Parameters.AddWithValue("DateOfLastAction", field["DateOfLastAction"]);
                cmd.Parameters.AddWithValue("ActionTakenBy", field["ActionTakenBy"]);
                cmd.Parameters.AddWithValue("ClosedBy", field["ClosedBy"]);
                cmd.Parameters.AddWithValue("DateClosed", field["DateClosed"]);
                cmd.Parameters.AddWithValue("ReportPath", field["ReportPath"]);
                cmd.Parameters.AddWithValue("SourceFurtherInfo", field["txtSourceFurtherInfo"]);
                cmd.Parameters.AddWithValue("OtherExternalSource", field["txtOtherExternalSource"]);
                cmd.Parameters.AddWithValue("RecordNumber", field["hfRecordNo"]);

                cmd.Parameters.AddWithValue("Location2Gov", field["Governorate2"]);
                cmd.Parameters.AddWithValue("Location2Dis", field["District2"]);
                cmd.Parameters.AddWithValue("Location3Gov", field["Governorate3"]);
                cmd.Parameters.AddWithValue("Location3Dis", field["District3"]);
                cmd.Parameters.AddWithValue("Location4Gov", field["Governorate4"]);
                cmd.Parameters.AddWithValue("Location4Dis", field["District4"]);
                cmd.Parameters.AddWithValue("Location5Gov", field["Governorate5"]);
                cmd.Parameters.AddWithValue("Location5Dis", field["District5"]);
                cmd.Parameters.AddWithValue("Location6Gov", field["Governorate6"]);
                cmd.Parameters.AddWithValue("Location6Dis", field["District6"]);
                cmd.Parameters.AddWithValue("Location7Gov", field["Governorate7"]);
                cmd.Parameters.AddWithValue("Location7Dis", field["District7"]);
                cmd.Parameters.AddWithValue("Location8Gov", field["Governorate8"]);
                cmd.Parameters.AddWithValue("Location8Dis", field["District8"]);
                cmd.Parameters.AddWithValue("Location9Gov", field["Governorate9"]);
                cmd.Parameters.AddWithValue("Location9Dis", field["District9"]);
                cmd.Parameters.AddWithValue("Location10Gov", field["Governorate10"]);
                cmd.Parameters.AddWithValue("Location10Dis", field["District10"]);
                cmd.Parameters.AddWithValue("MonitoringActivityID", field["ddlMonitoringActivity"]);
                cmd.Parameters.AddWithValue("FollowUpDate", field["txtFollowUp"]);
                cmd.Parameters.AddWithValue("Staff", field["Staff"]);
                cmd.Parameters.AddWithValue("HKMLocationType", field["HKMLocationType"]);
                cmd.Parameters.AddWithValue("SubProgrammeDetails", field["SubProgrammeDetails"]);

                cmd.Parameters.AddWithValue("Title", field["Title"]);
                cmd.Parameters.AddWithValue("ActivityType1", field["subactivity"]);
                cmd.Parameters.AddWithValue("NumberofForms1", field["numberofforms"]);
                cmd.Parameters.AddWithValue("ActivityType2", field["subactivity2"]);
                cmd.Parameters.AddWithValue("NumberofForms2", field["numberofforms2"]);
                cmd.Parameters.AddWithValue("ActivityType3", field["subactivity3"]);
                cmd.Parameters.AddWithValue("NumberofForms3", field["numberofforms3"]);
                cmd.Parameters.AddWithValue("ActivityType4", field["subactivity4"]);
                cmd.Parameters.AddWithValue("NumberofForms4", field["numberofforms4"]);
                cmd.Parameters.AddWithValue("ActivityType5", field["subactivity5"]);
                cmd.Parameters.AddWithValue("NumberofForms5", field["numberofforms5"]);
                cmd.Parameters.AddWithValue("ActivityType6", field["subactivity6"]);
                cmd.Parameters.AddWithValue("NumberofForms6", field["numberofforms6"]);
                cmd.Parameters.AddWithValue("ActivityType7", field["subactivity7"]);
                cmd.Parameters.AddWithValue("NumberofForms7", field["numberofforms7"]);
                cmd.Parameters.AddWithValue("ActivityType8", field["subactivity8"]);
                cmd.Parameters.AddWithValue("NumberofForms8", field["numberofforms8"]);
                cmd.Parameters.AddWithValue("ActivityType9", field["subactivity9"]);
                cmd.Parameters.AddWithValue("NumberofForms9", field["numberofforms9"]);
                cmd.Parameters.AddWithValue("ActivityType10", field["subactivity10"]);
                cmd.Parameters.AddWithValue("NumberofForms10", field["numberofforms10"]);
                cmd.Parameters.AddWithValue("MainChallenges", field["mainchallenges"]);
                cmd.Parameters.AddWithValue("MainFindings", field["MainFindings"]);
                cmd.Parameters.AddWithValue("IssuestoFollowUp", field["IssuesForFollowUp"]);
                cmd.Parameters.AddWithValue("ActionTakenbyFM", field["ActionTakenByFM"]);


                
                    

                if (field["ddlMonitoringActivitySub"] != "")
                {
                    cmd.Parameters.AddWithValue("MonitoringActivitySubID", field["ddlMonitoringActivitySub"]);
                }



                int rows = CommonFunctions.RunSPCommand(cmd);

                SqlCommand cmdCur = new SqlCommand();
                cmdCur.CommandText = "LogRecordLoad";
                cmdCur.Parameters.AddWithValue("RecordNumber", field["hfRecordNo"].ToString());
                DataTable dtCur = CommonFunctions.GetDataFromDb_SP(cmdCur);



                if (rows > 0)
                {


                    //check if there are files uploaded and add their info to DB
                    string username = Session["username"].ToString();
                    SqlCommand cmd1 = new SqlCommand();
                    string[] files = System.IO.Directory.GetFiles(Server.MapPath("~/Attatchments"), string.Format("*{0}*", username), System.IO.SearchOption.TopDirectoryOnly);
                    if (files.Length > 0)
                    {
                        //delete old ones 
                        cmd1.CommandText = "DocumentsDelete";
                        cmd1.Parameters.AddWithValue("RecordNumber", field["hfRecordNo"].ToString());
                        CommonFunctions.RunSPCommand(cmd1);


                        FileInfo f;
                        string[] files1 = System.IO.Directory.GetFiles(Server.MapPath(string.Format("~/Attatchments/{0}", username)), string.Format("*{0}*", username), System.IO.SearchOption.TopDirectoryOnly);
                        if (files1.Length > 0)
                        {
                            foreach (string s in files1)
                            {
                                f = new FileInfo(s);
                                f.Delete();
                            }

                        }

                        Directory.CreateDirectory(Server.MapPath(string.Format("~/Attatchments/{0}", username)));
                        foreach (string s in files)
                        {
                            cmd1.Parameters.Clear();
                            cmd1.CommandText = "DocumentAdd";
                            cmd1.Parameters.AddWithValue("Path", username + "/" + Path.GetFileName(s));
                            cmd1.Parameters.AddWithValue("RecordNumber", field["hfRecordNo"].ToString());
                            CommonFunctions.RunSPCommand(cmd1);

                            FileInfo f1 = new FileInfo(s);
                            f1.MoveTo(string.Format(Server.MapPath("~/Attatchments/{0}/" + Path.GetFileName(s)), username));

                        }
                    }



                    response = CommonFunctions.JsonResponse(200, "The record was saved successfull", field["Staff"]);
                    CommonFunctions.LogActivity("Successfully edited record no: " + field["hfRecordNo"], field["Staff"]);
                    CommonFunctions.LogRecordChanges(field["hfRecordNo"], GetChanges(dtPrevious, dtCur), field["Staff"]);
                    string From = field["Staff"] + "@wfp.org";
                    string To = "";


                    if (field["SendEmail"] == "on" && field["ResponsiblePerson"] != null && field["ResponsiblePerson"].Length > 0 && field["Status"] != "2")
                    {
                        To = CommonFunctions.GetStaffEmail(field["ResponsiblePerson"]);
                        StringBuilder SBrecordURL = new StringBuilder();
                        SBrecordURL.Append("https://cbttriangulation.jor.wfp.org/LogBook/logbook?grid-filter=RecordNumber__1__");
                        SBrecordURL.Append(field["hfRecordNo"].ToString());
                        string RecordUrl = SBrecordURL.ToString();

                        string Message = "Dear " + CommonFunctions.GetFirstNameFromEmail(To) + ",<br /><br />" +
                            "This is to inform you that your unit was assigned a ticket by the logbook system. See the details below:<br /><br />"
                            + CommonFunctions.RecordView(field["hfRecordNo"].ToString())
                            + "<br /> Click " + string.Format("<a href='{0}'>Here", RecordUrl) + "</a>" + " to view the ticket<br />"
                            + "<br /> Thanks, <br />" + CommonFunctions.GetFirstNameFromEmail(From);

                        emailRecordExists = Logbook_Sent_Emails_Log_Check(cmd, field["hfRecordNo"].ToString(), To);
                        if (!(emailRecordExists))
                        {
                            CommonFunctions.SendEmail(From, To, "Log Book Ticket#: " + field["hfRecordNo"].ToString(), Message, "");
                            Logbook_Sent_Emails_Log_Insert(cmd, field["hfRecordNo"].ToString(), To);
                        }


                    }


                    string Staff = CommonFunctions.GetValueFromDb("Select Staff from logbook_records where RecordNumber='" + field["hfRecordNo"] + "'");
                    if (field["ResponsiblePerson"] != null)
                    {
                        To = Staff + "@wfp.org";
                        From = CommonFunctions.GetStaffEmail(field["ResponsiblePerson"]);
                        string ResponsiblePersonName = CommonFunctions.GetStaffName(field["ResponsiblePerson"]);


                        //If record closed, notify the creator of the record
                        if (field["Status"] != PreviousStatus && field["Status"] == "2")
                        {

                            From = Session["Username"].ToString() + "@wfp.org";
                            To = Staff + "@wfp.org";
                            string RecordUrl = "https://cbttriangulation.jor.wfp.org/LogBook/logbook?grid-filter=RecordNumber__1__" + field["hfRecordNo"].ToString();
                            //if (To.Length > 5 )
                            //{
                            string Message = "Dear " + CommonFunctions.GetFirstNameFromEmail(To) + ",<br /><br />" +
                                string.Format("This is to inform you that the ticket with the below details was closed by {0}:<br /><br />", Session["Username"].ToString())
                                + CommonFunctions.RecordView(field["hfRecordNo"].ToString(), ref To)
                                + "<br /> Click " + string.Format("<a href='{0}'>Here", RecordUrl) + "</a>" + " to view the ticket<br />"
                                + "<br /> Thanks, <br />" + CommonFunctions.GetFirstNameFromEmail(From);

                            CommonFunctions.SendEmail(From, To, "Log Book Ticket#: " + field["hfRecordNo"].ToString(), Message, "", "");




                            // notify HoP is severity is high
                            if (field["Severity"] == "3")
                            {

                                To = CommonFunctions.GetValueFromDb("SELECT email FROM logbook_responsiblePerson_vw WHERE unit = 'Programme – HoP'");
                                Message = "Dear " + CommonFunctions.GetFirstNameFromEmail(To) + ",<br /><br />" +
                                string.Format("This is to inform you that the ticket with the below details was closed by {0}:<br /><br />", Session["Username"].ToString())
                                + CommonFunctions.RecordView(field["hfRecordNo"].ToString(), ref To)
                                + "<br /> Click " + string.Format("<a href='{0}'>Here", RecordUrl) + "</a>" + " to view the ticket<br />"
                                + "<br /> Thanks, <br />" + CommonFunctions.GetFirstNameFromEmail(From);


                                CommonFunctions.SendEmail(From, To, "Log Book Ticket#: " + field["hfRecordNo"].ToString(), Message, "");


                            }


                        }


                    }


                }
                else
                {
                    response = CommonFunctions.JsonResponse(400, "Error! The record could not be updated. Consult the System Administrator");
                    CommonFunctions.LogActivity("Failed to update a record because of an error", field["Staff"]);
                }
                return Content(response, "application/json");
            }
            catch (Exception ex)
            {
                // Exception handling
                string filePath = @"~/Exceptions_Log.txt";
                StringBuilder str = new StringBuilder();
                str.Append("Message :" + ex.Message + Environment.NewLine + "StackTrace :" + ex.StackTrace);
                str.Append("" + Environment.NewLine + "Date :" + DateTime.Now.ToString());
                str.Append("" + Environment.NewLine + "UserName :" + Session["username"].ToString());
                str.Append(Environment.NewLine + "-----------------------------------------------------------------------------" + Environment.NewLine + Environment.NewLine + Environment.NewLine);
                System.IO.File.AppendAllText(Server.MapPath(filePath), str.ToString());

                string response = CommonFunctions.JsonResponse(400, "Error! The record could not be created. Consult the System Administrator");
                return Content(response, "application/json");
            }
        }

        public ActionResult SaveFieldsIntoSession(FormCollection fields)
        {
            try
            {
                Session.Add("Programme",fields["Programme"]);
                Session.Add("SubProgramme", fields["SubProgramme"]);
               string response = CommonFunctions.JsonResponse(200, "The record was saved successfull");
                return Content(response, "application/json");
            }
            catch (Exception ex)
            {
                // Exception handling
                string filePath = @"~/Exceptions_Log.txt";
                StringBuilder str = new StringBuilder();
                str.Append("Message :" + ex.Message + Environment.NewLine + "StackTrace :" + ex.StackTrace);
                str.Append("" + Environment.NewLine + "Date :" + DateTime.Now.ToString());
                str.Append("" + Environment.NewLine + "UserName :" + Session["username"].ToString());
                str.Append(Environment.NewLine + "-----------------------------------------------------------------------------" + Environment.NewLine + Environment.NewLine + Environment.NewLine);
                System.IO.File.AppendAllText(Server.MapPath(filePath), str.ToString());

                string response = CommonFunctions.JsonResponse(400, "Error! The record could not be created. Consult the System Administrator");
                return Content(response, "application/json");
            }
        }



        private string SqlValue(string val, bool quote)
        {
            string tmp = "";
            if (quote)
                tmp = SqlStr(val);
            else
                tmp = val;
            if (tmp == "")
                tmp = "NULL";
            else if (tmp == "null")
                tmp = "NULL";
            else if (quote)
                tmp = "'"+tmp+"'";
            return tmp;
        }
        private object SqlValue1(string val, bool quote)
        {
            object tmp = "";
            if (quote)
                tmp = SqlStr(val);
            else
                tmp = val;
            if (tmp == "")
                tmp = "NULL";
            else if (tmp == "null")
                tmp = "NULL";
            else if (quote)
                tmp = "'" + tmp + "'";

            if (tmp == "NULL")
            {
                tmp = SqlValue("NULL", true);
            }
            return tmp;
        }
        private string SqlStr(string val)
        {
            if (val != null)
            {
                return val.Replace("'", "''");
            }
            else
            {
                return "";
            }
        }
        private string MandatoryTo(string ReasonId)
        {
            string EmailTo = CommonFunctions.GetEmailAddresses(@"SELECT t2.email FROM dbo.splitstring((select EmailTo 
                                                            from logbook_subcategories where id = " + ReasonId + @" and TimeFrame=3)) as t1
                                                            inner join staff as t2 on t1.Name = t2.ID");
            return EmailTo.Replace("email\r\n", "").Trim().Replace(@"\r\n", ";");
        }
        private string MandatoryCC(string ReasonId)
        {
            string EmailCC = CommonFunctions.GetEmailAddresses(@"SELECT t2.email FROM dbo.splitstring((select EmailCC 
                                                            from logbook_subcategories where id = " + ReasonId + @" and TimeFrame=3)) as t1
                                                            inner join staff as t2 on t1.Name = t2.ID");
            return EmailCC.Replace("email\r\n", "").Trim().Replace(@"\r\n", ";");
        }
        private string GetChanges(DataTable previous, DataTable changes)
        {
            StringBuilder retBuilder = new StringBuilder();
            DateTime dt;
            int i = 0;
            
            foreach (DataColumn col in changes.Columns)
            {
                string colName = col.ColumnName;
                string prevVal = previous.Rows[0][colName].ToString();
                string curVal = changes.Rows[0][colName].ToString();

                if (prevVal == "01/01/1900 00:00:00" || string.IsNullOrEmpty(prevVal))
                {
                    prevVal = "None";
                }
                if (curVal == "01/01/1900 00:00:00" || string.IsNullOrEmpty(curVal))
                {
                    curVal = "None";
                }

                if (prevVal.Trim() != curVal.Trim())
                {
                    i++;
                    string lf = (i > 1) ? "<br />" : "";
                    if (string.IsNullOrEmpty(prevVal))
                    {
                        prevVal = "None";
                    }
                    if (string.IsNullOrEmpty(curVal))
                    {
                        curVal = "None";
                    }

                   if (DateTime.TryParse(prevVal, out dt) )
                    {
                        prevVal = System.Convert.ToDateTime(prevVal).ToString("dd/MM/yyyy");
                    }
                    if (DateTime.TryParse(curVal, out dt))
                    {
                        curVal = System.Convert.ToDateTime(curVal).ToString("dd/MM/yyyy");
                    }

                    retBuilder.Append(lf + "Changed <em><b>" + colName + "</b></em> from <em style='font-size:16px;'>" + prevVal + "</em> to " + "<em style='font-size:16px;'>" + curVal + "</em>");
                }
               
            }
            return retBuilder.ToString();
        }

        public JsonResult Upload()
        {
            try
            {

                //delete previous files / if exists 
                string username = Session["Username"].ToString();
                FileInfo f;
                string[] files = System.IO.Directory.GetFiles(Server.MapPath("~/Attatchments"), string.Format("*{0}*", username), System.IO.SearchOption.TopDirectoryOnly);
                if (files.Length > 0)
                {
                    foreach (string s in files)
                    {
                        f = new FileInfo(s);
                        f.Delete();
                    }
                    
                }
                
                string alertMessage = "Success: " + Request.Files.Count + " file/s Uploaded" + ", please close the upload window.";

                //validations 

                if (Request.Files.Count == 0)
                {
                    alertMessage = "Error: please select files to upload";
                }

                else if (Request.Files.Count > 3)
                {
                    alertMessage = "Error: the maximum number of files allowed is 3";
                }
                else
                {
                    for (int i = 0; i < Request.Files.Count; i++)
                    {
                        HttpPostedFileBase file = Request.Files[i]; //Uploaded file
                                                                    //Use the following properties to get file's name, size and MIMEType
                        int fileSize = file.ContentLength;
                        string fileName = file.FileName;
                        string mimeType = file.ContentType;
                        string fileExtension = Path.GetExtension(fileName);
                        System.IO.Stream fileContent = file.InputStream;

                        //validations 
                        if (!(fileName.ToUpper().Contains("PDF") || fileName.ToUpper().Contains("DOC") || fileName.ToUpper().Contains("DOCX") || fileName.ToUpper().Contains("XLS") || fileName.ToUpper().Contains("XLSX") || fileName.ToUpper().Contains("JPG") || fileName.ToUpper().Contains("PNG")))
                        {
                            alertMessage = "Error: allowed files are only: pdf, doc, docx, xls, xlsx, jpg, or png.";
                        }
                        else if (fileSize > 3145728)
                        {
                            alertMessage = "Error: maximum file size is 3 MB";
                        }                        

                        else
                        {
                            //To save file, use SaveAs method
                            file.SaveAs(Server.MapPath("~/Attatchments/") + username + '_' + DateTime.Now.ToString("yyyyMMddHHmmssfff") +  fileExtension); //File will be saved in application root

                        }

                    }

                }
                return Json(alertMessage);
            }
            catch (HttpException ex)
            {
                return Json("Error: "+ex.Message);
            }
            
        }
        public void Logbook_Sent_Emails_Log_Insert(SqlCommand cmd, string latest, string EmailSentTo) 
        {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Clear();
                cmd.CommandText = "Logbook_Sent_Emails_Log_Insert";
                cmd.Parameters.AddWithValue("RecordNumber", latest);
                cmd.Parameters.AddWithValue("EmailSentTo", EmailSentTo);
                CommonFunctions.RunSPCommand(cmd);
        }

        public bool Logbook_Sent_Emails_Log_Check(SqlCommand cmd, string latest, string EmailSentTo)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["LogBookDb"].ToString()))
            {
                con.Open();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                bool emailRecordExists = false;
                cmd.Parameters.Clear();
                cmd.CommandText = "Logbook_Sent_Emails_Log_Check";
                cmd.Parameters.AddWithValue("RecordNumber", latest);
                cmd.Parameters.AddWithValue("EmailSentTo", EmailSentTo);
                emailRecordExists = Convert.ToBoolean(cmd.ExecuteScalar());               
                return emailRecordExists;
            }
            
        }
        public ActionResult UpdateHelpdesk(FormCollection field)
        {
            string response = "";
            string sql = string.Format(@"UPDATE [dbo].[logbook_helpdesk]
                           SET[Status] = {1}
                              ,[CaseNumber] = {2}
                              ,[PhoneNumber] = {3}
                              ,[Gender] = {4}
                              ,[Governorate] = {5}
                              ,[Partner] = {6}
                              ,[CBO] = {7}
                              ,[Subcategory] = {8}
                              ,[Details] = {9}
                              ,[AddressedOnSpot] = {10}
                              ,[ReplacementRequired] = {11}
                              ,[ResponsibleUnit] = {12}
                         WHERE[RecordNumber]={0}",
                SqlValue(field["RecordNumber"], true),
                SqlValue(field["Status"], true),
                SqlValue(field["CaseNumber"], true),
                SqlValue(field["PhoneNumber"], true),
                SqlValue(field["Gender"], true),
                SqlValue(field["Governorate"], true),
                SqlValue(field["Partner"], true),
                SqlValue(field["CBO"], true),
                SqlValue(field["Subcategory"], true),
                SqlValue(field["Details"], true),
                SqlValue(field["AddressedOnSpot"], true),
                SqlValue(field["ReplacementRequired"], true),
                SqlValue(field["ResponsibleUnit"], true)
             );
            int rows = CommonFunctions.RunCommand(sql);

            if (rows > 0)
            {
                response = CommonFunctions.JsonResponse(200, "The record was saved successfull", field["Staff"]);
                CommonFunctions.LogActivity("Successfully edited helpdesk record no: " + field["TicketNumber"], field["Staff"]);
                if (field["ReplacementRequired"] == "1")
                {
                    //string ReplacementSQL = "Insert into Card";
                }
            }
            else
            {
                response = CommonFunctions.JsonResponse(400, "Error! The record could not be updated. Consult the System Administrator");
                CommonFunctions.LogActivity("Failed to update the record because of an error", field["Staff"]);
            }
            return Content(response, "application/json");
        }

    }

}
