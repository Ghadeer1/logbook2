﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HotlineMVC.Models
{
    public class HelpdeskModel
    {
        public string RecordNumber { get; set; }
        public DateTime Date { get; set; }
        public string Time { get; set; }
        public string Status { get; set; }
        public string CaseNumber { get; set; }
        public int PhoneNumber { get; set; }
        public string Gender { get; set; }
        public string Governorate { get; set; }
        public string Partner { get; set; }
        public string CBO { get; set; }
        public string Staff { get; set; }
        public string Category { get; set; }
        public string Subcategory { get; set; }
        public string Details { get; set; }
        public string AddressedOnSpot { get; set; }
        public string ReplacementRequired { get; set; }
        public string ResponsibleUnit { get; set; }
        public string GovernorateDesc { get; set; }
        public string PartnerDesc { get; set; }
        public string CBODesc { get; set; }
        public string AddressedOnSpotDesc { get; set; }
        public string ReplacementRequiredDesc { get; set; }
        public string ResponsibleUnitDesc { get; set; }
        public string StatusDesc { get; set; }
        public string CategoryDesc { get; set; }
        public string SubcategoryDesc { get; set; }
        public string GenderDesc { get; set; }
    }
}