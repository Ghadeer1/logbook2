﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HotlineMVC.Controllers
{
    public class ReplacementController : Controller
    {
        // GET: Replacement
        public ActionResult Index()
        {
            ViewBag.PendingRequests = CommonFunctions.GetValueFromDb("Select Count(*) from logbook_card_replacement where status=1");
            ViewBag.AllIssues = CommonFunctions.GetValueFromDb("Select Count(*) from logbook_card_replacement");
            ViewBag.AverageAge = CommonFunctions.GetValueFromDb("Select Avg(Age) from logbook_card_replacement_view");
            ViewBag.ServiceLevel = 80;
            return View();
        }
    }
}