﻿jQuery(document).ready(function () {
    $("#calendar").fullCalendar(
        {
            events: calendarEvents,
            displayEventTime: false,
            height: 250
        });
});