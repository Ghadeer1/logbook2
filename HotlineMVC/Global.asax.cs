﻿using HotlineMVC.Controllers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace HotlineMVC
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);


            //Send monitoring notes emails

            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "LoadFollowUpMonitoringNotes";
           DataTable dt =  CommonFunctions.GetDataFromDb_SP(cmd);
            
            string from = "";
            string To = "";
            string subject = "Logbook Monitoring Note {0} Reminder";
            StringBuilder msg = new StringBuilder();
            StringBuilder view = new StringBuilder();
            string RecordNumber = "";
            string DateCreated = "";
            string TimeCreated = "";
            string RecordUrl = "https://cbttriangulation.jor.wfp.org/LogBook/logbook?grid-filter=RecordNumber__1__";
            string by = "";
            string program;
            string subprogram;
            string monitoringactivity;
            string notes;

            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                                       
                     msg = new StringBuilder();
                     view = new StringBuilder();   

                    by = dr["staff"].ToString();
                    from = by + "@wfp.org";
                    To = by + "@wfp.org";
                    RecordNumber = dr["RecordNumber"].ToString();
                    DateCreated = Convert.ToDateTime(dr["DateCreated"]).ToString("dd-MM-yyyy");
                    TimeCreated = dr["TimeCreated"].ToString();
                    program = dr["program"].ToString();
                    subprogram = dr["subprogram"].ToString();
                    monitoringactivity = dr["monitoringactivity"].ToString();
                    notes = dr["notes"].ToString();

                    msg.Append("Dear " + CommonFunctions.GetFirstNameFromEmail(To) + ",<br /><br />");
                    msg.Append(string.Format("This is a remider about the monitoring note number {0} which was assigned to you on {1} at {2} as per the below details:<br /><br />", RecordNumber, DateCreated, TimeCreated));
                    msg.Append(messageView(RecordNumber, by, DateCreated, TimeCreated, program, subprogram, monitoringactivity, notes));
                    msg.Append("<br /> Click " + string.Format("<a href='{0}'>here</a>", RecordUrl + RecordNumber) + " to update the ticket<br />");
                    msg.Append("<br /> Thanks, <br />" + CommonFunctions.GetFirstNameFromEmail(from));
                    CommonFunctions.SendEmail(from, To, string.Format(subject, RecordNumber), msg.ToString());

                    //Mark as email sent 
                    cmd.Parameters.Clear();
                    cmd.CommandText = "UpdateFollowUpMonitoringNotesEmailSentFlag";
                    cmd.Parameters.AddWithValue("RecordNumber", RecordNumber);
                    CommonFunctions.RunSPCommand(cmd);

                }

                
            }

            //Send still open tickets emails  -- tickets escalation
            string category;
            string subcategory;
            string details;
            string severity= "";
            string responsibleunit = "";
            int daysOpen = 0;
            cmd.Parameters.Clear();
            cmd.CommandText = "LateRecodedTicketsLoad";
            dt = new DataTable();
            dt = CommonFunctions.GetDataFromDb_SP(cmd);

            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {

                    msg = new StringBuilder();
                    view = new StringBuilder();

                    severity = dr["severity"].ToString();
                    daysOpen = Convert.ToInt16(dr["days_Late"]);
                    by = dr["staff"].ToString();
                    from = by + "@wfp.org";
                    To = dr["EscalatedToStaffEmail"].ToString();
                    RecordNumber = dr["RecordNumber"].ToString();
                    DateCreated = Convert.ToDateTime(dr["DateCreated"]).ToString("dd-MM-yyyy");
                    TimeCreated = dr["TimeCreated"].ToString();
                    program = dr["program"].ToString();
                    subprogram = dr["subprogram"].ToString();
                    category = dr["category"].ToString();
                    subcategory = dr["subcategory"].ToString();                    
                    details = dr["details"].ToString();
                    responsibleunit = dr["ResposibleUnit"].ToString();

                    msg.Append("Dear " + CommonFunctions.GetFirstNameFromEmail(To) + ",<br /><br />");
                    msg.Append(string.Format("This is to inform you about ticket number {0} which was created on {1} at {2} and has been open for {3} days and no one has taken any further action regarding it, below are the details:<br /><br />", RecordNumber, DateCreated, TimeCreated, daysOpen.ToString()));
                    msg.Append(messageView1(RecordNumber, by, DateCreated, TimeCreated,severity, program, subprogram, category, subcategory, details, responsibleunit));
                    msg.Append("<br /> Click " + string.Format("<a href='{0}'>here</a>", RecordUrl + RecordNumber) + " to update the ticket<br />");
                    msg.Append("<br /> Thanks, <br />" + CommonFunctions.GetFirstNameFromEmail(from));
                    //CommonFunctions.SendEmail(from, To, string.Format(subject, RecordNumber), msg.ToString());


                }
            }

            //Daily Report Email



        }
        protected void Application_Error(object sender, EventArgs e)
        {
            var httpContext = ((MvcApplication)sender).Context;
            var currentController = " ";
            var currentAction = " ";
            var currentRouteData = RouteTable.Routes.GetRouteData(new HttpContextWrapper(httpContext));

            if (currentRouteData != null)
            {
                if (currentRouteData.Values["controller"] != null && !String.IsNullOrEmpty(currentRouteData.Values["controller"].ToString()))
                {
                    currentController = currentRouteData.Values["controller"].ToString();
                }

                if (currentRouteData.Values["action"] != null && !String.IsNullOrEmpty(currentRouteData.Values["action"].ToString()))
                {
                    currentAction = currentRouteData.Values["action"].ToString();
                }
            }

            var ex = Server.GetLastError();
            //var controller = new ErrorController();
            var routeData = new RouteData();
            var action = "GenericError";

            if (ex is HttpException)
            {
                var httpEx = ex as HttpException;

                switch (httpEx.GetHttpCode())
                {
                    case 404:
                        action = "NotFound";
                        break;

                        // others if any
                }
            }

            httpContext.ClearError();
            httpContext.Response.Clear();
            httpContext.Response.StatusCode = ex is HttpException ? ((HttpException)ex).GetHttpCode() : 500;
            httpContext.Response.TrySkipIisCustomErrors = true;

            routeData.Values["controller"] = "Error";
            routeData.Values["action"] = action;
            routeData.Values["exception"] = new HandleErrorInfo(ex, currentController, currentAction);

            IController errormanagerController = new ErrorController();
            HttpContextWrapper wrapper = new HttpContextWrapper(httpContext);
            var rc = new RequestContext(wrapper, routeData);
            errormanagerController.Execute(rc);
        }


        protected string messageView(string RecordNumber, string By, string date, string time, string program, string subprogram, string monitoringactivity, string notes )
        {
            return string.Format(@"<table border=0>
                <tr>
                    <td style=vertical-align:top>

                            <table style=width:400px border=1 style=border-collapse:collapse>
                                <tr>
                                    <td align=center style='color:white;background-color:#2A93FC;font-weight:bold' colspan=2>Monitoring Note Info</td>
                                </tr>
                                <tr>
                                    <td>Record Number:</td>
                                    <td>{0}</td>
                                </tr>
                                <tr>
                                    <td>Recorded by:</td>
                                    <td>{1}</td>
                                </tr>
                                <tr>
                                    <td>Date:</td>
                                    <td>{2}</td>
                                </tr>
                                <tr>
                                    <td>Time:</td>
                                    <td>{3}</td>
                                </tr>
                                <tr>
                                    <td>Program:</td>
                                    <td>{4}</td>
                                </tr>
                                <tr>
                                    <td>Sub Program:</td>
                                    <td>{5}</td>
                                </tr>
                                <tr>
                                    <td>Monitoring Activity:</td>
                                    <td>{6}</td>
                                </tr>
                               
                                <tr>
                                    <td>Notes:</td>
                                    <td>{7}</td>
                                </tr>

                               
                            </table>
                    </td>
                    
                            </table>
                    </td>
                </tr>
            </table>", RecordNumber, By, date, time, program, subprogram, monitoringactivity, notes);
        }

        protected string messageView1(string RecordNumber, string By, string date, string time,string severity, string program, string subprogram, string category,string subcategory, string details, string ResponsibleUnit)
        {
            return string.Format(@"<table border=0>
                <tr>
                    <td style=vertical-align:top>

                            <table style=width:400px border=1 style=border-collapse:collapse>
                                <tr>
                                    <td align=center style='color:white;background-color:#2A93FC;font-weight:bold' colspan=2>Ticket Info</td>
                                </tr>
                                <tr>
                                    <td>Record Number:</td>
                                    <td>{0}</td>
                                </tr>
                                <tr>
                                    <td>Recorded by:</td>
                                    <td>{1}</td>
                                </tr>
                                <tr>
                                    <td>Responsible Unit:</td>
                                    <td>{10}</td>
                                </tr>
                                <tr>
                                    <td>Date:</td>
                                    <td>{2}</td>
                                </tr>
                                <tr>
                                    <td>Time:</td>
                                    <td>{3}</td>
                                </tr>
                                <tr>
                                    <td>Severity:</td>
                                    <td>{4}</td>
                                </tr>
                                <tr>
                                    <td>Program:</td>
                                    <td>{5}</td>
                                </tr>
                                <tr>
                                    <td>Sub Program:</td>
                                    <td>{6}</td>
                                </tr>
                                <tr>
                                    <td>Category:</td>
                                    <td>{7}</td>
                                </tr>
                                <tr>
                                    <td>Sub Categotry:</td>
                                    <td>{8}</td>
                                </tr>
                                <tr>
                                    <td>Details:</td>
                                    <td>{9}</td>
                                </tr>

                               
                            </table>
                    </td>
                    
                            </table>
                    </td>
                </tr>
            </table>", RecordNumber, By, date, time,severity, program, subprogram, category, subcategory, details, ResponsibleUnit);
        }


    


    }
}
