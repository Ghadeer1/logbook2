﻿/***
* This script demonstrates how you can build you own custom filter widgets:
* 1. Specify widget type for column:
*       columns.Add(o => o.Customers.CompanyName)
*           .SetFilterWidgetType("CustomCompanyNameFilterWidget")
* 2. Register script with custom widget on the page:
*       <script src="@Url.Content("~/Scripts/gridmvc.customwidgets.js")" type="text/javascript"> </script>
* 3. Register your own widget in Grid.Mvc:
*       GridMvc.addFilterWidget(new CustomersFilterWidget());
*
* For more documentation see: http://gridmvc.codeplex.com/documentation
*/

/***
* CustomersFilterWidget - Provides filter user interface for customer name column in this project
* This widget onRenders select list with avaliable customers.
*/

function CustomersFilterWidget() {
    /***
    * This method must return type of registered widget type in 'SetFilterWidgetType' method
    */
    this.getAssociatedTypes = function () {
        return ["CustomCompanyNameFilterWidget"];
    };
    /***
    * This method invokes when filter widget was shown on the page
    */
    this.onShow = function () {
        /* Place your on show logic here */
    };

    this.showClearFilterButton = function () {
        return true;
    };
    /***
    * This method will invoke when user was clicked on filter button.
    * container - html element, which must contain widget layout;
    * lang - current language settings;
    * typeName - current column type (if widget assign to multipile types, see: getAssociatedTypes);
    * values - current filter values. Array of objects [{filterValue: '', filterType:'1'}];
    * cb - callback function that must invoked when user want to filter this column. Widget must pass filter type and filter value.
    * data - widget data passed from the server
    */
    this.onRender = function (container, lang, typeName, values, cb, data) {
        //store parameters:
        this.cb = cb;
        this.container = container;
        this.lang = lang;

        //this filterwidget demo supports only 1 filter value for column column
        this.value = values.length > 0 ? values[0] : { filterType: 1, filterValue: "" };

        this.renderWidget(); //onRender filter widget
        this.loadCustomers(); //load customer's list from the server
        this.registerEvents(); //handle events
    };
    this.renderWidget = function () {
        debugger;
        var html = '<select style="width:200px;" class="grid-filter-type customerslist form-control">\
                    </select>';
        this.container.append(html);
    };
    /***
    * Method loads all customers from the server via Ajax:
    */
    this.loadCustomers = function () {
        var $this = this;
        $.post("/LogBook/Ajax/GetMainPurpose", function (data) {
            $this.fillCustomers(data);
        });
    };
    /***
    * Method fill customers select list by data
    */
    this.fillCustomers = function (items) {
        var customerList = this.container.find(".customerslist");
        customerList.append('<option value=""></option>');
        for (var i = 0; i < items.length; i++) {
            customerList.append('<option ' + (items[i].Text == this.value.filterValue ? 'selected="selected"' : '') + ' value="' + items[i].Text + '">' + items[i].Text + '</option>');
        }
    };
    /***
    * Internal method that register event handlers for 'apply' button.
    */
    this.registerEvents = function () {
        //get list with customers
        var customerList = this.container.find(".customerslist");
        //save current context:
        var $context = this;
        //register onclick event handler
        customerList.change(function () {
            //invoke callback with selected filter values:
            var values = [{ filterValue: $(this).val(), filterType: 1 /* Equals */ }];
            $context.cb(values);
        });
    };

}

function RecordsTypeWidget() {
    /***
    * This method must return type of registered widget type in 'SetFilterWidgetType' method
    */
    this.getAssociatedTypes = function () {
        return ["RecordsTypeFilterWidget"];
    };
    /***
    * This method invokes when filter widget was shown on the page
    */
    this.onShow = function () {
        /* Place your on show logic here */
    };

    this.showClearFilterButton = function () {
        return true;
    };
    /***
    * This method will invoke when user was clicked on filter button.
    * container - html element, which must contain widget layout;
    * lang - current language settings;
    * typeName - current column type (if widget assign to multipile types, see: getAssociatedTypes);
    * values - current filter values. Array of objects [{filterValue: '', filterType:'1'}];
    * cb - callback function that must invoked when user want to filter this column. Widget must pass filter type and filter value.
    * data - widget data passed from the server
    */
    this.onRender = function (container, lang, typeName, values, cb, data) {
        //store parameters:
        this.cb = cb;
        this.container = container;
        this.lang = lang;

        //this filterwidget demo supports only 1 filter value for column column
        this.value = values.length > 0 ? values[0] : { filterType: 1, filterValue: "" };

        this.renderWidget(); //onRender filter widget
        this.loadCustomers(); //load customer's list from the server
        this.registerEvents(); //handle events
    };
    this.renderWidget = function () {
        debugger;
        var html = '<select style="width:150px;" class="grid-filter-type RecordsTypelist form-control">\
                    </select>';
        this.container.append(html);
    };
    /***
    * Method loads all customers from the server via Ajax:
    */
    this.loadCustomers = function () {
        var $this = this;
        $.post("/LogBook/Ajax/getRecordsTypesForFilter", function (data) {
            $this.fillCustomers(data);
        });
    };
    /***
    * Method fill customers select list by data
    */
    this.fillCustomers = function (items) {
        var customerList = this.container.find(".RecordsTypelist");
        customerList.append('<option value=""></option>');
        for (var i = 0; i < items.length; i++) {
            customerList.append('<option ' + (items[i].Text == this.value.filterValue ? 'selected="selected"' : '') + ' value="' + items[i].Text + '">' + items[i].Text + '</option>');
        }
    };
    /***
    * Internal method that register event handlers for 'apply' button.
    */
    this.registerEvents = function () {
        //get list with customers
        var customerList = this.container.find(".RecordsTypelist");
        //save current context:
        var $context = this;
        //register onclick event handler
        customerList.change(function () {
            //invoke callback with selected filter values:
            var values = [{ filterValue: $(this).val(), filterType: 1 /* Equals */ }];
            $context.cb(values);
        });
    };

}

function SourcesWidget() {
    /***
    * This method must return type of registered widget type in 'SetFilterWidgetType' method
    */
    this.getAssociatedTypes = function () {
        return ["SourcesFilterWidget"];
    };
    /***
    * This method invokes when filter widget was shown on the page
    */
    this.onShow = function () {
        /* Place your on show logic here */
    };

    this.showClearFilterButton = function () {
        return true;
    };
    /***
    * This method will invoke when user was clicked on filter button.
    * container - html element, which must contain widget layout;
    * lang - current language settings;
    * typeName - current column type (if widget assign to multipile types, see: getAssociatedTypes);
    * values - current filter values. Array of objects [{filterValue: '', filterType:'1'}];
    * cb - callback function that must invoked when user want to filter this column. Widget must pass filter type and filter value.
    * data - widget data passed from the server
    */
    this.onRender = function (container, lang, typeName, values, cb, data) {
        //store parameters:
        this.cb = cb;
        this.container = container;
        this.lang = lang;

        //this filterwidget demo supports only 1 filter value for column column
        this.value = values.length > 0 ? values[0] : { filterType: 1, filterValue: "" };

        this.renderWidget(); //onRender filter widget
        this.loadCustomers(); //load customer's list from the server
        this.registerEvents(); //handle events
    };
    this.renderWidget = function () {
        debugger;
        var html = '<select style="width:150px;" class="grid-filter-type SourcesList form-control">\
                    </select>';
        this.container.append(html);
    };
    /***
    * Method loads all customers from the server via Ajax:
    */
    this.loadCustomers = function () {
        var $this = this;
        $.post("/LogBook/Ajax/getSourcesForFilter", function (data) {
            $this.fillCustomers(data);
        });
    };
    /***
    * Method fill customers select list by data
    */
    this.fillCustomers = function (items) {
        var customerList = this.container.find(".SourcesList");
        customerList.append('<option value=""></option>');
        for (var i = 0; i < items.length; i++) {
            customerList.append('<option ' + (items[i].Text == this.value.filterValue ? 'selected="selected"' : '') + ' value="' + items[i].Text + '">' + items[i].Text + '</option>');
        }
    };
    /***
    * Internal method that register event handlers for 'apply' button.
    */
    this.registerEvents = function () {
        //get list with customers
        var customerList = this.container.find(".SourcesList");
        //save current context:
        var $context = this;
        //register onclick event handler
        customerList.change(function () {
            //invoke callback with selected filter values:
            var values = [{ filterValue: $(this).val(), filterType: 1 /* Equals */ }];
            $context.cb(values);
        });
    };

}

function StatusesWidget() {
    /***
    * This method must return type of registered widget type in 'SetFilterWidgetType' method
    */
    this.getAssociatedTypes = function () {
        return ["StatusesFilterWidget"];
    };
    /***
    * This method invokes when filter widget was shown on the page
    */
    this.onShow = function () {
        /* Place your on show logic here */
    };

    this.showClearFilterButton = function () {
        return true;
    };
    /***
    * This method will invoke when user was clicked on filter button.
    * container - html element, which must contain widget layout;
    * lang - current language settings;
    * typeName - current column type (if widget assign to multipile types, see: getAssociatedTypes);
    * values - current filter values. Array of objects [{filterValue: '', filterType:'1'}];
    * cb - callback function that must invoked when user want to filter this column. Widget must pass filter type and filter value.
    * data - widget data passed from the server
    */
    this.onRender = function (container, lang, typeName, values, cb, data) {
        //store parameters:
        this.cb = cb;
        this.container = container;
        this.lang = lang;

        //this filterwidget demo supports only 1 filter value for column column
        this.value = values.length > 0 ? values[0] : { filterType: 1, filterValue: "" };

        this.renderWidget(); //onRender filter widget
        this.loadCustomers(); //load customer's list from the server
        this.registerEvents(); //handle events
    };
    this.renderWidget = function () {
        debugger;
        var html = '<select style="width:150px;" class="grid-filter-type StatusesList form-control">\
                    </select>';
        this.container.append(html);
    };
    /***
    * Method loads all customers from the server via Ajax:
    */
    this.loadCustomers = function () {
        var $this = this;
        $.post("/LogBook/Ajax/getStatusesForFilter", function (data) {
            $this.fillCustomers(data);
        });
    };
    /***
    * Method fill customers select list by data
    */
    this.fillCustomers = function (items) {
        var customerList = this.container.find(".StatusesList");
        customerList.append('<option value=""></option>');
        for (var i = 0; i < items.length; i++) {
            customerList.append('<option ' + (items[i].Text == this.value.filterValue ? 'selected="selected"' : '') + ' value="' + items[i].Text + '">' + items[i].Text + '</option>');
        }
    };
    /***
    * Internal method that register event handlers for 'apply' button.
    */
    this.registerEvents = function () {
        //get list with customers
        var customerList = this.container.find(".StatusesList");
        //save current context:
        var $context = this;
        //register onclick event handler
        customerList.change(function () {
            //invoke callback with selected filter values:
            var values = [{ filterValue: $(this).val(), filterType: 1 /* Equals */ }];
            $context.cb(values);
        });
    };

}

function StaffWidget() {
    /***
    * This method must return type of registered widget type in 'SetFilterWidgetType' method
    */
    this.getAssociatedTypes = function () {
        return ["StaffFilterWidget"];
    };
    /***
    * This method invokes when filter widget was shown on the page
    */
    this.onShow = function () {
        /* Place your on show logic here */
    };

    this.showClearFilterButton = function () {
        return true;
    };
    /***
    * This method will invoke when user was clicked on filter button.
    * container - html element, which must contain widget layout;
    * lang - current language settings;
    * typeName - current column type (if widget assign to multipile types, see: getAssociatedTypes);
    * values - current filter values. Array of objects [{filterValue: '', filterType:'1'}];
    * cb - callback function that must invoked when user want to filter this column. Widget must pass filter type and filter value.
    * data - widget data passed from the server
    */
    this.onRender = function (container, lang, typeName, values, cb, data) {
        //store parameters:
        this.cb = cb;
        this.container = container;
        this.lang = lang;

        //this filterwidget demo supports only 1 filter value for column column
        this.value = values.length > 0 ? values[0] : { filterType: 1, filterValue: "" };

        this.renderWidget(); //onRender filter widget
        this.loadCustomers(); //load customer's list from the server
        this.registerEvents(); //handle events
    };
    this.renderWidget = function () {
        debugger;
        var html = '<select style="width:150px;" class="grid-filter-type StaffList form-control">\
                    </select>';
        this.container.append(html);
    };
    /***
    * Method loads all customers from the server via Ajax:
    */
    this.loadCustomers = function () {
        var $this = this;
        $.post("/LogBook/Ajax/getRecordedByForFilter", function (data) {
            $this.fillCustomers(data);
        });
    };
    /***
    * Method fill customers select list by data
    */
    this.fillCustomers = function (items) {
        var customerList = this.container.find(".StaffList");
        customerList.append('<option value=""></option>');
        for (var i = 0; i < items.length; i++) {
            customerList.append('<option ' + (items[i].Text == this.value.filterValue ? 'selected="selected"' : '') + ' value="' + items[i].Text + '">' + items[i].Text + '</option>');
        }
    };
    /***
    * Internal method that register event handlers for 'apply' button.
    */
    this.registerEvents = function () {
        //get list with customers
        var customerList = this.container.find(".StaffList");
        //save current context:
        var $context = this;
        //register onclick event handler
        customerList.change(function () {
            //invoke callback with selected filter values:
            var values = [{ filterValue: $(this).val(), filterType: 1 /* Equals */ }];
            $context.cb(values);
        });
    };

}

function GovernorateWidget() {
    /***
    * This method must return type of registered widget type in 'SetFilterWidgetType' method
    */
    this.getAssociatedTypes = function () {
        return ["GovernorateFilterWidget"];
    };
    /***
    * This method invokes when filter widget was shown on the page
    */
    this.onShow = function () {
        /* Place your on show logic here */
    };

    this.showClearFilterButton = function () {
        return true;
    };
    /***
    * This method will invoke when user was clicked on filter button.
    * container - html element, which must contain widget layout;
    * lang - current language settings;
    * typeName - current column type (if widget assign to multipile types, see: getAssociatedTypes);
    * values - current filter values. Array of objects [{filterValue: '', filterType:'1'}];
    * cb - callback function that must invoked when user want to filter this column. Widget must pass filter type and filter value.
    * data - widget data passed from the server
    */
    this.onRender = function (container, lang, typeName, values, cb, data) {
        //store parameters:
        this.cb = cb;
        this.container = container;
        this.lang = lang;

        //this filterwidget demo supports only 1 filter value for column column
        this.value = values.length > 0 ? values[0] : { filterType: 1, filterValue: "" };

        this.renderWidget(); //onRender filter widget
        this.loadCustomers(); //load customer's list from the server
        this.registerEvents(); //handle events
    };
    this.renderWidget = function () {
        debugger;
        var html = '<select style="width:150px;" class="grid-filter-type GovernoratesList form-control">\
                    </select>';
        this.container.append(html);
    };
    /***
    * Method loads all customers from the server via Ajax:
    */
    this.loadCustomers = function () {
        var $this = this;
        $.post("/LogBook/Ajax/getGovernorateForFilter", function (data) {
            $this.fillCustomers(data);
        });
    };
    /***
    * Method fill customers select list by data
    */
    this.fillCustomers = function (items) {
        var customerList = this.container.find(".GovernoratesList");
        customerList.append('<option value=""></option>');
        for (var i = 0; i < items.length; i++) {
            customerList.append('<option ' + (items[i].Text == this.value.filterValue ? 'selected="selected"' : '') + ' value="' + items[i].Text + '">' + items[i].Text + '</option>');
        }
    };
    /***
    * Internal method that register event handlers for 'apply' button.
    */
    this.registerEvents = function () {
        //get list with customers
        var customerList = this.container.find(".GovernoratesList");
        //save current context:
        var $context = this;
        //register onclick event handler
        customerList.change(function () {
            //invoke callback with selected filter values:
            var values = [{ filterValue: $(this).val(), filterType: 1 /* Equals */ }];
            $context.cb(values);
        });
    };

}

function ProgramWidget() {
    /***
    * This method must return type of registered widget type in 'SetFilterWidgetType' method
    */
    this.getAssociatedTypes = function () {
        return ["ProgramFilterWidget"];
    };
    /***
    * This method invokes when filter widget was shown on the page
    */
    this.onShow = function () {
        /* Place your on show logic here */
    };

    this.showClearFilterButton = function () {
        return true;
    };
    /***
    * This method will invoke when user was clicked on filter button.
    * container - html element, which must contain widget layout;
    * lang - current language settings;
    * typeName - current column type (if widget assign to multipile types, see: getAssociatedTypes);
    * values - current filter values. Array of objects [{filterValue: '', filterType:'1'}];
    * cb - callback function that must invoked when user want to filter this column. Widget must pass filter type and filter value.
    * data - widget data passed from the server
    */
    this.onRender = function (container, lang, typeName, values, cb, data) {
        //store parameters:
        this.cb = cb;
        this.container = container;
        this.lang = lang;

        //this filterwidget demo supports only 1 filter value for column column
        this.value = values.length > 0 ? values[0] : { filterType: 1, filterValue: "" };

        this.renderWidget(); //onRender filter widget
        this.loadCustomers(); //load customer's list from the server
        this.registerEvents(); //handle events
    };
    this.renderWidget = function () {
        debugger;
        var html = '<select style="width:150px;" class="grid-filter-type ProgramsList form-control">\
                    </select>';
        this.container.append(html);
    };
    /***
    * Method loads all customers from the server via Ajax:
    */
    this.loadCustomers = function () {
        var $this = this;
        $.post("/LogBook/Ajax/getProgramForFilter", function (data) {
            $this.fillCustomers(data);
        });
    };
    /***
    * Method fill customers select list by data
    */
    this.fillCustomers = function (items) {
        var customerList = this.container.find(".ProgramsList");
        customerList.append('<option value=""></option>');
        for (var i = 0; i < items.length; i++) {
            customerList.append('<option ' + (items[i].Text == this.value.filterValue ? 'selected="selected"' : '') + ' value="' + items[i].Text + '">' + items[i].Text + '</option>');
        }
    };
    /***
    * Internal method that register event handlers for 'apply' button.
    */
    this.registerEvents = function () {
        //get list with customers
        var customerList = this.container.find(".ProgramsList");
        //save current context:
        var $context = this;
        //register onclick event handler
        customerList.change(function () {
            //invoke callback with selected filter values:
            var values = [{ filterValue: $(this).val(), filterType: 1 /* Equals */ }];
            $context.cb(values);
        });
    };

}

function SubProgramWidget() {
    /***
    * This method must return type of registered widget type in 'SetFilterWidgetType' method
    */
	this.getAssociatedTypes = function () {
		return ["SubProgramFilterWidget"];
	};
    /***
    * This method invokes when filter widget was shown on the page
    */
	this.onShow = function () {
		/* Place your on show logic here */
	};

	this.showClearFilterButton = function () {
		return true;
	};
    /***
    * This method will invoke when user was clicked on filter button.
    * container - html element, which must contain widget layout;
    * lang - current language settings;
    * typeName - current column type (if widget assign to multipile types, see: getAssociatedTypes);
    * values - current filter values. Array of objects [{filterValue: '', filterType:'1'}];
    * cb - callback function that must invoked when user want to filter this column. Widget must pass filter type and filter value.
    * data - widget data passed from the server
    */
	this.onRender = function (container, lang, typeName, values, cb, data) {
		//store parameters:
		this.cb = cb;
		this.container = container;
		this.lang = lang;

		//this filterwidget demo supports only 1 filter value for column column
		this.value = values.length > 0 ? values[0] : { filterType: 1, filterValue: "" };

		this.renderWidget(); //onRender filter widget
		this.loadCustomers(); //load customer's list from the server
		this.registerEvents(); //handle events
	};
	this.renderWidget = function () {
		debugger;
		var html = '<select style="width:150px;" class="grid-filter-type SubProgramsList form-control">\
                    </select>';
		this.container.append(html);
	};
    /***
    * Method loads all customers from the server via Ajax:
    */
	this.loadCustomers = function () {
		var $this = this;
		$.post("/LogBook/Ajax/getSubProgramForFilter", function (data) {
			$this.fillCustomers(data);
		});
	};
    /***
    * Method fill customers select list by data
    */
	this.fillCustomers = function (items) {
	    var customerList = this.container.find(".SubProgramsList");
	    customerList.append('<option value=""></option>');
		for (var i = 0; i < items.length; i++) {
			customerList.append('<option ' + (items[i].Text == this.value.filterValue ? 'selected="selected"' : '') + ' value="' + items[i].Text + '">' + items[i].Text + '</option>');
		}
	};
    /***
    * Internal method that register event handlers for 'apply' button.
    */
	this.registerEvents = function () {
		//get list with customers
		var customerList = this.container.find(".SubProgramsList");
		//save current context:
		var $context = this;
		//register onclick event handler
		customerList.change(function () {
			//invoke callback with selected filter values:
			var values = [{ filterValue: $(this).val(), filterType: 1 /* Equals */ }];
			$context.cb(values);
		});
	};

}

function ResponsibleUnitWidget() {
    /***
    * This method must return type of registered widget type in 'SetFilterWidgetType' method
    */
    this.getAssociatedTypes = function () {
        return ["ResponsibleUnitFilterWidget"];
    };
    /***
    * This method invokes when filter widget was shown on the page
    */
    this.onShow = function () {
        /* Place your on show logic here */
    };

    this.showClearFilterButton = function () {
        return true;
    };
    /***
    * This method will invoke when user was clicked on filter button.
    * container - html element, which must contain widget layout;
    * lang - current language settings;
    * typeName - current column type (if widget assign to multipile types, see: getAssociatedTypes);
    * values - current filter values. Array of objects [{filterValue: '', filterType:'1'}];
    * cb - callback function that must invoked when user want to filter this column. Widget must pass filter type and filter value.
    * data - widget data passed from the server
    */
    this.onRender = function (container, lang, typeName, values, cb, data) {
        //store parameters:
        this.cb = cb;
        this.container = container;
        this.lang = lang;

        //this filterwidget demo supports only 1 filter value for column column
        this.value = values.length > 0 ? values[0] : { filterType: 1, filterValue: "" };

        this.renderWidget(); //onRender filter widget
        this.loadCustomers(); //load customer's list from the server
        this.registerEvents(); //handle events
    };
    this.renderWidget = function () {
        debugger;
        var html = '<select style="width:150px;" class="grid-filter-type ResponsibleUnitsList form-control">\
                    </select>';
        this.container.append(html);
    };
    /***
    * Method loads all customers from the server via Ajax:
    */
    this.loadCustomers = function () {
        var $this = this;
        $.post("/LogBook/Ajax/getResponsibleUnitForFilter", function (data) {
            $this.fillCustomers(data);
        });
    };
    /***
    * Method fill customers select list by data
    */
    this.fillCustomers = function (items) {
        var customerList = this.container.find(".ResponsibleUnitsList");
        customerList.append('<option value=""></option>');
        for (var i = 0; i < items.length; i++) {
            customerList.append('<option ' + (items[i].Text == this.value.filterValue ? 'selected="selected"' : '') + ' value="' + items[i].Text + '">' + items[i].Text + '</option>');
        }
    };
    /***
    * Internal method that register event handlers for 'apply' button.
    */
    this.registerEvents = function () {
        //get list with customers
        var customerList = this.container.find(".ResponsibleUnitsList");
        //save current context:
        var $context = this;
        //register onclick event handler
        customerList.change(function () {
            //invoke callback with selected filter values:
            var values = [{ filterValue: $(this).val(), filterType: 1 /* Equals */ }];
            $context.cb(values);
        });
    };
}

function CategoryWidget() {
    /***
    * This method must return type of registered widget type in 'SetFilterWidgetType' method
    */
	this.getAssociatedTypes = function () {
		return ["CategoryFilterWidget"];
	};
    /***
    * This method invokes when filter widget was shown on the page
    */
	this.onShow = function () {
		/* Place your on show logic here */
	};

	this.showClearFilterButton = function () {
		return true;
	};
    /***
    * This method will invoke when user was clicked on filter button.
    * container - html element, which must contain widget layout;
    * lang - current language settings;
    * typeName - current column type (if widget assign to multipile types, see: getAssociatedTypes);
    * values - current filter values. Array of objects [{filterValue: '', filterType:'1'}];
    * cb - callback function that must invoked when user want to filter this column. Widget must pass filter type and filter value.
    * data - widget data passed from the server
    */
	this.onRender = function (container, lang, typeName, values, cb, data) {
		//store parameters:
		this.cb = cb;
		this.container = container;
		this.lang = lang;

		//this filterwidget demo supports only 1 filter value for column column
		this.value = values.length > 0 ? values[0] : { filterType: 1, filterValue: "" };

		this.renderWidget(); //onRender filter widget
		this.loadCustomers(); //load customer's list from the server
		this.registerEvents(); //handle events
	};
	this.renderWidget = function () {
		debugger;
		var html = '<select style="width:150px;" class="grid-filter-type CategorysList form-control">\
                    </select>';
		this.container.append(html);
	};
    /***
    * Method loads all customers from the server via Ajax:
    */
	this.loadCustomers = function () {
		var $this = this;
		$.post("/LogBook/Ajax/getCategoryForFilter", function (data) {
			$this.fillCustomers(data);
		});
	};
    /***
    * Method fill customers select list by data
    */
	this.fillCustomers = function (items) {
	    var customerList = this.container.find(".CategorysList");
	    customerList.append('<option value=""></option>');
		for (var i = 0; i < items.length; i++) {
			customerList.append('<option ' + (items[i].Text == this.value.filterValue ? 'selected="selected"' : '') + ' value="' + items[i].Text + '">' + items[i].Text + '</option>');
		}
	};
    /***
    * Internal method that register event handlers for 'apply' button.
    */
	this.registerEvents = function () {
		//get list with customers
		var customerList = this.container.find(".CategorysList");
		//save current context:
		var $context = this;
		//register onclick event handler
		customerList.change(function () {
			//invoke callback with selected filter values:
			var values = [{ filterValue: $(this).val(), filterType: 1 /* Equals */ }];
			$context.cb(values);
		});
	};

}

function DistrictWidget() {
    /***
    * This method must return type of registered widget type in 'SetFilterWidgetType' method
    */
	this.getAssociatedTypes = function () {
		return ["DistrictFilterWidget"];
	};
    /***
    * This method invokes when filter widget was shown on the page
    */
	this.onShow = function () {
		/* Place your on show logic here */
	};

	this.showClearFilterButton = function () {
		return true;
	};
    /***
    * This method will invoke when user was clicked on filter button.
    * container - html element, which must contain widget layout;
    * lang - current language settings;
    * typeName - current column type (if widget assign to multipile types, see: getAssociatedTypes);
    * values - current filter values. Array of objects [{filterValue: '', filterType:'1'}];
    * cb - callback function that must invoked when user want to filter this column. Widget must pass filter type and filter value.
    * data - widget data passed from the server
    */
	this.onRender = function (container, lang, typeName, values, cb, data) {
		//store parameters:
		this.cb = cb;
		this.container = container;
		this.lang = lang;

		//this filterwidget demo supports only 1 filter value for column column
		this.value = values.length > 0 ? values[0] : { filterType: 1, filterValue: "" };

		this.renderWidget(); //onRender filter widget
		this.loadCustomers(); //load customer's list from the server
		this.registerEvents(); //handle events
	};
	this.renderWidget = function () {
		debugger;
		var html = '<select style="width:150px;" class="grid-filter-type DistrictList form-control">\
                    </select>';
		this.container.append(html);
	};
    /***
    * Method loads all customers from the server via Ajax:
    */
	this.loadCustomers = function () {
		var $this = this;
		$.post("/LogBook/Ajax/getDistrictForFilter", function (data) {
			$this.fillCustomers(data);
		});
	};
    /***
    * Method fill customers select list by data
    */
	this.fillCustomers = function (items) {
	    var customerList = this.container.find(".DistrictList");
	    customerList.append('<option value=""></option>');
		for (var i = 0; i < items.length; i++) {
			customerList.append('<option ' + (items[i].Text == this.value.filterValue ? 'selected="selected"' : '') + ' value="' + items[i].Text + '">' + items[i].Text + '</option>');
		}
	};
    /***
    * Internal method that register event handlers for 'apply' button.
    */
	this.registerEvents = function () {
		//get list with customers
		var customerList = this.container.find(".DistrictList");
		//save current context:
		var $context = this;
		//register onclick event handler
		customerList.change(function () {
			//invoke callback with selected filter values:
			var values = [{ filterValue: $(this).val(), filterType: 1 /* Equals */ }];
			$context.cb(values);
		});
	};

}

function PartnerWidget() {
    /***
    * This method must return type of registered widget type in 'SetFilterWidgetType' method
    */
    this.getAssociatedTypes = function () {
        return ["PartnerFilterWidget"];
    };
    /***
    * This method invokes when filter widget was shown on the page
    */
    this.onShow = function () {
        /* Place your on show logic here */
    };

    this.showClearFilterButton = function () {
        return true;
    };
    /***
    * This method will invoke when user was clicked on filter button.
    * container - html element, which must contain widget layout;
    * lang - current language settings;
    * typeName - current column type (if widget assign to multipile types, see: getAssociatedTypes);
    * values - current filter values. Array of objects [{filterValue: '', filterType:'1'}];
    * cb - callback function that must invoked when user want to filter this column. Widget must pass filter type and filter value.
    * data - widget data passed from the server
    */
    this.onRender = function (container, lang, typeName, values, cb, data) {
        //store parameters:
        this.cb = cb;
        this.container = container;
        this.lang = lang;

        //this filterwidget demo supports only 1 filter value for column column
        this.value = values.length > 0 ? values[0] : { filterType: 1, filterValue: "" };

        this.renderWidget(); //onRender filter widget
        this.loadCustomers(); //load customer's list from the server
        this.registerEvents(); //handle events
    };
    this.renderWidget = function () {
        debugger;
        var html = '<select style="width:150px;" class="grid-filter-type PartnerList form-control">\
                    </select>';
        this.container.append(html);
    };
    /***
    * Method loads all customers from the server via Ajax:
    */
    this.loadCustomers = function () {
        var $this = this;
        $.post("/LogBook/Ajax/getPartnerForFilter", function (data) {
            $this.fillCustomers(data);
        });
    };
    /***
    * Method fill customers select list by data
    */
    this.fillCustomers = function (items) {
        var customerList = this.container.find(".PartnerList");
        customerList.append('<option value=""></option>');
        for (var i = 0; i < items.length; i++) {
            customerList.append('<option ' + (items[i].Text == this.value.filterValue ? 'selected="selected"' : '') + ' value="' + items[i].Text + '">' + items[i].Text + '</option>');
        }
    };
    /***
    * Internal method that register event handlers for 'apply' button.
    */
    this.registerEvents = function () {
        //get list with customers
        var customerList = this.container.find(".PartnerList");
        //save current context:
        var $context = this;
        //register onclick event handler
        customerList.change(function () {
            //invoke callback with selected filter values:
            var values = [{ filterValue: $(this).val(), filterType: 1 /* Equals */ }];
            $context.cb(values);
        });
    };
}
function CBOWidget() {
    /***
    * This method must return type of registered widget type in 'SetFilterWidgetType' method
    */
    this.getAssociatedTypes = function () {
        return ["CBOFilterWidget"];
    };
    /***
    * This method invokes when filter widget was shown on the page
    */
    this.onShow = function () {
        /* Place your on show logic here */
    };

    this.showClearFilterButton = function () {
        return true;
    };
    /***
    * This method will invoke when user was clicked on filter button.
    * container - html element, which must contain widget layout;
    * lang - current language settings;
    * typeName - current column type (if widget assign to multipile types, see: getAssociatedTypes);
    * values - current filter values. Array of objects [{filterValue: '', filterType:'1'}];
    * cb - callback function that must invoked when user want to filter this column. Widget must pass filter type and filter value.
    * data - widget data passed from the server
    */
    this.onRender = function (container, lang, typeName, values, cb, data) {
        //store parameters:
        this.cb = cb;
        this.container = container;
        this.lang = lang;

        //this filterwidget demo supports only 1 filter value for column column
        this.value = values.length > 0 ? values[0] : { filterType: 1, filterValue: "" };

        this.renderWidget(); //onRender filter widget
        this.loadCustomers(); //load customer's list from the server
        this.registerEvents(); //handle events
    };
    this.renderWidget = function () {
        var html = '<select style="width:150px;" class="grid-filter-type CBOList form-control">\
                    </select>';
        this.container.append(html);
    };
    /***
    * Method loads all customers from the server via Ajax:
    */
    this.loadCustomers = function () {
        var $this = this;
        $.post("/LogBook/Ajax/getCBOForFilter", function (data) {
            $this.fillCustomers(data);
        });
    };
    /***
    * Method fill customers select list by data
    */
    this.fillCustomers = function (items) {
        var customerList = this.container.find(".CBOList");
        customerList.append('<option value=""></option>');
        for (var i = 0; i < items.length; i++) {
            customerList.append('<option ' + (items[i].Text == this.value.filterValue ? 'selected="selected"' : '') + ' value="' + items[i].Text + '">' + items[i].Text + '</option>');
        }
    };
    /***
    * Internal method that register event handlers for 'apply' button.
    */
    this.registerEvents = function () {
        //get list with customers
        var customerList = this.container.find(".CBOList");
        //save current context:
        var $context = this;
        //register onclick event handler
        customerList.change(function () {
            //invoke callback with selected filter values:
            var values = [{ filterValue: $(this).val(), filterType: 1 /* Equals */ }];
            $context.cb(values);
        });
    };
}
function HelpdeskCategoryWidget() {
    /***
    * This method must return type of registered widget type in 'SetFilterWidgetType' method
    */
    this.getAssociatedTypes = function () {
        return ["HelpdeskCategoryFilterWidget"];
    };
    /***
    * This method invokes when filter widget was shown on the page
    */
    this.onShow = function () {
        /* Place your on show logic here */
    };

    this.showClearFilterButton = function () {
        return true;
    };
    /***
    * This method will invoke when user was clicked on filter button.
    * container - html element, which must contain widget layout;
    * lang - current language settings;
    * typeName - current column type (if widget assign to multipile types, see: getAssociatedTypes);
    * values - current filter values. Array of objects [{filterValue: '', filterType:'1'}];
    * cb - callback function that must invoked when user want to filter this column. Widget must pass filter type and filter value.
    * data - widget data passed from the server
    */
    this.onRender = function (container, lang, typeName, values, cb, data) {
        //store parameters:
        this.cb = cb;
        this.container = container;
        this.lang = lang;

        //this filterwidget demo supports only 1 filter value for column column
        this.value = values.length > 0 ? values[0] : { filterType: 1, filterValue: "" };

        this.renderWidget(); //onRender filter widget
        this.loadCustomers(); //load customer's list from the server
        this.registerEvents(); //handle events
    };
    this.renderWidget = function () {
        var html = '<select style="width:150px;" class="grid-filter-type HelpdeskCategoryList form-control">\
                    </select>';
        this.container.append(html);
    };
    /***
    * Method loads all customers from the server via Ajax:
    */
    this.loadCustomers = function () {
        var $this = this;
        $.post("/LogBook/Ajax/getHelpdeskCategoryForFilter", function (data) {
            $this.fillCustomers(data);
        });
    };
    /***
    * Method fill customers select list by data
    */
    this.fillCustomers = function (items) {
        var customerList = this.container.find(".HelpdeskCategoryList");
        customerList.append('<option value=""></option>');
        for (var i = 0; i < items.length; i++) {
            customerList.append('<option ' + (items[i].Text == this.value.filterValue ? 'selected="selected"' : '') + ' value="' + items[i].Text + '">' + items[i].Text + '</option>');
        }
    };
    /***
    * Internal method that register event handlers for 'apply' button.
    */
    this.registerEvents = function () {
        //get list with customers
        var customerList = this.container.find(".HelpdeskCategoryList");
        //save current context:
        var $context = this;
        //register onclick event handler
        customerList.change(function () {
            //invoke callback with selected filter values:
            var values = [{ filterValue: $(this).val(), filterType: 1 /* Equals */ }];
            $context.cb(values);
        });
    };
}
function HelpdeskSubcategoryWidget() {
    /***
    * This method must return type of registered widget type in 'SetFilterWidgetType' method
    */
    this.getAssociatedTypes = function () {
        return ["HelpdeskSubcategoryFilterWidget"];
    };
    /***
    * This method invokes when filter widget was shown on the page
    */
    this.onShow = function () {
        /* Place your on show logic here */
    };

    this.showClearFilterButton = function () {
        return true;
    };
    /***
    * This method will invoke when user was clicked on filter button.
    * container - html element, which must contain widget layout;
    * lang - current language settings;
    * typeName - current column type (if widget assign to multipile types, see: getAssociatedTypes);
    * values - current filter values. Array of objects [{filterValue: '', filterType:'1'}];
    * cb - callback function that must invoked when user want to filter this column. Widget must pass filter type and filter value.
    * data - widget data passed from the server
    */
    this.onRender = function (container, lang, typeName, values, cb, data) {
        //store parameters:
        this.cb = cb;
        this.container = container;
        this.lang = lang;

        //this filterwidget demo supports only 1 filter value for column column
        this.value = values.length > 0 ? values[0] : { filterType: 1, filterValue: "" };

        this.renderWidget(); //onRender filter widget
        this.loadCustomers(); //load customer's list from the server
        this.registerEvents(); //handle events
    };
    this.renderWidget = function () {
        debugger;
        var html = '<select style="width:150px;" class="grid-filter-type HelpdeskSubcategoryList form-control">\
                    </select>';
        this.container.append(html);
    };
    /***
    * Method loads all customers from the server via Ajax:
    */
    this.loadCustomers = function () {
        var $this = this;
        $.post("/LogBook/Ajax/getHelpdeskSubcategoryForFilter", function (data) {
            $this.fillCustomers(data);
        });
    };
    /***
    * Method fill customers select list by data
    */
    this.fillCustomers = function (items) {
        var customerList = this.container.find(".HelpdeskSubcategoryList");
        customerList.append('<option value=""></option>');
        for (var i = 0; i < items.length; i++) {
            customerList.append('<option ' + (items[i].Text == this.value.filterValue ? 'selected="selected"' : '') + ' value="' + items[i].Text + '">' + items[i].Text + '</option>');
        }
    };
    /***
    * Internal method that register event handlers for 'apply' button.
    */
    this.registerEvents = function () {
        //get list with customers
        var customerList = this.container.find(".HelpdeskSubcategoryList");
        //save current context:
        var $context = this;
        //register onclick event handler
        customerList.change(function () {
            //invoke callback with selected filter values:
            var values = [{ filterValue: $(this).val(), filterType: 1 /* Equals */ }];
            $context.cb(values);
        });
    };
}
function AddressedOnSpotWidget() {
    /***
    * This method must return type of registered widget type in 'SetFilterWidgetType' method
    */
    this.getAssociatedTypes = function () {
        return ["AddressedOnSpotFilterWidget"];
    };
    /***
    * This method invokes when filter widget was shown on the page
    */
    this.onShow = function () {
        /* Place your on show logic here */
    };

    this.showClearFilterButton = function () {
        return true;
    };
    /***
    * This method will invoke when user was clicked on filter button.
    * container - html element, which must contain widget layout;
    * lang - current language settings;
    * typeName - current column type (if widget assign to multipile types, see: getAssociatedTypes);
    * values - current filter values. Array of objects [{filterValue: '', filterType:'1'}];
    * cb - callback function that must invoked when user want to filter this column. Widget must pass filter type and filter value.
    * data - widget data passed from the server
    */
    this.onRender = function (container, lang, typeName, values, cb, data) {
        //store parameters:
        this.cb = cb;
        this.container = container;
        this.lang = lang;

        //this filterwidget demo supports only 1 filter value for column column
        this.value = values.length > 0 ? values[0] : { filterType: 1, filterValue: "" };

        this.renderWidget(); //onRender filter widget
        this.loadCustomers(); //load customer's list from the server
        this.registerEvents(); //handle events
    };
    this.renderWidget = function () {
        debugger;
        var html = '<select style="width:150px;" class="grid-filter-type AddressedOnSpotList form-control">\
                    </select>';
        this.container.append(html);
    };
    /***
    * Method loads all customers from the server via Ajax:
    */
    this.loadCustomers = function () {
        var $this = this;
        $.post("/LogBook/Ajax/getAddressedOnSpotForFilter", function (data) {
            $this.fillCustomers(data);
        });
    };
    /***
    * Method fill customers select list by data
    */
    this.fillCustomers = function (items) {
        var customerList = this.container.find(".AddressedOnSpotList");
        customerList.append('<option value=""></option>');
        for (var i = 0; i < items.length; i++) {
            customerList.append('<option ' + (items[i].Text == this.value.filterValue ? 'selected="selected"' : '') + ' value="' + items[i].Text + '">' + items[i].Text + '</option>');
        }
    };
    /***
    * Internal method that register event handlers for 'apply' button.
    */
    this.registerEvents = function () {
        //get list with customers
        var customerList = this.container.find(".AddressedOnSpotList");
        //save current context:
        var $context = this;
        //register onclick event handler
        customerList.change(function () {
            //invoke callback with selected filter values:
            var values = [{ filterValue: $(this).val(), filterType: 1 /* Equals */ }];
            $context.cb(values);
        });
    };
}
function GenderWidget() {
    /***
    * This method must return type of registered widget type in 'SetFilterWidgetType' method
    */
    this.getAssociatedTypes = function () {
        return ["GenderFilterWidget"];
    };
    /***
    * This method invokes when filter widget was shown on the page
    */
    this.onShow = function () {
        /* Place your on show logic here */
    };

    this.showClearFilterButton = function () {
        return true;
    };
    /***
    * This method will invoke when user was clicked on filter button.
    * container - html element, which must contain widget layout;
    * lang - current language settings;
    * typeName - current column type (if widget assign to multipile types, see: getAssociatedTypes);
    * values - current filter values. Array of objects [{filterValue: '', filterType:'1'}];
    * cb - callback function that must invoked when user want to filter this column. Widget must pass filter type and filter value.
    * data - widget data passed from the server
    */
    this.onRender = function (container, lang, typeName, values, cb, data) {
        //store parameters:
        this.cb = cb;
        this.container = container;
        this.lang = lang;

        //this filterwidget demo supports only 1 filter value for column column
        this.value = values.length > 0 ? values[0] : { filterType: 1, filterValue: "" };

        this.renderWidget(); //onRender filter widget
        this.loadCustomers(); //load customer's list from the server
        this.registerEvents(); //handle events
    };
    this.renderWidget = function () {
        debugger;
        var html = '<select style="width:150px;" class="grid-filter-type GendersList form-control">\
                    </select>';
        this.container.append(html);
    };
    /***
    * Method loads all customers from the server via Ajax:
    */
    this.loadCustomers = function () {
        var $this = this;
        $.post("/LogBook/Ajax/getGenderForFilter", function (data) {
            $this.fillCustomers(data);
        });
    };
    /***
    * Method fill customers select list by data
    */
    this.fillCustomers = function (items) {
        var customerList = this.container.find(".GendersList");
        customerList.append('<option value=""></option>');
        for (var i = 0; i < items.length; i++) {
            customerList.append('<option ' + (items[i].Text == this.value.filterValue ? 'selected="selected"' : '') + ' value="' + items[i].Text + '">' + items[i].Text + '</option>');
        }
    };
    /***
    * Internal method that register event handlers for 'apply' button.
    */
    this.registerEvents = function () {
        //get list with customers
        var customerList = this.container.find(".GendersList");
        //save current context:
        var $context = this;
        //register onclick event handler
        customerList.change(function () {
            //invoke callback with selected filter values:
            var values = [{ filterValue: $(this).val(), filterType: 1 /* Equals */ }];
            $context.cb(values);
        });
    };

}
