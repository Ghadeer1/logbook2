﻿msg = "";
error = false;
var records = [];
var updated = false;
$("#success-alert").hide();

$(function () {
    pageGrids.ordersGrid.addFilterWidget(new StatusesWidget());
    pageGrids.ordersGrid.addFilterWidget(new GovernorateWidget());
    pageGrids.ordersGrid.addFilterWidget(new ResponsibleUnitWidget());
    pageGrids.ordersGrid.addFilterWidget(new PartnerWidget());
    pageGrids.ordersGrid.addFilterWidget(new CBOWidget());
    pageGrids.ordersGrid.addFilterWidget(new HelpdeskCategoryWidget());
    pageGrids.ordersGrid.addFilterWidget(new HelpdeskSubcategoryWidget());
    pageGrids.ordersGrid.addFilterWidget(new AddressedOnSpotWidget());
    pageGrids.ordersGrid.addFilterWidget(new GenderWidget());
});

function newRecord() {
    $('.datepicker').datepicker({ dateFormat: 'yy-mm-dd' });
    reset();
    $('#update_data').modal('show');
    form1.act.value = "create";
    addNew();
}

function addNew() {
    reset();
    form1.act.value = "create";
    $("#message").html("You are creating a new record");
    $("#RecordNumber").html('');
}

function edit(id) {
    unlockForm();
    form1.act.value = "edit";
    form1.SavedRecordID = id.toString();

    $('#update_data').modal({
        backdrop: 'static',
        keyboard: false
    });
    if ((id != null) && (id != NaN)) {
        $.post("/LogBook/ajax/EditHelpdesk/" + id, function (data, status) {
            reset();
            form1.Partner.value = data[0].Partner;
            changePartner();
            form1.Governorate.value = data[0].Governorate;
            filterCBO();
            form1.CBO.value = data[0].CBO;
            form1.RecordNumber.value = data[0].RecordNumber;
            form1.Status.value = data[0].Status;
            form1.CaseNumber.value = data[0].CaseNumber;
            form1.PhoneNumber.value = data[0].PhoneNumber;
            form1.Gender.value = data[0].Gender;
            form1.Category.value = data[0].Category;
            changeCategory();
            form1.SubCategory.value = data[0].Subcategory;
            changeSubcategory()
            form1.ReplacementRequired.value = data[0].ReplacementRequired;
            form1.Details.value = data[0].Details;
            form1.AddressedOnSpot.value = data[0].AddressedOnSpot;
            changeAddressedOnSpot();
            form1.ResponsibleUnit.value = data[0].ResponsibleUnit;
            checkAvailability(data[0].CaseNumber);
        });
        $("#message").html("You are editing record no.: ");
        $("#RecordNumber").html(id);
    }
}

function reset() {
    if (form1.act.value == "create")
        $(".optional").hide();
    else
        $(".optional").show();
    form1.Status.value = "";
    form1.CaseNumber.value = "";
    form1.PhoneNumber.value = "";
    form1.Gender.value = "";
    form1.Governorate.value = "";
    form1.CBO.value = "";
    form1.Category.value = "";
    form1.SubCategory.value = "";
    form1.ReplacementRequired.value = "";
    form1.Details.value = "";
    form1.AddressedOnSpot.value = "";
    form1.ResponsibleUnit.value = "";
    $("#status").hide();
    $("#ReplacementRequired-tr").hide();
}
function hideForm() {
    reset();
    $('#update_data').modal('hide');
    if (updated) {
        location.reload();
        updated = false;
    }
}
function viewBeneficiaryProfile(id) {
    var dialog = new BootstrapDialog.show({
        title: 'Beneficiary Profile for '+id,
        cssClass: 'view-dialog',
        message: $('<div></div>').load('/LogBook/HelpDesk/BeneficiaryProfile?id='+id)
    });
}
function checkAvailability(value) {
    $("#status").show();
    $("#status").html("Checking beneficiary ID ...");
    $("#status").blink();
    $.get("/logBook/Ajax/checkBeneficiaryId?id=" + value, function (response, status) {
        debugger;
        data = JSON.parse(response);
        if (data.status == 200) {
            $("#status").html("<a style='text-decoration:underline' href=javascript:viewBeneficiaryProfile('"+value+"')>WFP beneficiary</a>");
            $("#status").css("color", "green");
        }
        else if (data.status == 400) {
            $("#status").html("Non-WFP beneficiary");
            $("#status").css("color", "red");
        }
        else {
            $("#status").html("Unable to check index number");
            $("#status").css("color", "red");
        }
        $('#status').unblink();
    });
}
function saveRecord(close) {
    debugger;
    if (formIsValid()) {
        unlockForm();
        debugger;
        $("#loader2").show();
        var url = "";
        if (form1.act.value == "create") {
            url = "/LogBook/Ajax/CreateHelpdesk";
        }
        else if (form1.act.value == "edit") {
            url = "/LogBook/Ajax/UpdateHelpdesk";
        }

        $.ajax({
            type: "POST",
            url: url,
            data: $('form').serialize(),
            success: function (msg) {
                if (msg.status == "200") {
                    $("#alert-message").html(msg.StatusMessage);
                    $("#success-alert").alert();
                    $("#success-alert").fadeTo(2000, 500).slideUp(500, function () {
                        $("#success-alert").slideUp(500);
                    });
                    $("#myrecords").html("Number of my records today: " + msg.MyRecords)
                    updated = true;
                    if (close) {
                        location.reload();
                        updated = false;
                    }
                    else {
                        addNew();
                    }
                }
                else {
                    alert(msg.StatusMessage);
                }
                $("#loader2").hide();
            },
            error: function (xhr, status, error) {
                var err = eval("(" + xhr.responseText + ")");
                $("#loader2").hide();
                alert(err.Message);
            }
        });
    }
}
function changeCategory() {
    $("#SubCategory-tr").show();
    cascadeDropdown("Category", "SubCategory", SubCategory);
    debugger;
    if (form1.Category.value == "1") {
        $("#ReplacementRequired-tr").show();
    }
    else {
        $("#ReplacementRequired-tr").hide();
    }
}
function cascadeDropdown(parent, child, itemList) {
    clearDropdown(child);
    addDropDownItem(child, "", "");
    var key = document.getElementById(parent).value;
    for (var item in itemList) {
        var thisItem = itemList[item];
        if (thisItem.pkey == key) {
            addDropDownItem(child, thisItem.id, thisItem.name);
        }
    }
    document.getElementById(child).value = "";
}
function clearDropdown(ddlName) {
    var selectbox = document.getElementById(ddlName);
    var i;
    for (i = selectbox.options.length - 1; i >= 0; i--) {
        selectbox.remove(i);
    }
}
function addDropDownItem(ddlName, optValue, optText) {
    var opt = document.createElement("option");
    opt.text = optText;
    opt.value = optValue;
    document.getElementById(ddlName).options.add(opt);
}
function formIsValid() {
    msg = "";
    error = false;
    checkRequired("Partner", "Partner is required");
    checkRequired("Governorate", "Governorate is required");
    checkRequired("CaseNumber", "Case ID is required");
    checkRequired("PhoneNumber", "Case phone number is required");
    checkNumber("PhoneNumber", "The phone number should be numbers only");
    checkRequired("Gender", "Gender is required");
    checkRequired("Category", "Category is required");
    checkRequired("SubCategory", "Subcategory is required");
    checkRequired("Details", "Details are required");
    checkRequired("AddressedOnSpot", "Please indicate if the issue was addressed on spot");
    checkRequired("Status", "Status is required");
    if (form1.Status.value == 1)
        checkRequired("ResponsibleUnit", "Since the status is open, responsible unit is required");
    if (form1.Category.value == 1)
        checkRequired("ReplacementRequired", "Please indicate if card replacement is required");
    if (error) {
        alert(msg);
        return false;
    }
    else {
        return true;
    }
}
function checkRequired(param, message) {
    console.log(param);
    if (document.getElementById(param).value == null
        || document.getElementById(param).value == '') {
        error = true;
        if (msg == "")
            msg = message;
        else
            msg = msg + "\n" + message;
    }
}
function checkNumber(param, message) {
    if (!$.isNumeric(document.getElementById(param).value)) {
        error = true;
        if (msg == "")
            msg = message;
        else
            msg = msg + "\n" + message;
    }
}
function unlockForm() {

}
function view(id) {
    var dialog = new BootstrapDialog.show({
        title: 'Record Details',
        cssClass: 'view-record',
        message: $('<div></div>').load('/LogBook/HelpDesk/ViewRecord/' + id)
    });
}
function changePartner() {
    cascadeDropdown("Partner", "Governorate", Governorates);
    filterCBO();
}
function filterCBO() {
    clearDropdown("CBO");
    addDropDownItem("CBO", "", "");
    var governorate = document.getElementById("Governorate").value;
    var partner = document.getElementById("Partner").value;
    for (var item in CBOs) {
        var thisItem = CBOs[item];
        if (thisItem.Governorate == governorate && thisItem.Partner == partner) {
            addDropDownItem("CBO", thisItem.id, thisItem.CBODesc);
            document.getElementById("CBO").value = thisItem.id;
            return;
        }
    }
}
function changeSubcategory() {
    clearDropdown("ResponsibleUnit");
    addDropDownItem("ResponsibleUnit", "", "");
    var subcategory = document.getElementById("SubCategory").value;
    var unit="";
    for (var item in SubCategory) {
        var thisItem = SubCategory[item];
        if (thisItem.id == subcategory) {
            unit=thisItem.unit
            addDropDownItem("ResponsibleUnit", thisItem.unit, thisItem.UnitDesc);
        }
    }
    document.getElementById("ResponsibleUnit").value = unit;
}
function changeAddressedOnSpot() {
    if (document.getElementById("AddressedOnSpot").value == 1)
        form1.Status.value = 2; //closed
    else
        form1.Status.value = 1; //open
}