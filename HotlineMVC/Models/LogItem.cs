﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HotlineMVC.Models
{
    public class LogItem
    {
        public string Activity { get; set; }
        public string DoneBy { get; set; }
        public string DateTime { get; set; }
    }
}