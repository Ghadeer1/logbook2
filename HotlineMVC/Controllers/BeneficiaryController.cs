﻿using HotlineMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HotlineMVC.Controllers
{
    public class BeneficiaryController : Controller
    {
        // GET: Home  
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public JsonResult Index(string Prefix)
        {

            List<SelectListItem> beneficiaries = CommonFunctions.GetListFromDb("select distinct Customer_ID from prepaid_reload_meps");
            
            //Searching records from list using LINQ query  
            var beneficiaryId = (from N in beneficiaries
                                 select new { N.Value });
            return Json(beneficiaryId, JsonRequestBehavior.AllowGet);
        }
    }
}