namespace HotlineMVC.Models
{
    public class HistoryModel
    {
        public int ID { get; set; }
        public int RecordNumber { get; set; }
        public string Changes { get; set; }
        public string DoneBy { get; set; }
        public string DateTime { get; set; }
    }
}