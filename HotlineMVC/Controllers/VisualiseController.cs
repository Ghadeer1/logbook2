﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HotlineMVC.Controllers
{
    public class VisualiseController : Controller
    {
        // GET: Visualise
        public ActionResult Index()
        {
            CommonFunctions.LogActivity("Viewed Visualization Page");
            ViewBag.Months = CommonFunctions.GetJSArray(@"select format([date],'MMM-yy') as fullmonth,format([date],'yy-MM') as month 
                    from logbook_records 
                    group by format([date],'MMM-yy'),format([date],'yy-MM') order by month");
            ViewBag.Data = CommonFunctions.GetJson(@"select Map_ID as [hc-key], count(*) as value from logbook_records t1
                inner join logbook_governorates t2 on t1.Governorate = t2.ID
                group by Map_ID");
            return View();
        }
        public ActionResult Map()
        {
            ViewBag.Data = CommonFunctions.GetJson(@"select Map_ID as [hc-key], count(*) as value from logbook_records t1
                inner join logbook_governorates t2 on t1.Governorate = t2.ID
                where date >= '01-Nov-16' and date <= '30-Nov-16'
                group by Map_ID");
            return View();
        }
    }
}