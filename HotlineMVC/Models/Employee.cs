﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HotlineMVC.Models
{
    public class Employee
    {
        public int EmployeeID { get; set; }
        [Required]
        public string EmployeeName { get; set; }
        public static List<Employee> TotalEmployees
        {
            get
            {
                return new List<Employee>{
                                            new Employee{EmployeeID=1,EmployeeName="Addison",DepartmentID=1},
                                            new Employee{EmployeeID=2,EmployeeName="Ashwin",DepartmentID=1},
                                            new Employee{EmployeeID=3,EmployeeName="Alden",DepartmentID=1},
                                            new Employee{EmployeeID=4,EmployeeName="Anthony",DepartmentID=6},
                                            new Employee{EmployeeID=5,EmployeeName="Bailee",DepartmentID=5},
                                            new Employee{EmployeeID=6,EmployeeName="Baileigh",DepartmentID=4},
                                            new Employee{EmployeeID=7,EmployeeName="Banjamin",DepartmentID=2},
                                            new Employee{EmployeeID=8,EmployeeName="Cadan",DepartmentID=3},
                                            new Employee{EmployeeID=9,EmployeeName="Cadimhe",DepartmentID=2},
                                            new Employee{EmployeeID=10,EmployeeName="Carissa",DepartmentID=1},
                                            new Employee{EmployeeID=11,EmployeeName="Ceara",DepartmentID=2},
                                            new Employee{EmployeeID=12,EmployeeName="Cecilia",DepartmentID=1}
                                            };
            }
        }
        public int DepartmentID { get; set; }

        public static List<Employee> GetEmployeesLikeName(string namestring)
        {
            var x = TotalEmployees.Where<Employee>(d => d.EmployeeName.ToLower().StartsWith(namestring.ToLower()));
            return x.ToList();

        }
    }
}